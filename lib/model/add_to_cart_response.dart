/// status : true
/// message : "Added into cart"
/// data : {"store_id":"58","product_id":"1287","quantity":"1","sell_price":"110","user_id":5,"updated_at":"2021-08-31T14:12:56.000000Z","created_at":"2021-08-31T14:12:56.000000Z","id":13}

class AddToCartResponse {
  bool? status;
  String? message;
  Data? data;

  AddToCartResponse({this.status, this.message, this.data});

  AddToCartResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// store_id : "58"
/// product_id : "1287"
/// quantity : "1"
/// sell_price : "110"
/// user_id : 5
/// updated_at : "2021-08-31T14:12:56.000000Z"
/// created_at : "2021-08-31T14:12:56.000000Z"
/// id : 13

class Data {
  String? storeId;
  String? productId;
  String? quantity;
  String? sellPrice;
  int? userId;
  String? updatedAt;
  String? createdAt;
  int? id;

  Data(
      {this.storeId,
      this.productId,
      this.quantity,
      this.sellPrice,
      this.userId,
      this.updatedAt,
      this.createdAt,
      this.id});

  Data.fromJson(dynamic json) {
    storeId = json['store_id'].toString();
    productId = json['product_id'].toString();
    quantity = json['quantity'];
    sellPrice = json['sell_price'].toString();
    userId = json['user_id'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['store_id'] = storeId;
    map['product_id'] = productId;
    map['quantity'] = quantity;
    map['sell_price'] = sellPrice;
    map['user_id'] = userId;
    map['updated_at'] = updatedAt;
    map['created_at'] = createdAt;
    map['id'] = id;
    return map;
  }
}
