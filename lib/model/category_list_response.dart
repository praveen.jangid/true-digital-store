import 'package:true_digital_store/api/network_utils.dart';

/// status : true
/// message : "Category list"
/// data : [{"id":30,"name":"Hardware","slug":"hardware","category_image":"1622824974_Hardware-Store.png","category_icon":"","status":"1","cat_order":5,"created_at":"2021-06-04T16:42:54.000000Z","updated_at":"2021-06-29T02:57:22.000000Z"},{"id":31,"name":"Electronics","slug":"electronics","category_image":"1622824995_electronics.jpg","category_icon":"","status":"1","cat_order":6,"created_at":"2021-06-04T16:43:15.000000Z","updated_at":"2021-06-28T18:02:50.000000Z"},{"id":32,"name":"Salon & Spa","slug":"salon","category_image":"1622825014_salon.jpg","category_icon":"","status":"1","cat_order":7,"created_at":"2021-06-04T16:43:34.000000Z","updated_at":"2021-07-09T08:08:59.000000Z"},{"id":33,"name":"Mobile&Accessories","slug":"mobile-accessories","category_image":"1623310228_true-digital-store-mobile& accessories.jpg","category_icon":"","status":"1","cat_order":3,"created_at":"2021-06-04T16:43:56.000000Z","updated_at":"2021-06-29T02:53:42.000000Z"},{"id":34,"name":"FRUIT & VEGETABLES","slug":"fruit-vegetables","category_image":"1622825078_depositphotos_78384768-stock-illustration-mother-and-son-shopping-in.jpg","category_icon":"","status":"1","cat_order":4,"created_at":"2021-06-04T16:44:38.000000Z","updated_at":"2021-06-29T02:53:56.000000Z"},{"id":35,"name":"True Digital Store","slug":"restaurents-sweets","category_image":"1624552926_1623096294_antique.jpg","category_icon":"","status":"1","cat_order":8,"created_at":"2021-06-04T16:45:02.000000Z","updated_at":"2021-06-29T02:58:17.000000Z"},{"id":36,"name":"KIRANA&DAIRY","slug":"kirana-dairy","category_image":"1622825123_kd.jpg","category_icon":"","status":"1","cat_order":1,"created_at":"2021-06-04T16:45:23.000000Z","updated_at":"2021-06-29T02:54:33.000000Z"},{"id":42,"name":"Medical","slug":"medical","category_image":"","category_icon":"1622831962_1622379975_madicine.jpg","status":"1","cat_order":3,"created_at":"2021-06-04T18:39:22.000000Z","updated_at":"2021-06-30T10:46:34.000000Z"},{"id":44,"name":"Find a Doctor","slug":"find-a-doctor","category_image":"","category_icon":"1623075355_s.jpg","status":"1","cat_order":5,"created_at":"2021-06-04T18:42:51.000000Z","updated_at":"2021-06-30T10:46:47.000000Z"},{"id":45,"name":"TRAVELS","slug":"travels","category_image":"","category_icon":"1622832189_1622379771_Travel.jpg","status":"1","cat_order":4,"created_at":"2021-06-04T18:43:09.000000Z","updated_at":"2021-06-30T10:46:25.000000Z"},{"id":46,"name":"FURNITURE","slug":"furniture","category_image":"","category_icon":"1622832200_1622624363_1622379853_Furniture.jpg","status":"1","cat_order":0,"created_at":"2021-06-04T18:43:20.000000Z","updated_at":"2021-06-30T10:45:38.000000Z"},{"id":47,"name":"SPORTS","slug":"sports","category_image":"","category_icon":"1623075003_12.JPG","status":"1","cat_order":87,"created_at":"2021-06-05T08:20:14.000000Z","updated_at":"2021-06-30T10:44:42.000000Z"},{"id":48,"name":"Restaurent & Sweets","slug":"restaurent-sweets","category_image":"1624552159_1622825102_rr.jpg","category_icon":"","status":"1","cat_order":0,"created_at":"2021-06-24T16:29:19.000000Z","updated_at":"2021-06-29T02:56:08.000000Z"},{"id":49,"name":"Bakery Items","slug":"bakery","category_image":"","category_icon":"1625050459_Untitled design.jpg","status":"1","cat_order":1,"created_at":"2021-06-30T06:19:51.000000Z","updated_at":"2021-06-30T10:54:19.000000Z"},{"id":50,"name":"IT & Software","slug":"it-software","category_image":"","category_icon":"","status":"1","cat_order":0,"created_at":"2021-07-08T16:08:55.000000Z","updated_at":"2021-07-08T16:08:55.000000Z"}]

class CategoryListResponse {
  bool? status;
  String? message;
  List<CategoryData>? data;

  CategoryListResponse({this.status, this.message, this.data});

  CategoryListResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(CategoryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 30
/// name : "Hardware"
/// slug : "hardware"
/// category_image : "1622824974_Hardware-Store.png"
/// category_icon : ""
/// status : "1"
/// cat_order : 5
/// created_at : "2021-06-04T16:42:54.000000Z"
/// updated_at : "2021-06-29T02:57:22.000000Z"

class CategoryData {
  int? id;
  String? name;
  String? slug;
  String? categoryImage;
  String? categoryIcon;
  String? status;
  int? catOrder;
  String? createdAt;
  String? updatedAt;

  CategoryData(
      {this.id,
      this.name,
      this.slug,
      this.categoryImage,
      this.categoryIcon,
      this.status,
      this.catOrder,
      this.createdAt,
      this.updatedAt});

  CategoryData.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    if (json['category_image'] != null && json['category_image'] != "") {
      categoryImage = NetworkUtils.base_image_url + json['category_image'];
    } else {
      categoryImage = null;
    }

    categoryIcon = json['category_icon'];
    status = json['status'];
    catOrder = json['cat_order'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['slug'] = slug;
    map['category_image'] = categoryImage;
    map['category_icon'] = categoryIcon;
    map['status'] = status;
    map['cat_order'] = catOrder;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }
}
