/// status : true
/// message : "Order created succefully"
/// data : {"user_id":5,"store_id":153,"order_uuid":"2ed393170aa34f129b31bd5e79b95dba","total_amount":132,"ship_name":"PRadeep","ship_phone":"121211","ship_address":"Ganesh nagar","ship_landmark":"school","ship_city":"jaipur","ship_state":"Raj","ship_zipcode":"302302","payment_status":"2","status":"1","updated_at":"2021-10-31T02:40:10.000000Z","created_at":"2021-10-31T02:40:10.000000Z","id":5}

class OrderPlaceDto {
  bool? status;
  String? message;
  Data? data;

  OrderPlaceDto({this.status, this.message, this.data});

  OrderPlaceDto.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// user_id : 5
/// store_id : 153
/// order_uuid : "2ed393170aa34f129b31bd5e79b95dba"
/// total_amount : 132
/// ship_name : "PRadeep"
/// ship_phone : "121211"
/// ship_address : "Ganesh nagar"
/// ship_landmark : "school"
/// ship_city : "jaipur"
/// ship_state : "Raj"
/// ship_zipcode : "302302"
/// payment_status : "2"
/// status : "1"
/// updated_at : "2021-10-31T02:40:10.000000Z"
/// created_at : "2021-10-31T02:40:10.000000Z"
/// id : 5

class Data {
  int? userId;
  int? storeId;
  String? orderUuid;
  String? totalAmount;
  String? shipName;
  String? shipPhone;
  String? shipAddress;
  String? shipLandmark;
  String? shipCity;
  String? shipState;
  String? shipZipcode;
  String? paymentStatus;
  String? status;
  String? updatedAt;
  String? createdAt;
  int? id;

  Data(
      {this.userId,
      this.storeId,
      this.orderUuid,
      this.totalAmount,
      this.shipName,
      this.shipPhone,
      this.shipAddress,
      this.shipLandmark,
      this.shipCity,
      this.shipState,
      this.shipZipcode,
      this.paymentStatus,
      this.status,
      this.updatedAt,
      this.createdAt,
      this.id});

  Data.fromJson(dynamic json) {
    userId = json['user_id'];
    storeId = json['store_id'];
    orderUuid = json['order_uuid'];
    totalAmount = json['total_amount'].toString();
    shipName = json['ship_name'];
    shipPhone = json['ship_phone'];
    shipAddress = json['ship_address'];
    shipLandmark = json['ship_landmark'];
    shipCity = json['ship_city'];
    shipState = json['ship_state'];
    shipZipcode = json['ship_zipcode'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['user_id'] = userId;
    map['store_id'] = storeId;
    map['order_uuid'] = orderUuid;
    map['total_amount'] = totalAmount;
    map['ship_name'] = shipName;
    map['ship_phone'] = shipPhone;
    map['ship_address'] = shipAddress;
    map['ship_landmark'] = shipLandmark;
    map['ship_city'] = shipCity;
    map['ship_state'] = shipState;
    map['ship_zipcode'] = shipZipcode;
    map['payment_status'] = paymentStatus;
    map['status'] = status;
    map['updated_at'] = updatedAt;
    map['created_at'] = createdAt;
    map['id'] = id;
    return map;
  }
}
