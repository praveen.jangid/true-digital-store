import 'package:true_digital_store/model/product.dart';

/// status : true
/// message : "Product list"
/// data : [{"id":10569,"card_id":153,"category_id":536,"subcategory_id":537,"product_name":"Dew","mrp_price":"21","sell_price":"12","weight":"1 liter","product_image":null,"description":"good","status":"1","created_at":"2021-10-07T16:56:34.000000Z","updated_at":"2021-10-08T02:26:24.000000Z"},{"id":10597,"card_id":153,"category_id":540,"subcategory_id":552,"product_name":"dew","mrp_price":"1","sell_price":"1","weight":"1 liter","product_image":null,"description":"","status":"1","created_at":"2021-10-12T04:22:06.000000Z","updated_at":"2021-10-15T04:29:58.000000Z"},{"id":10598,"card_id":51,"category_id":553,"subcategory_id":554,"product_name":"Dew","mrp_price":"21","sell_price":"12","weight":"1 liter","product_image":null,"description":"","status":"1","created_at":"2021-10-15T04:16:10.000000Z","updated_at":"2021-10-15T04:16:10.000000Z"},{"id":10614,"card_id":51,"category_id":564,"subcategory_id":568,"product_name":"dew","mrp_price":"1","sell_price":"1","weight":"1 liter","product_image":null,"description":"","status":"1","created_at":"2021-10-15T04:16:10.000000Z","updated_at":"2021-10-15T04:16:10.000000Z"}]

class StoreProductSearch {
  bool? status;
  String? message;
  List<Product>? data;

  StoreProductSearch({this.status, this.message, this.data});

  StoreProductSearch.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Product.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
