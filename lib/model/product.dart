import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/add_to_cart_response.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/cartIconWidget.dart';
import 'package:true_digital_store/widgets/login.dart';

import 'cart_list_response.dart';

/// id : 837
/// card_id : 51
/// category_id : 76
/// product_name : "Daal Tadka"
/// mrp_price : "150"
/// sell_price : "120"
/// weight : "Full"
/// product_image : "1623002393_830daal tadka.jpg"
/// description : ""
/// status : "1"
/// created_at : "2021-06-06T12:43:13.000000Z"
/// updated_at : "2021-06-06T17:59:53.000000Z"

class Product {
  int? id;
  int? cardId;
  int? categoryId;
  int? subcategoryId;
  String? productName;
  String? mrpPrice;
  String? sellPrice;
  String? weight;
  String? description;
  String? status;
  String? createdAt;
  String? updatedAt;
  List<Product_images>? productImages;
  Store? theStore;

  int? cartItemId;

  Product(
      {this.id,
      this.cardId,
      this.categoryId,
      this.subcategoryId,
      this.productName,
      this.mrpPrice,
      this.sellPrice,
      this.weight,
      this.productImages,
      this.description,
      this.status,
      this.createdAt,
      this.updatedAt});

  Product.fromJson(dynamic json) {
    try {
      id = json['id'];
      cardId = json['card_id'];
      categoryId = json['category_id'];
      subcategoryId = json['subcategory_id'];
      productName = json['product_name'];
      mrpPrice = json['mrp_price'];
      sellPrice = json['sell_price'];
      weight = json['weight'];
      if (json['product_images'] != null) {
        productImages = [];
        json['product_images'].forEach((v) {
          productImages?.add(Product_images.fromJson(v));
        });
      }
      description = json['description'];
      status = json['status'];
      createdAt = json['created_at'];
      updatedAt = json['updated_at'];
    } catch (e) {
      log("PRODUCT PARSE ERROR:::::: ${e}");
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['card_id'] = cardId;
    map['category_id'] = categoryId;
    map['subcategory_id'] = subcategoryId;
    map['product_name'] = productName;
    map['mrp_price'] = mrpPrice;
    map['sell_price'] = sellPrice;
    map['weight'] = weight;
    if (productImages != null) {
      map['product_images'] = productImages?.map((v) => v.toJson()).toList();
    }
    map['description'] = description;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

  String getPercentDiscount() {
    if (this.mrpPrice == null ||
        this.mrpPrice == "" ||
        this.sellPrice == null ||
        this.sellPrice == "") {
      return "";
    }
    double mrp = double.parse(this.mrpPrice!);
    double sell = double.parse(this.sellPrice!);

    double diff = mrp - sell;
    var percentage = (diff * 100) / mrp;
    return percentage.toInt() == 0 ? "" : "${percentage.toInt()}% off";
  }

  String getProductImage() {
    return NetworkUtils.base_image_url +
        ((productImages != null && productImages?.length != 0)
            ? productImages?.first.productImage ?? ''
            : '');
  }

  Future<bool> addToCart(
      BuildContext context, int quantity, String storeId) async {
    BuildContext context1 = context;
    if ((cartData?.data?.cart?.length ?? 0) > 0) {
      if (cartData?.data?.cart?.first.storeId?.toString() !=
          storeId.toString()) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text('Replace cart items?'),
                content: Text('Your cart contains another store product'),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      child: Text('No')),
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                        CommonUtills.showprogressdialogcomplete(context1, true);
                        CartListResponse.clearCartList(context1,
                            (response, status) {
                          CommonUtills.showprogressdialogcomplete(
                              context1, false);
                          if (reloadCart != null) {
                            reloadCart!();
                          }
                          if (status) {
                            cartData = null;
                            addToCart(context1, quantity, storeId);
                          } else {
                            CommonUtills.successtoast(
                                context1, response.toString());
                          }
                        });
                      },
                      child: Text(
                        'Yes',
                        style: TextStyle(
                            fontFamily: AppTextStyle.bold, color: Colors.red),
                      )),
                ],
              );
            });
        return false;
      }
    }

    if (NetworkUtils.singupModel == null) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginWidget()));
      return false;
    }
    var isAdded = false;
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      Map<String, dynamic> params = {
        'store_id': theStore?.id?.toString() ?? '',
        'product_id': id?.toString() ?? '',
        'quantity': quantity.toString(),
        'sell_price': sellPrice?.toString() ?? ''
      };
      CommonUtills.showprogressdialogcomplete(context, true);
      var result = await _networkUtils.post(NetworkUtils.kAddToCart, context,
          body: params, encoding: Encoding.getByName("utf-8"));
      CommonUtills.showprogressdialogcomplete(context, false);
      AddToCartResponse response = AddToCartResponse.fromJson(result);
      if (response.status == true) {
        CommonUtills.successtoast(context, response!.message!);
        if (reloadCart != null) {
          reloadCart!();
        }
        return Future.value(true);
      } else {
        CommonUtills.errortoast(context,
            response?.message ?? 'Something went wrong, please try again');
        return Future.value(false);
      }
    } else {
      CommonUtills.noInternetToast(context);
      return Future.value(isAdded);
    }
  }

  Future<bool> updateCartQuantity(BuildContext context, int quantity) async {
    var isAdded = false;
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      Map<String, dynamic> params = {
        'store_id': theStore?.id?.toString() ?? '',
        'product_id': id?.toString() ?? '',
        'quantity': quantity.toString(),
        // 'sell_price': sellPrice?.toString() ?? ''
      };
      CommonUtills.showprogressdialogcomplete(context, true);
      var result = await _networkUtils.post(
          NetworkUtils.kUpdateCartQuantity, context,
          body: params, encoding: Encoding.getByName("utf-8"));
      CommonUtills.showprogressdialogcomplete(context, false);
      AddToCartResponse response = AddToCartResponse.fromJson(result);
      if (response.status == true) {
        CommonUtills.successtoast(context, response!.message!);
        if (reloadCart != null) {
          reloadCart!();
        }
        return Future.value(true);
      } else {
        CommonUtills.errortoast(context,
            response?.message ?? 'Something went wrong, please try again');
        return Future.value(false);
      }
    } else {
      CommonUtills.noInternetToast(context);
      return Future.value(isAdded);
    }
  }

  Future<bool> removeFromCart(BuildContext context) async {
    var isAdded = false;
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();

      CommonUtills.showprogressdialogcomplete(context, true);
      var result = await _networkUtils.delete(
          NetworkUtils.kRemoveFromCart + "?cart_id=${cartItemId ?? 0}", context,
          encoding: Encoding.getByName("utf-8"));
      CommonUtills.showprogressdialogcomplete(context, false);
      // AddToCartResponse response = AddToCartResponse.fromJson(result);
      if (result['status'] == true) {
        CommonUtills.successtoast(context, result['message']);
        if (reloadCart != null) {
          reloadCart!();
        }
        return Future.value(true);
      } else {
        CommonUtills.errortoast(context,
            result['message'] ?? 'Something went wrong, please try again');
        return Future.value(false);
      }
    } else {
      CommonUtills.noInternetToast(context);
      return Future.value(isAdded);
    }
  }
}

/// id : 51
/// product_id : 10574
/// product_image : "1633661469_Fortune Soya OIl.jpg"
/// created_at : "2021-10-08T02:51:09.000000Z"
/// updated_at : "2021-10-08T02:51:09.000000Z"

class Product_images {
  int? id;
  int? productId;
  String? productImage;
  String? createdAt;
  String? updatedAt;

  Product_images(
      {this.id,
      this.productId,
      this.productImage,
      this.createdAt,
      this.updatedAt});

  Product_images.fromJson(dynamic json) {
    id = json['id'];
    productId = json['product_id'];
    productImage = json['product_image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['product_id'] = productId;
    map['product_image'] = productImage;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

  String getProductImage() {
    return NetworkUtils.base_image_url + (this.productImage ?? "");
  }
}
