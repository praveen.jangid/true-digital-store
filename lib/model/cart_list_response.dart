import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/order_place_dto.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/utils/common_utills.dart';

/// status : true
/// message : "Cart data"
/// data : {"subTotal":400,"cart":[{"id":4,"store_id":14,"user_id":5,"product_id":37,"quantity":2,"sell_price":100,"created_at":"2021-08-27T02:43:29.000000Z","updated_at":"2021-08-27T02:43:29.000000Z"},{"id":5,"store_id":14,"user_id":5,"product_id":37,"quantity":2,"sell_price":100,"created_at":"2021-08-27T02:43:42.000000Z","updated_at":"2021-08-27T02:43:42.000000Z"}],"coupon_code":"372359","discount":0,"totalPrice":400,"coupon_message":"Invalid Coupon Code."}

class CartListResponse {
  bool? status;
  String? message;
  CartData? data;

  CartListResponse({this.status, this.message, this.data});

  CartListResponse.fromJson(dynamic json) {
    try {
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? CartData.fromJson(json['data']) : null;
    } catch (e) {
      log("EROOOOROROOROROR: ${e}");
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

  static Future<dynamic> cartList(
      BuildContext context, Function(dynamic) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.get(NetworkUtils.kGetCartData, context).then((value) {
        CartListResponse response = CartListResponse.fromJson(value);
        log("CART RESSSS::::::::: ${response.toJson()}");
        if (response.status == true) {
          completion(response);
        } else {
          completion(response.message);
        }
      }).onError((error, stackTrace) {
        log("CART ERROR::::::::: ${error}");

        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }

  static Future<dynamic> clearCartList(BuildContext context,
      Function(dynamic response, bool status) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.delete(NetworkUtils.kClearCart, context).then((value) {
        bool status = value['status'] as bool;
        if (status == true) {
          completion(value['message'] as String, true);
        } else {
          completion(value['message'] as String, false);
        }
      }).onError((error, stackTrace) {
        completion(error, false);
      });
    } else {
      completion('No internet connection available, please check and try again',
          false);
    }
  }

  static Future<dynamic> placeOrder(BuildContext context, Map params,
      Function(dynamic response) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils
          .post(NetworkUtils.kOrderCreate, context, body: params)
          .then((value) {
        OrderPlaceDto response = OrderPlaceDto.fromJson(value);
        if (response.status == true) {
          completion(response);
        } else {
          completion(response.message);
        }
      }).onError((error, stackTrace) {
        print(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }

  static Future<dynamic> applyCoupon(BuildContext context, String code,
      Function(dynamic response) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      String path = NetworkUtils.kGetCartData + "?coupon_code=" + code;
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.get(path, context).then((value) {
        CartListResponse response = CartListResponse.fromJson(value);
        if (response.status == true) {
          completion(response);
        } else {
          completion(response.message);
        }
      }).onError((error, stackTrace) {
        print(error);
        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }
}

/// subTotal : 400
/// cart : [{"id":4,"store_id":14,"user_id":5,"product_id":37,"quantity":2,"sell_price":100,"created_at":"2021-08-27T02:43:29.000000Z","updated_at":"2021-08-27T02:43:29.000000Z"},{"id":5,"store_id":14,"user_id":5,"product_id":37,"quantity":2,"sell_price":100,"created_at":"2021-08-27T02:43:42.000000Z","updated_at":"2021-08-27T02:43:42.000000Z"}]
/// coupon_code : "372359"
/// discount : 0
/// totalPrice : 400
/// coupon_message : "Invalid Coupon Code."

// class CartData {
//   int? subTotal;
//   List<Cart>? cart;
//   var gstCharges;
//   var minimumCartAmount;
//   var deliveryCharge;
//   var totalPrice;
//   String? couponMessage;
//   String? couponCode;
//   var discount;
//   var termsCondition;
//
//   CartData(
//       this.subTotal,
//       this.cart,
//       this.gstCharges,
//       this.minimumCartAmount,
//       this.deliveryCharge,
//       this.totalPrice,
//       this.couponMessage,
//       this.couponCode,
//       this.discount,
//       this.termsCondition);
//
//   // CartData(
//   //     {this.subTotal,
//   //     this.cart,
//   //     this.couponCode,
//   //     this.discount,
//   //     this.totalPrice,
//   //     this.couponMessage});
//
//   CartData.fromJson(dynamic json) {
//     subTotal = json['subTotal'];
//     if (json['cart'] != null) {
//       cart = [];
//       json['cart'].forEach((v) {
//         cart?.add(Cart.fromJson(v));
//       });
//     }
//
//     gstCharges = json['gstCharges'];
//     minimumCartAmount = json['minimumCartAmount'];
//     deliveryCharge = json['deliveryCharge'];
//     totalPrice = json['totalPrice'];
//     couponMessage = json['coupon_message'];
//     couponCode = json['coupon_code'];
//     discount = json['discount'];
//     termsCondition = json['termsCondition'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['subTotal'] = subTotal;
//     if (cart != null) {
//       map['cart'] = cart?.map((v) => v.toJson()).toList();
//     }
//     map['gstCharges'] = gstCharges;
//     map['minimumCartAmount'] = minimumCartAmount;
//     map['deliveryCharge'] = deliveryCharge;
//     map['totalPrice'] = totalPrice;
//     map['coupon_message'] = couponMessage;
//     map['coupon_code'] = couponCode;
//     map['discount'] = discount;
//     map['termsCondition'] = termsCondition;
//     return map;
//   }
// }

/// id : 4
/// store_id : 14
/// user_id : 5
/// product_id : 37
/// quantity : 2
/// sell_price : 100
/// created_at : "2021-08-27T02:43:29.000000Z"
/// updated_at : "2021-08-27T02:43:29.000000Z"

// class Cart {
//   int? id;
//   int? storeId;
//   int? userId;
//   int? productId;
//   int? quantity;
//   int? sellPrice;
//   String? createdAt;
//   String? updatedAt;
//
//   Cart(
//       {this.id,
//       this.storeId,
//       this.userId,
//       this.productId,
//       this.quantity,
//       this.sellPrice,
//       this.createdAt,
//       this.updatedAt});
//
//   Cart.fromJson(dynamic json) {
//     id = json['id'];
//     storeId = json['store_id'];
//     userId = json['user_id'];
//     productId = json['product_id'];
//     quantity = json['quantity'];
//     sellPrice = json['sell_price'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['id'] = id;
//     map['store_id'] = storeId;
//     map['user_id'] = userId;
//     map['product_id'] = productId;
//     map['quantity'] = quantity;
//     map['sell_price'] = sellPrice;
//     map['created_at'] = createdAt;
//     map['updated_at'] = updatedAt;
//     return map;
//   }
//
//   Future<dynamic> deleteItem(
//       BuildContext context, Function(dynamic) completion) async {
//     bool networkStatus = await CommonUtills.checkNetworkStatus();
//     if (networkStatus == true) {
//       NetworkUtils _networkUtils = NetworkUtils();
//       String url = NetworkUtils.kDeleteAddress + '?address_id=${this.id ?? ''}';
//       _networkUtils.delete(url, context).then((value) {
//         CartListResponse response = CartListResponse.fromJson(value);
//         completion(response);
//       }).onError((error, stackTrace) {
//         completion(error);
//       });
//     } else {
//       completion(
//           'No internet connection available, please check and try again');
//     }
//   }
// }

class CartData {
  int? subTotal;
  List<Cart>? cart;
  int? gstCharges;
  int? minimumCartAmount;
  int? deliveryCharge;
  String? totalPrice;
  String? couponMessage;
  String? couponCode;
  String? termsCondition;
  String? discount;
  String? couponUUID;
  String? addressId;

  CartData(
      {this.subTotal,
      this.cart,
      this.gstCharges,
      this.minimumCartAmount,
      this.deliveryCharge,
      this.totalPrice,
      this.couponMessage,
      this.couponCode,
      this.termsCondition,
      this.discount});

  CartData.fromJson(dynamic json) {
    subTotal = json['subTotal'];
    if (json['cart'] != null) {
      cart = [];
      json['cart'].forEach((v) {
        cart?.add(Cart.fromJson(v));
      });
    }
    gstCharges = json['gstCharges'];
    minimumCartAmount = json['minimumCartAmount'];
    deliveryCharge = json['deliveryCharge'];
    totalPrice = json['totalPrice'];
    couponMessage = json['coupon_message'];
    couponCode = json['coupon_code'];
    termsCondition = json['termsCondition'];
    discount = (json["discount"] ?? "").toString();
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['subTotal'] = subTotal;
    if (cart != null) {
      map['cart'] = cart?.map((v) => v.toJson()).toList();
    }
    map['gstCharges'] = gstCharges;
    map['minimumCartAmount'] = minimumCartAmount;
    map['deliveryCharge'] = deliveryCharge;
    map['totalPrice'] = totalPrice;
    map['coupon_message'] = couponMessage;
    map['coupon_code'] = couponCode;
    map['termsCondition'] = termsCondition;
    map['discount'] = discount;
    return map;
  }
}

/// id : 286
/// store_id : 153
/// user_id : 216
/// product_id : 10650
/// quantity : 4
/// sell_price : 60
/// created_at : "2021-11-25T02:53:39.000000Z"
/// updated_at : "2021-11-25T02:54:50.000000Z"
/// product_details : {"id":10650,"card_id":153,"category_id":598,"subcategory_id":599,"product_name":"Society","mrp_price":"70","sell_price":"60","weight":"1kg","product_image":"","description":"jzxvlkdsjklvjil","status":"1","created_at":"2021-11-23T08:26:32.000000Z","updated_at":"2021-11-23T08:26:32.000000Z"}

class Cart {
  int? id;
  int? storeId;
  int? userId;
  int? productId;
  int? quantity;
  int? sellPrice;
  String? createdAt;
  String? updatedAt;
  List<Product_images>? productImages;
  Product? productDetails;
  Store? store_details;

  Cart({
    this.id,
    this.storeId,
    this.userId,
    this.productId,
    this.quantity,
    this.sellPrice,
    this.createdAt,
    this.updatedAt,
    this.productDetails,
    this.productImages,
    this.store_details
  });

  Cart.fromJson(dynamic json) {
    try {
      id = json['id'];
      storeId = json['store_id'];
      userId = json['user_id'];
      productId = json['product_id'];
      quantity = json['quantity'];
      sellPrice = json['sell_price'];
      createdAt = json['created_at'];
      updatedAt = json['updated_at'];
      store_details = json['store_details'] != null
          ? Store.fromJson(json['store_details'])
          : null;
      productDetails = json['product_details'] != null
          ? Product.fromJson(json['product_details'])
          : null;
      if (json['product_images'] != null) {
        productImages = [];
        json['product_images'].forEach((v) {
          productImages?.add(Product_images.fromJson(v));
        });
      }
    } catch (e) {
      log("EROOOOROROOROROR: ${e}");
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['store_id'] = storeId;
    map['user_id'] = userId;
    map['product_id'] = productId;
    map['quantity'] = quantity;
    map['sell_price'] = sellPrice;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (productDetails != null) {
      map['product_details'] = productDetails?.toJson();
    }
    return map;
  }

  Future<dynamic> deleteItem(
      BuildContext context, Function(dynamic) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      String url = NetworkUtils.kDeleteAddress + '?address_id=${this.id ?? ''}';
      _networkUtils.delete(url, context).then((value) {
        CartListResponse response = CartListResponse.fromJson(value);
        completion(response);
      }).onError((error, stackTrace) {
        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }
}

/// id : 10650
/// card_id : 153
/// category_id : 598
/// subcategory_id : 599
/// product_name : "Society"
/// mrp_price : "70"
/// sell_price : "60"
/// weight : "1kg"
/// product_image : ""
/// description : "jzxvlkdsjklvjil"
/// status : "1"
/// created_at : "2021-11-23T08:26:32.000000Z"
/// updated_at : "2021-11-23T08:26:32.000000Z"

// class Product_details {
//   int? id;
//   int? cardId;
//   int? categoryId;
//   int? subcategoryId;
//   String? productName;
//   String? mrpPrice;
//   String? sellPrice;
//   String? weight;
//   String? productImage;
//   String? description;
//   String? status;
//   String? createdAt;
//   String? updatedAt;
//
//   Product_details({
//       this.id,
//       this.cardId,
//       this.categoryId,
//       this.subcategoryId,
//       this.productName,
//       this.mrpPrice,
//       this.sellPrice,
//       this.weight,
//       this.productImage,
//       this.description,
//       this.status,
//       this.createdAt,
//       this.updatedAt});
//
//   Product_details.fromJson(dynamic json) {
//     id = json['id'];
//     cardId = json['card_id'];
//     categoryId = json['category_id'];
//     subcategoryId = json['subcategory_id'];
//     productName = json['product_name'];
//     mrpPrice = json['mrp_price'];
//     sellPrice = json['sell_price'];
//     weight = json['weight'];
//     productImage = json['product_image'];
//     description = json['description'];
//     status = json['status'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['id'] = id;
//     map['card_id'] = cardId;
//     map['category_id'] = categoryId;
//     map['subcategory_id'] = subcategoryId;
//     map['product_name'] = productName;
//     map['mrp_price'] = mrpPrice;
//     map['sell_price'] = sellPrice;
//     map['weight'] = weight;
//     map['product_image'] = productImage;
//     map['description'] = description;
//     map['status'] = status;
//     map['created_at'] = createdAt;
//     map['updated_at'] = updatedAt;
//     return map;
//   }
//
// }
