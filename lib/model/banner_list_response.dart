/// status : true
/// message : "Banner list"
/// data : [{"id":48,"banner_image":"1625465695_SLIDER POSTER2.1.jpg","banner_link":"https://truedigitalstore.com/store-details/nfc-fast-food-restaurant","status":"1","created_at":"2021-07-05T06:14:55.000000Z","updated_at":"2021-07-05T06:14:55.000000Z"},{"id":49,"banner_image":"1625505399_FASTFOOD SLIDER.jpg","banner_link":"https://truedigitalstore.com/view-store/restaurent-sweets","status":"1","created_at":"2021-07-05T17:16:39.000000Z","updated_at":"2021-07-05T17:16:39.000000Z"},{"id":51,"banner_image":"1625560528_ICE CREAME.jpg","banner_link":"https://truedigitalstore.com/store-details/anil-ice-cream-(cold-drink)-and-general-store","status":"1","created_at":"2021-07-06T08:35:28.000000Z","updated_at":"2021-07-06T08:35:28.000000Z"},{"id":53,"banner_image":"1626079496_SALON POSTEER.jpg","banner_link":"https://truedigitalstore.com/view-store/salon","status":"1","created_at":"2021-07-12T08:44:56.000000Z","updated_at":"2021-07-12T08:44:56.000000Z"},{"id":54,"banner_image":"1626079519_VACCINE POSTER.jpg","banner_link":"https://www.cowin.gov.in/","status":"1","created_at":"2021-07-12T08:45:19.000000Z","updated_at":"2021-07-12T08:45:19.000000Z"}]

class BannerListResponse {
  bool? status;
  String? message;
  List<BannerData>? data;

  BannerListResponse({
      this.status,
      this.message,
      this.data});

  BannerListResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(BannerData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 48
/// banner_image : "1625465695_SLIDER POSTER2.1.jpg"
/// banner_link : "https://truedigitalstore.com/store-details/nfc-fast-food-restaurant"
/// status : "1"
/// created_at : "2021-07-05T06:14:55.000000Z"
/// updated_at : "2021-07-05T06:14:55.000000Z"

class BannerData {
  int? id;
  String? bannerImage;
  String? bannerLink;
  String? status;
  String? createdAt;
  String? updatedAt;

  BannerData({
      this.id,
      this.bannerImage,
      this.bannerLink,
      this.status,
      this.createdAt,
      this.updatedAt});

  BannerData.fromJson(dynamic json) {
    id = json['id'];
    bannerImage = json['banner_image'];
    bannerLink = json['banner_link'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['banner_image'] = bannerImage;
    map['banner_link'] = bannerLink;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

}