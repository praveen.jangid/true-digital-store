import 'package:true_digital_store/model/product.dart';

class StoreProductResponse {
  bool? status;
  String? message;
  List<Product>? data;

  StoreProductResponse({this.status, this.message, this.data});

  StoreProductResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Product.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
