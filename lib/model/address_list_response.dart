import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/utils/common_utills.dart';

/// status : true
/// message : "Address List"
/// data : [{"id":1,"user_id":5,"name":"PRadeep","address":"Ganesh nagar","street":"ganesh nagar","landmark":"school","city":"jaipur","state":"Raj","zipcode":"302302","phone":"121211","status":"1","created_at":"2021-08-26T19:29:43.000000Z","updated_at":"2021-08-26T19:29:43.000000Z"}]

class AddressListResponse {
  bool? status;
  String? message;
  List<Address>? data;

  AddressListResponse({this.status, this.message, this.data});

  AddressListResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Address.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

  static Future<dynamic> addressList(
      BuildContext context, Function(dynamic) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.get(NetworkUtils.kGetAddressList, context).then((value) {
        AddressListResponse response = AddressListResponse.fromJson(value);
        if (response.status == true) {
          completion(response.data);
        } else {
          completion(response.message);
        }
      }).onError((error, stackTrace) {
        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }
}

/// id : 1
/// user_id : 5
/// name : "PRadeep"
/// address : "Ganesh nagar"
/// street : "ganesh nagar"
/// landmark : "school"
/// city : "jaipur"
/// state : "Raj"
/// zipcode : "302302"
/// phone : "121211"
/// status : "1"
/// created_at : "2021-08-26T19:29:43.000000Z"
/// updated_at : "2021-08-26T19:29:43.000000Z"

class AddAddressResponse {
  bool? status;
  String? message;
  Address? data;

  AddAddressResponse({this.status, this.message, this.data});

  AddAddressResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = Address.fromJson(json["data"]);
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

  static Future<dynamic> addressList(
      BuildContext context, Function(dynamic) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.get(NetworkUtils.kGetAddressList, context).then((value) {
        AddressListResponse response = AddressListResponse.fromJson(value);
        if (response.status == true) {
          completion(response.data);
        } else {
          completion(response.message);
        }
      }).onError((error, stackTrace) {
        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }
}

class Address {
  int? id;
  int? userId;
  String? name;
  String? address;
  String? phone;
  String? createdAt;
  String? updatedAt;
  String? location;
  String? latitude;
  String? longitude;

  Address(
      {this.id,
      this.userId,
      this.name,
      this.address,
      this.phone,
      this.createdAt,
      this.updatedAt});

  Address.fromJson(dynamic json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    address = json['address'];
    phone = json['phone'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    location = json['location'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['name'] = name;
    map['address'] = address;
    map['phone'] = phone;
    map['location'] = location;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

  String fullAddress() {
    return ""; //'${(this.address ?? '') + ',' + (this.street ?? '') + ',' + (this.landmark ?? '')}';
  }

  String cityState() {
    return ""; //'${(this.city ?? '') + ',' + (this.state ?? '') + ',' + (this.zipcode ?? '')}';
  }

  Future<dynamic> deleteAddress(
      BuildContext context, Function(dynamic) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      String url = NetworkUtils.kDeleteAddress + '?address_id=${this.id ?? ''}';
      _networkUtils.delete(url, context).then((value) {
        AddressListResponse response = AddressListResponse.fromJson(value);
        completion(response);
      }).onError((error, stackTrace) {
        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }

  Future<Address?> addAddress(BuildContext context) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      Map<String, dynamic> params = toJson();
      params.removeWhere((key, value) => key == null || value == null);
      CommonUtills.showprogressdialogcomplete(context, true);

      String apiPath = "";
      if (this.id != null) {
        apiPath = NetworkUtils.kUpdateAddress;
        params["address_id"] = this.id;
      } else {
        apiPath = NetworkUtils.kAddAddress;
      }
      var result = await _networkUtils.post(apiPath, context, body: params);
      CommonUtills.showprogressdialogcomplete(context, false);
      print(result);
      AddAddressResponse response = AddAddressResponse.fromJson(result);
      if (response.status == true) {
        CommonUtills.successtoast(context, response!.message!);
        return response.data;
      } else {
        CommonUtills.errortoast(context,
            response?.message ?? 'Something went wrong, please try again');
        return null;
      }
    } else {
      CommonUtills.noInternetToast(context);
      return null;
    }
  }
}
