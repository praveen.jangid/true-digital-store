import 'package:flutter/cupertino.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/utils/common_utills.dart';

import '../splash_screen.dart';

/// status : true
/// message : "User Logged In Successfully"
/// data : {"id":83,"user_uuid":"f928deacb073457692376141f83d9b56","name":"Pradeep","email":"007raj2@gmail.com","phone_number":"9887192579","phone_code":"+91","device_token":"","isVerified":1,"gender":"u","status":"1","isVerifiedEmail":0,"created_at":"2021-06-09T12:27:56.000000Z","updated_at":"2021-06-09T12:28:15.000000Z","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNWU1ZjIzM2I0ZWJmMmM0ZjNiYzg5ODVhNDlkNjkxOTA3YTJmMDU1MzRmNzlhODVkNmQwYjI0MzY4ZGIzNjg1YjQ5ZmI2OGEyOWY5Yjg3NmIiLCJpYXQiOjE2Mzc3NTkxMDksIm5iZiI6MTYzNzc1OTEwOSwiZXhwIjoxNjY5Mjk1MTA5LCJzdWIiOiI4MyIsInNjb3BlcyI6W119.F-TvCQ7jqbLQA2G63G5w2AFc7PJdJBwiPA9L5KEYWcv1G-TBoHlO847yj14xEvoXwOCa-y2XZHu_kEeMYbvGVLDY81qrLnShhyyUOwl8jQo3eNiRFsciHdmrna8P3ElC2VaQI5MnW-7S91mnkijdIvETYKtENFMaTHSzl3-8VZvLRMFtB888b1jIA2O2Zi9HgX7xzzEcYkKMz60CnMcLADK-PLRnPnWrpoA5BcGfEPeV23SOgquL6iDtJK9F4mK8bk06qLCbaA3g8m69LhUWPdh3drIvf13axAdzrFFIcP_3bo6SgqTWmSNbi0TwktnxuWrj-UvSeQdmbg71Zden-xTaYjLpZYyWbMh6uSdh5TfrCmVo4_kec-jHlFItDWrtlbTPYRWcLH9tsBRK6McqCg9qoPAOKvqKjT_q7Zj-0Jhg5NWnEQsfYHktam3EnnRpEPbp1SKrY0wwkSv-1kuODxXiK5iSASHjP4wccaYmZv1WgnRe8quT4oUUcLrS8vMxxEloCbtC3bdpE45PVuzeFYPO9we-fdwhpDfo5lYAFUieAPSZRC_8DRyBSkwlo1RNObo4OGF0a-OMLNB7C4RECuymH-VMwQYt3LI8AEoDI0Lx9pJMnjpqtlhWoC8xWbJCcvjZ0xdQi3zkrdzBVNavnzT4i4q63EOY-Fy3_dvFnzA"}

class LoginResponse {
  bool? status;
  String? message;
  Data? data;

  LoginResponse({
      this.status, 
      this.message, 
      this.data});

  LoginResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

  static Future<LoginResponse?> loginWith(BuildContext context, String phoneNumber) async {
    var isAdded = false;
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      Map<String, dynamic> params = {
        'phone_number': phoneNumber,
        'device_token': firebaseToken ?? '',
      };
      CommonUtills.showprogressdialogcomplete(context, true);
      var result = await _networkUtils.post(NetworkUtils.kLogin, context,
          body: params);
      CommonUtills.showprogressdialogcomplete(context, false);
      LoginResponse response = LoginResponse.fromJson(result);
      if (response.status == true) {
        CommonUtills.successtoast(context, response!.message!);

        return response;
      } else {
        CommonUtills.errortoast(context,
            response?.message ?? 'Something went wrong, please try again');
        return null;
      }
    } else {
      CommonUtills.noInternetToast(context);
      return null;
    }
  }

}

/// id : 83
/// user_uuid : "f928deacb073457692376141f83d9b56"
/// name : "Pradeep"
/// email : "007raj2@gmail.com"
/// phone_number : "9887192579"
/// phone_code : "+91"
/// device_token : ""
/// isVerified : 1
/// gender : "u"
/// status : "1"
/// isVerifiedEmail : 0
/// created_at : "2021-06-09T12:27:56.000000Z"
/// updated_at : "2021-06-09T12:28:15.000000Z"
/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNWU1ZjIzM2I0ZWJmMmM0ZjNiYzg5ODVhNDlkNjkxOTA3YTJmMDU1MzRmNzlhODVkNmQwYjI0MzY4ZGIzNjg1YjQ5ZmI2OGEyOWY5Yjg3NmIiLCJpYXQiOjE2Mzc3NTkxMDksIm5iZiI6MTYzNzc1OTEwOSwiZXhwIjoxNjY5Mjk1MTA5LCJzdWIiOiI4MyIsInNjb3BlcyI6W119.F-TvCQ7jqbLQA2G63G5w2AFc7PJdJBwiPA9L5KEYWcv1G-TBoHlO847yj14xEvoXwOCa-y2XZHu_kEeMYbvGVLDY81qrLnShhyyUOwl8jQo3eNiRFsciHdmrna8P3ElC2VaQI5MnW-7S91mnkijdIvETYKtENFMaTHSzl3-8VZvLRMFtB888b1jIA2O2Zi9HgX7xzzEcYkKMz60CnMcLADK-PLRnPnWrpoA5BcGfEPeV23SOgquL6iDtJK9F4mK8bk06qLCbaA3g8m69LhUWPdh3drIvf13axAdzrFFIcP_3bo6SgqTWmSNbi0TwktnxuWrj-UvSeQdmbg71Zden-xTaYjLpZYyWbMh6uSdh5TfrCmVo4_kec-jHlFItDWrtlbTPYRWcLH9tsBRK6McqCg9qoPAOKvqKjT_q7Zj-0Jhg5NWnEQsfYHktam3EnnRpEPbp1SKrY0wwkSv-1kuODxXiK5iSASHjP4wccaYmZv1WgnRe8quT4oUUcLrS8vMxxEloCbtC3bdpE45PVuzeFYPO9we-fdwhpDfo5lYAFUieAPSZRC_8DRyBSkwlo1RNObo4OGF0a-OMLNB7C4RECuymH-VMwQYt3LI8AEoDI0Lx9pJMnjpqtlhWoC8xWbJCcvjZ0xdQi3zkrdzBVNavnzT4i4q63EOY-Fy3_dvFnzA"

class Data {
  int? id;
  String? userUuid;
  String? name;
  String? email;
  String? phoneNumber;
  String? phoneCode;
  String? deviceToken;
  int? isVerified;
  String? gender;
  String? status;
  int? isVerifiedEmail;
  String? createdAt;
  String? updatedAt;
  String? token;

  Data({
      this.id, 
      this.userUuid, 
      this.name, 
      this.email, 
      this.phoneNumber, 
      this.phoneCode, 
      this.deviceToken, 
      this.isVerified, 
      this.gender, 
      this.status, 
      this.isVerifiedEmail, 
      this.createdAt, 
      this.updatedAt, 
      this.token});

  Data.fromJson(dynamic json) {
    id = json['id'];
    userUuid = json['user_uuid'];
    name = json['name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    phoneCode = json['phone_code'];
    deviceToken = json['device_token'];
    isVerified = json['isVerified'];
    gender = json['gender'];
    status = json['status'];
    isVerifiedEmail = json['isVerifiedEmail'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_uuid'] = userUuid;
    map['name'] = name;
    map['email'] = email;
    map['phone_number'] = phoneNumber;
    map['phone_code'] = phoneCode;
    map['device_token'] = deviceToken;
    map['isVerified'] = isVerified;
    map['gender'] = gender;
    map['status'] = status;
    map['isVerifiedEmail'] = isVerifiedEmail;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['token'] = token;
    return map;
  }

}