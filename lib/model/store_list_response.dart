import 'package:true_digital_store/model/store.dart';

/// status : true
/// message : "Store list"
/// data : [{"id":69,"user_id":110,"business_name":"muscat-electronics","category_id":31,"cover_image":"","first_name":"IMRAN","last_name":"KHILZEE","position":"OWNER","phone":"+919828505016","telephone":"+918209505025","whatsapp":"+919828505016","address":"NEAR","email":null,"website":null,"location":"Mahaveer Market, New SBBJ Road, Didwana, Rajasthan 341303, India","company_est_date":"2008","about_us":"ELECTRIC SHOP","latitude":"27.4004133","longitude":"74.5724863","facebook_link":null,"twitter_link":null,"instagram_link":null,"linkedin_link":null,"youtube_link":null,"pinterest_link":null,"youtube_video1":null,"youtube_video2":null,"youtube_video3":null,"youtube_video4":null,"youtube_video5":null,"youtube_video6":null,"paytm":null,"googlepay":null,"phonepe":null,"bank_name":null,"account_name":null,"account_number":null,"ifsc_code":null,"gst":null,"paytm_qr":"","googlepay_qr":"","phonepe_qr":"","status":"1","payment_status":"1","payment_date":"2021-07-03 13:46:04","trail_ends":"0000-00-00 00:00:00","package_expire":"2022-07-03 13:46:04","shop_time":null,"post_views":44,"minCartAmount":null,"deliveryCharge":null,"created_at":"2021-07-03T08:03:46.000000Z","updated_at":"2021-08-19T16:29:07.000000Z"}]

class StoreListResponse {
  bool? status;
  String? message;
  List<Store>? data;

  StoreListResponse({this.status, this.message, this.data});

  StoreListResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Store.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
