import 'package:true_digital_store/model/store.dart';

/// status : true
/// message : "Order List"
/// data : [{"id":4,"user_id":5,"order_uuid":"7e09df7844b2421ca8fb3cae4f0b3a32","store_id":89,"total_amount":"380.00","ship_name":"PRadeep","ship_phone":"121211","ship_address":"Ganesh nagar","ship_street":"","ship_landmark":"school","ship_city":"jaipur","ship_state":"Raj","ship_zipcode":"302302","delivery_type":"1","payment_mode":"1","payment_status":"2","status":"1","created_at":"2021-10-26T16:54:23.000000Z","updated_at":"2021-10-26T16:54:23.000000Z","order_items":[{"id":2,"order_id":4,"product_title":"Double Chabi Golden Rice","mrp_price":"600.00","sell_price":"380.00","quantity":1,"total_price":"380.00","created_at":"2021-10-26T16:54:23.000000Z","updated_at":"2021-10-26T16:54:23.000000Z"}]},{"id":5,"user_id":5,"order_uuid":"2ed393170aa34f129b31bd5e79b95dba","store_id":153,"total_amount":"132.00","ship_name":"PRadeep","ship_phone":"121211","ship_address":"Ganesh nagar","ship_street":"","ship_landmark":"school","ship_city":"jaipur","ship_state":"Raj","ship_zipcode":"302302","delivery_type":"1","payment_mode":"1","payment_status":"2","status":"1","created_at":"2021-10-31T02:40:10.000000Z","updated_at":"2021-10-31T02:40:10.000000Z","order_items":[{"id":3,"order_id":5,"product_title":"Dew","mrp_price":"21.00","sell_price":"12.00","quantity":1,"total_price":"12.00","created_at":"2021-10-31T02:40:10.000000Z","updated_at":"2021-10-31T02:40:10.000000Z"},{"id":4,"order_id":5,"product_title":"Kingtec(Royal)","mrp_price":"70.00","sell_price":"60.00","quantity":1,"total_price":"60.00","created_at":"2021-10-31T02:40:10.000000Z","updated_at":"2021-10-31T02:40:10.000000Z"},{"id":5,"order_id":5,"product_title":"Society","mrp_price":"70.00","sell_price":"60.00","quantity":1,"total_price":"60.00","created_at":"2021-10-31T02:40:10.000000Z","updated_at":"2021-10-31T02:40:10.000000Z"}]},{"id":6,"user_id":5,"order_uuid":"3456d0f10c664900b7470bf6f95907bb","store_id":153,"total_amount":"12.00","ship_name":"PRadeep","ship_phone":"121211","ship_address":"Ganesh nagar","ship_street":"","ship_landmark":"school","ship_city":"jaipur","ship_state":"Raj","ship_zipcode":"302302","delivery_type":"1","payment_mode":"1","payment_status":"2","status":"1","created_at":"2021-10-31T02:46:10.000000Z","updated_at":"2021-10-31T02:46:10.000000Z","order_items":[{"id":6,"order_id":6,"product_title":"Dew","mrp_price":"21.00","sell_price":"12.00","quantity":1,"total_price":"12.00","created_at":"2021-10-31T02:46:10.000000Z","updated_at":"2021-10-31T02:46:10.000000Z"}]},{"id":7,"user_id":5,"order_uuid":"32fd838480e4437f8e7dc3d169342fb3","store_id":153,"total_amount":"60.00","ship_name":"PRadeep","ship_phone":"121211","ship_address":"Ganesh nagar","ship_street":"","ship_landmark":"school","ship_city":"jaipur","ship_state":"Raj","ship_zipcode":"302302","delivery_type":"1","payment_mode":"1","payment_status":"2","status":"1","created_at":"2021-10-31T02:47:43.000000Z","updated_at":"2021-10-31T02:47:43.000000Z","order_items":[{"id":7,"order_id":7,"product_title":"Kingtec(Royal)","mrp_price":"70.00","sell_price":"60.00","quantity":1,"total_price":"60.00","created_at":"2021-10-31T02:47:43.000000Z","updated_at":"2021-10-31T02:47:43.000000Z"}]}]

class OrderlistDto {
  bool? status;
  String? message;
  List<OrderData>? data;

  OrderlistDto({this.status, this.message, this.data});

  OrderlistDto.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(OrderData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 4
/// user_id : 5
/// order_uuid : "7e09df7844b2421ca8fb3cae4f0b3a32"
/// store_id : 89
/// total_amount : "380.00"
/// ship_name : "PRadeep"
/// ship_phone : "121211"
/// ship_address : "Ganesh nagar"
/// ship_street : ""
/// ship_landmark : "school"
/// ship_city : "jaipur"
/// ship_state : "Raj"
/// ship_zipcode : "302302"
/// delivery_type : "1"
/// payment_mode : "1"
/// payment_status : "2"
/// status : "1"
/// created_at : "2021-10-26T16:54:23.000000Z"
/// updated_at : "2021-10-26T16:54:23.000000Z"
/// order_items : [{"id":2,"order_id":4,"product_title":"Double Chabi Golden Rice","mrp_price":"600.00","sell_price":"380.00","quantity":1,"total_price":"380.00","created_at":"2021-10-26T16:54:23.000000Z","updated_at":"2021-10-26T16:54:23.000000Z"}]

class OrderData {
  int? id;
  int? userId;
  String? orderUuid;
  int? storeId;
  String? totalAmount;
  String? shipName;
  String? shipPhone;
  String? shipAddress;
  String? shipStreet;
  String? shipLandmark;
  String? shipCity;
  String? shipState;
  String? ship_location;
  String? shipZipcode;
  String? deliveryType;
  String? paymentMode;
  String? paymentStatus;
  String? status;
  String? createdAt;
  String? updatedAt;
  List<OrderItem>? orderItems;
  Store? theStore;

  OrderData(
      {this.id,
      this.userId,
      this.orderUuid,
      this.storeId,
      this.totalAmount,
      this.shipName,
      this.shipPhone,
      this.shipAddress,
      this.shipStreet,
      this.shipLandmark,
      this.shipCity,
      this.shipState,
      this.shipZipcode,
      this.deliveryType,
      this.paymentMode,
      this.paymentStatus,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.orderItems,
      this.theStore, this.ship_location});

  OrderData.fromJson(dynamic json) {
    id = json['id'];
    ship_location = json['ship_location'];
    userId = json['user_id'];
    orderUuid = json['order_uuid'];
    storeId = json['store_id'];
    totalAmount = json['total_amount'];
    shipName = json['ship_name'];
    shipPhone = json['ship_phone'];
    shipAddress = json['ship_address'];
    shipStreet = json['ship_street'];
    shipLandmark = json['ship_landmark'];
    shipCity = json['ship_city'];
    shipState = json['ship_state'];
    shipZipcode = json['ship_zipcode'];
    deliveryType = json['delivery_type'];
    paymentMode = json['payment_mode'];
    paymentStatus = json['payment_status'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['order_items'] != null) {
      orderItems = [];
      json['order_items'].forEach((v) {
        orderItems?.add(OrderItem.fromJson(v));
      });
    }

    if (json["store"] != null) {
      this.theStore = Store.fromJson(json["store"]);
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['order_uuid'] = orderUuid;
    map['store_id'] = storeId;
    map['total_amount'] = totalAmount;
    map['ship_name'] = shipName;
    map['ship_phone'] = shipPhone;
    map['ship_address'] = shipAddress;
    map['ship_street'] = shipStreet;
    map['ship_landmark'] = shipLandmark;
    map['ship_city'] = shipCity;
    map['ship_state'] = shipState;
    map['ship_zipcode'] = shipZipcode;
    map['delivery_type'] = deliveryType;
    map['payment_mode'] = paymentMode;
    map['payment_status'] = paymentStatus;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (orderItems != null) {
      map['order_items'] = orderItems?.map((v) => v.toJson()).toList();
    }
    return map;
  }

  String getPaymentMode() {
    var mode = "";
    if (this.paymentMode == "1") {
      return "Credit Card/Debit Card";
    } else if (this.paymentMode == "2") {
      return "Digital Payment";
    } else if (this.paymentMode == "3") {
      return "COD";
    } else {
      return "";
    }
    return mode;
  }
}

/// id : 2
/// order_id : 4
/// product_title : "Double Chabi Golden Rice"
/// mrp_price : "600.00"
/// sell_price : "380.00"
/// quantity : 1
/// total_price : "380.00"
/// created_at : "2021-10-26T16:54:23.000000Z"
/// updated_at : "2021-10-26T16:54:23.000000Z"

class OrderItem {
  int? id;
  int? orderId;
  String? productTitle;
  String? productImage;
  String? mrpPrice;
  String? sellPrice;
  int? quantity;
  String? totalPrice;
  String? createdAt;
  String? updatedAt;

  OrderItem(
      {this.id,
      this.orderId,
      this.productTitle,
      this.productImage,
      this.mrpPrice,
      this.sellPrice,
      this.quantity,
      this.totalPrice,
      this.createdAt,
      this.updatedAt});

  OrderItem.fromJson(dynamic json) {
    id = json['id'];
    orderId = json['order_id'];
    productTitle = json['product_title'];
    productImage = json['product_image'];
    mrpPrice = json['mrp_price'];
    sellPrice = json['sell_price'];
    quantity = json['quantity'];
    totalPrice = json['total_price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['order_id'] = orderId;
    map['product_title'] = productTitle;
    map['product_image'] = productImage;
    map['mrp_price'] = mrpPrice;
    map['sell_price'] = sellPrice;
    map['quantity'] = quantity;
    map['total_price'] = totalPrice;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

  double totalItemsPrice() {
    return this.quantity! * double.parse(this.sellPrice!);
  }
}
