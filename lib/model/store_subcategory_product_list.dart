import 'package:true_digital_store/model/product.dart';

/// status : true
/// message : "Product list With Subcategory"
/// data : [{"id":511,"user_id":21,"name":"Cow Milk","category_image":"1633063075_215e31a98cb6fd49cf29b70d8af45d6c.png","status":"1","pcat_order":0,"created_at":"2021-10-01T03:36:50.000000Z","updated_at":"2021-10-01T04:37:55.000000Z","products":[{"id":848,"card_id":52,"category_id":79,"subcategory_id":511,"product_name":"Saras","mrp_price":"25","sell_price":"23","weight":"500ml","product_image":"1622984885_77p1-1457842480_835x547.jpg","description":"afagdsgdhfdhhjjjhfkhkhf zbbfxhf","status":"1","created_at":"2021-06-06T12:48:45.000000Z","updated_at":"2021-10-01T04:40:30.000000Z"}]}]

class StoreSubcategoryProductList {
  bool? status;
  String? message;
  List<StoreSubcategoryData>? data;

  StoreSubcategoryProductList({this.status, this.message, this.data});

  StoreSubcategoryProductList.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(StoreSubcategoryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 511
/// user_id : 21
/// name : "Cow Milk"
/// category_image : "1633063075_215e31a98cb6fd49cf29b70d8af45d6c.png"
/// status : "1"
/// pcat_order : 0
/// created_at : "2021-10-01T03:36:50.000000Z"
/// updated_at : "2021-10-01T04:37:55.000000Z"
/// products : [{"id":848,"card_id":52,"category_id":79,"subcategory_id":511,"product_name":"Saras","mrp_price":"25","sell_price":"23","weight":"500ml","product_image":"1622984885_77p1-1457842480_835x547.jpg","description":"afagdsgdhfdhhjjjhfkhkhf zbbfxhf","status":"1","created_at":"2021-06-06T12:48:45.000000Z","updated_at":"2021-10-01T04:40:30.000000Z"}]

class StoreSubcategoryData {
  int? id;
  int? userId;
  String? name;
  String? categoryImage;
  String? status;
  int? pcatOrder;
  String? createdAt;
  String? updatedAt;
  List<Product>? products;

  StoreSubcategoryData(
      {this.id,
      this.userId,
      this.name,
      this.categoryImage,
      this.status,
      this.pcatOrder,
      this.createdAt,
      this.updatedAt,
      this.products});

  StoreSubcategoryData.fromJson(dynamic json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    categoryImage = json['category_image'];
    status = json['status'];
    pcatOrder = json['pcat_order'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['products'] != null) {
      products = [];
      json['products'].forEach((v) {
        products?.add(Product.fromJson(v));
      });
    }
    if (json['product'] != null) {
      products = [];
      json['product'].forEach((v) {
        products?.add(Product.fromJson(v));
      });
    } else if (json['all_products'] != null) {
      products = [];
      json['all_products'].forEach((v) {
        products?.add(Product.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['name'] = name;
    map['category_image'] = categoryImage;
    map['status'] = status;
    map['pcat_order'] = pcatOrder;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (products != null) {
      map['products'] = products?.map((v) => v.toJson()).toList();
    }

    return map;
  }
}
