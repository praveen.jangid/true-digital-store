// import 'package:flutter/cupertino.dart';
// import 'package:true_digital_store/api/network_utils.dart';
// import 'package:true_digital_store/model/product.dart';
// import 'package:true_digital_store/utils/common_utills.dart';
//
// import 'cart_list_response.dart';
//
// /// status : true
// /// message : "Cart data"
// /// data : {"subTotal":324,"cart":[{"id":286,"store_id":153,"user_id":216,"product_id":10650,"quantity":4,"sell_price":60,"created_at":"2021-11-25T02:53:39.000000Z","updated_at":"2021-11-25T02:54:50.000000Z","product_details":{"id":10650,"card_id":153,"category_id":598,"subcategory_id":599,"product_name":"Society","mrp_price":"70","sell_price":"60","weight":"1kg","product_image":"","description":"jzxvlkdsjklvjil","status":"1","created_at":"2021-11-23T08:26:32.000000Z","updated_at":"2021-11-23T08:26:32.000000Z"}}],"gstCharges":18,"minimumCartAmount":100,"deliveryCharge":20,"totalPrice":"362.32","coupon_message":""}
//
//
//
// /// subTotal : 324
// /// cart : [{"id":286,"store_id":153,"user_id":216,"product_id":10650,"quantity":4,"sell_price":60,"created_at":"2021-11-25T02:53:39.000000Z","updated_at":"2021-11-25T02:54:50.000000Z","product_details":{"id":10650,"card_id":153,"category_id":598,"subcategory_id":599,"product_name":"Society","mrp_price":"70","sell_price":"60","weight":"1kg","product_image":"","description":"jzxvlkdsjklvjil","status":"1","created_at":"2021-11-23T08:26:32.000000Z","updated_at":"2021-11-23T08:26:32.000000Z"}}]
// /// gstCharges : 18
// /// minimumCartAmount : 100
// /// deliveryCharge : 20
// /// totalPrice : "362.32"
// /// coupon_message : ""
//
// class CartData {
//   int? subTotal;
//   List<Cart>? cart;
//   int? gstCharges;
//   int? minimumCartAmount;
//   int? deliveryCharge;
//   String? totalPrice;
//   String? couponMessage;
//
//   CartData({
//       this.subTotal,
//       this.cart,
//       this.gstCharges,
//       this.minimumCartAmount,
//       this.deliveryCharge,
//       this.totalPrice,
//       this.couponMessage});
//
//   CartData.fromJson(dynamic json) {
//     subTotal = json['subTotal'];
//     if (json['cart'] != null) {
//       cart = [];
//       json['cart'].forEach((v) {
//         cart?.add(Cart.fromJson(v));
//       });
//     }
//     gstCharges = json['gstCharges'];
//     minimumCartAmount = json['minimumCartAmount'];
//     deliveryCharge = json['deliveryCharge'];
//     totalPrice = json['totalPrice'];
//     couponMessage = json['coupon_message'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['subTotal'] = subTotal;
//     if (cart != null) {
//       map['cart'] = cart?.map((v) => v.toJson()).toList();
//     }
//     map['gstCharges'] = gstCharges;
//     map['minimumCartAmount'] = minimumCartAmount;
//     map['deliveryCharge'] = deliveryCharge;
//     map['totalPrice'] = totalPrice;
//     map['coupon_message'] = couponMessage;
//     return map;
//   }
//
// }
//
// /// id : 286
// /// store_id : 153
// /// user_id : 216
// /// product_id : 10650
// /// quantity : 4
// /// sell_price : 60
// /// created_at : "2021-11-25T02:53:39.000000Z"
// /// updated_at : "2021-11-25T02:54:50.000000Z"
// /// product_details : {"id":10650,"card_id":153,"category_id":598,"subcategory_id":599,"product_name":"Society","mrp_price":"70","sell_price":"60","weight":"1kg","product_image":"","description":"jzxvlkdsjklvjil","status":"1","created_at":"2021-11-23T08:26:32.000000Z","updated_at":"2021-11-23T08:26:32.000000Z"}
//
// class Cart {
//   int? id;
//   int? storeId;
//   int? userId;
//   int? productId;
//   int? quantity;
//   int? sellPrice;
//   String? createdAt;
//   String? updatedAt;
//   Product? productDetails;
//
//   Cart({
//       this.id,
//       this.storeId,
//       this.userId,
//       this.productId,
//       this.quantity,
//       this.sellPrice,
//       this.createdAt,
//       this.updatedAt,
//       this.productDetails});
//
//   Cart.fromJson(dynamic json) {
//     id = json['id'];
//     storeId = json['store_id'];
//     userId = json['user_id'];
//     productId = json['product_id'];
//     quantity = json['quantity'];
//     sellPrice = json['sell_price'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//     productDetails = json['product_details'] != null ? Product.fromJson(json['productDetails']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map['id'] = id;
//     map['store_id'] = storeId;
//     map['user_id'] = userId;
//     map['product_id'] = productId;
//     map['quantity'] = quantity;
//     map['sell_price'] = sellPrice;
//     map['created_at'] = createdAt;
//     map['updated_at'] = updatedAt;
//     if (productDetails != null) {
//       map['product_details'] = productDetails?.toJson();
//     }
//     return map;
//   }
//
//   Future<dynamic> deleteItem(
//       BuildContext context, Function(dynamic) completion) async {
//     bool networkStatus = await CommonUtills.checkNetworkStatus();
//     if (networkStatus == true) {
//       NetworkUtils _networkUtils = NetworkUtils();
//       String url = NetworkUtils.kDeleteAddress + '?address_id=${this.id ?? ''}';
//       _networkUtils.delete(url, context).then((value) {
//         CartListResponse response = CartListResponse.fromJson(value);
//         completion(response);
//       }).onError((error, stackTrace) {
//         completion(error);
//       });
//     } else {
//       completion(
//           'No internet connection available, please check and try again');
//     }
//   }
//
// }
//
// /// id : 10650
// /// card_id : 153
// /// category_id : 598
// /// subcategory_id : 599
// /// product_name : "Society"
// /// mrp_price : "70"
// /// sell_price : "60"
// /// weight : "1kg"
// /// product_image : ""
// /// description : "jzxvlkdsjklvjil"
// /// status : "1"
// /// created_at : "2021-11-23T08:26:32.000000Z"
// /// updated_at : "2021-11-23T08:26:32.000000Z"
//
// // class Product_details {
// //   int? id;
// //   int? cardId;
// //   int? categoryId;
// //   int? subcategoryId;
// //   String? productName;
// //   String? mrpPrice;
// //   String? sellPrice;
// //   String? weight;
// //   String? productImage;
// //   String? description;
// //   String? status;
// //   String? createdAt;
// //   String? updatedAt;
// //
// //   Product_details({
// //       this.id,
// //       this.cardId,
// //       this.categoryId,
// //       this.subcategoryId,
// //       this.productName,
// //       this.mrpPrice,
// //       this.sellPrice,
// //       this.weight,
// //       this.productImage,
// //       this.description,
// //       this.status,
// //       this.createdAt,
// //       this.updatedAt});
// //
// //   Product_details.fromJson(dynamic json) {
// //     id = json['id'];
// //     cardId = json['card_id'];
// //     categoryId = json['category_id'];
// //     subcategoryId = json['subcategory_id'];
// //     productName = json['product_name'];
// //     mrpPrice = json['mrp_price'];
// //     sellPrice = json['sell_price'];
// //     weight = json['weight'];
// //     productImage = json['product_image'];
// //     description = json['description'];
// //     status = json['status'];
// //     createdAt = json['created_at'];
// //     updatedAt = json['updated_at'];
// //   }
// //
// //   Map<String, dynamic> toJson() {
// //     var map = <String, dynamic>{};
// //     map['id'] = id;
// //     map['card_id'] = cardId;
// //     map['category_id'] = categoryId;
// //     map['subcategory_id'] = subcategoryId;
// //     map['product_name'] = productName;
// //     map['mrp_price'] = mrpPrice;
// //     map['sell_price'] = sellPrice;
// //     map['weight'] = weight;
// //     map['product_image'] = productImage;
// //     map['description'] = description;
// //     map['status'] = status;
// //     map['created_at'] = createdAt;
// //     map['updated_at'] = updatedAt;
// //     return map;
// //   }
// //
// // }