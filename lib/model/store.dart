import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/product.dart';

///JSON Object

/// id : 51
/// user_id : 74
/// business_name : "saini-jio-sweet-and-restaurent(सैनी-जीयो-स्वीट-एंड-रेस्टोरेंट)"
/// category_id : 48
/// cover_image : "1622970254_Maa-da-dhaba.jpg"
/// first_name : "Hanuman"
/// last_name : "Tunwal"
/// position : null
/// phone : "+919001616001"
/// telephone : "+919549953576"
/// whatsapp : "+919001616001"
/// address : null
/// email : null
/// website : null
/// location : "Medasar Bass, Medasar Bass, Didwana, Rajasthan, India"
/// company_est_date : "2018"
/// about_us : "डीडवाना का शुद्ध शाकाहारी रेस्टोरेंट और शुद्ध घी से बनी और ताजी मिठाइयां ही हमारी पहचान है।शादी और पार्टियों के ऑर्डर लिए जाते है।"
/// latitude : "27.4142793"
/// longitude : "74.60089789999999"
/// facebook_link : null
/// twitter_link : null
/// instagram_link : null
/// linkedin_link : null
/// youtube_link : null
/// pinterest_link : null
/// youtube_video1 : null
/// youtube_video2 : null
/// youtube_video3 : null
/// youtube_video4 : null
/// youtube_video5 : null
/// youtube_video6 : null
/// paytm : null
/// googlepay : null
/// phonepe : null
/// bank_name : null
/// account_name : null
/// account_number : null
/// ifsc_code : null
/// gst : null
/// paytm_qr : ""
/// googlepay_qr : ""
/// phonepe_qr : ""
/// status : "1"
/// payment_status : "1"
/// payment_date : "2021-06-13 22:31:32"
/// trail_ends : "0000-00-00 00:00:00"
/// package_expire : "2022-06-13 22:31:32"
/// shop_time : null
/// post_views : 214
/// minCartAmount : null
/// deliveryCharge : null
/// created_at : "2021-06-06T09:04:14.000000Z"
/// updated_at : "2021-08-31T08:58:15.000000Z"
/// product : [{"id":837,"card_id":51,"category_id":76,"product_name":"Daal Tadka","mrp_price":"150","sell_price":"120","weight":"Full","product_image":"1623002393_830daal tadka.jpg","description":"","status":"1","created_at":"2021-06-06T12:43:13.000000Z","updated_at":"2021-06-06T17:59:53.000000Z"},{"id":838,"card_id":51,"category_id":76,"product_name":"Daal Makhani","mrp_price":"150","sell_price":"120","weight":"Full","product_image":"1623002560_32daal makhani.jpg","description":"","status":"1","created_at":"2021-06-06T12:43:36.000000Z","updated_at":"2021-06-06T18:02:40.000000Z"},{"id":839,"card_id":51,"category_id":76,"product_name":"Daal fry","mrp_price":"100","sell_price":"80","weight":"Full","product_image":"1623002518_370Dal-Fry-Recipe.jpg","description":"","status":"1","created_at":"2021-06-06T12:43:56.000000Z","updated_at":"2021-06-06T18:01:58.000000Z"}]

class Store {
  String? id;
  int? userId;
  String? businessName;
  int? categoryId;
  String? coverImage;
  String? firstName;
  String? lastName;
  dynamic? position;
  String? phone;
  String? telephone;
  String? whatsapp;
  dynamic? address;
  dynamic? email;
  dynamic? website;
  String? location;
  String? companyEstDate;
  String? aboutUs;
  String? latitude;
  String? longitude;
  dynamic? facebookLink;
  dynamic? twitterLink;
  dynamic? instagramLink;
  dynamic? linkedinLink;
  dynamic? youtubeLink;
  dynamic? pinterestLink;
  dynamic? youtubeVideo1;
  dynamic? youtubeVideo2;
  dynamic? youtubeVideo3;
  dynamic? youtubeVideo4;
  dynamic? youtubeVideo5;
  dynamic? youtubeVideo6;
  dynamic? paytm;
  dynamic? googlepay;
  dynamic? phonepe;
  dynamic? bankName;
  dynamic? accountName;
  dynamic? accountNumber;
  dynamic? ifscCode;
  dynamic? gst;
  String? paytmQr;
  String? googlepayQr;
  String? phonepeQr;
  String? status;
  String? paymentStatus;
  String? paymentDate;
  String? trailEnds;
  String? packageExpire;
  dynamic? shopTime;
  int? postViews;
  dynamic? minCartAmount;
  dynamic? deliveryCharge;
  String? createdAt;
  String? updatedAt;
  String? shop_status;
  List<Offers>? offers;

  int? totalRating;
  int? totalFeedabck;
  List<dynamic>? feedback;
  List<Gallery>? gallery;
  List<StoreCategory>? storeCategory;
  List<Product>? products;
  String? order_delivered;

  Store(
      {this.id,
      this.userId,
      this.businessName,
      this.categoryId,
      this.coverImage,
      this.firstName,
      this.lastName,
      this.position,
      this.phone,
      this.telephone,
      this.whatsapp,
      this.address,
      this.email,
      this.website,
      this.location,
      this.companyEstDate,
      this.aboutUs,
      this.latitude,
      this.longitude,
      this.facebookLink,
      this.twitterLink,
      this.instagramLink,
      this.linkedinLink,
      this.youtubeLink,
      this.pinterestLink,
      this.youtubeVideo1,
      this.youtubeVideo2,
      this.youtubeVideo3,
      this.youtubeVideo4,
      this.youtubeVideo5,
      this.youtubeVideo6,
      this.paytm,
      this.googlepay,
      this.phonepe,
      this.bankName,
      this.accountName,
      this.accountNumber,
      this.ifscCode,
      this.gst,
      this.paytmQr,
      this.googlepayQr,
      this.phonepeQr,
      this.status,
      this.paymentStatus,
      this.paymentDate,
      this.trailEnds,
      this.packageExpire,
      this.shopTime,
      this.postViews,
      this.minCartAmount,
      this.deliveryCharge,
      this.createdAt,
      this.updatedAt,
      this.totalRating,
      this.totalFeedabck,
      this.feedback,
      this.gallery,
      this.shop_status,
      this.storeCategory,
      this.products,
      this.offers,
      this.order_delivered});

  Store.fromJson(dynamic json) {
    id = json['id'].toString();
    userId = json['user_id'];
    businessName = json['business_name'];
    categoryId = json['category_id'];
    if (json['cover_image'] != null && json['cover_image'] != "") {
      coverImage = NetworkUtils.base_image_url + json['cover_image'];
    } else {
      coverImage = null;
    }

    order_delivered = json['order_delivered']?.toString();

    firstName = json['first_name'];
    lastName = json['last_name'];
    position = json['position'];
    phone = json['phone'];
    telephone = json['telephone'];
    whatsapp = json['whatsapp'];
    address = json['address'];
    email = json['email'];
    website = json['website'];
    location = json['location'];
    companyEstDate = json['company_est_date'];
    aboutUs = json['about_us'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    facebookLink = json['facebook_link'];
    twitterLink = json['twitter_link'];
    instagramLink = json['instagram_link'];
    linkedinLink = json['linkedin_link'];
    youtubeLink = json['youtube_link'];
    pinterestLink = json['pinterest_link'];
    youtubeVideo1 = json['youtube_video1'];
    youtubeVideo2 = json['youtube_video2'];
    youtubeVideo3 = json['youtube_video3'];
    youtubeVideo4 = json['youtube_video4'];
    youtubeVideo5 = json['youtube_video5'];
    youtubeVideo6 = json['youtube_video6'];
    paytm = json['paytm'];
    googlepay = json['googlepay'];
    phonepe = json['phonepe'];
    bankName = json['bank_name'];
    accountName = json['account_name'];
    accountNumber = json['account_number'];
    ifscCode = json['ifsc_code'];
    gst = json['gst'];
    paytmQr = json['paytm_qr'];
    googlepayQr = json['googlepay_qr'];
    phonepeQr = json['phonepe_qr'];
    status = json['status'];
    paymentStatus = json['payment_status'];
    paymentDate = json['payment_date'];
    trailEnds = json['trail_ends'];
    packageExpire = json['package_expire'];
    shopTime = json['shop_time'];
    postViews = json['post_views'];
    minCartAmount = json['minCartAmount'];
    deliveryCharge = json['deliveryCharge'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    shop_status = json['shop_status'];

    totalRating = json['total_rating'];
    totalFeedabck = json['total_feedabck'];
    if (json['feedback'] != null) {
      feedback = [];
      json['feedback'].forEach((v) {
        // feedback?.add(dynamic.fromJson(v));
      });
    }
    if (json['offers'] != null) {
      offers = [];
      json['offers'].forEach((v) {
        offers?.add(Offers.fromJson(v));
      });
    }
    if (json["gallery"] != null) {
      gallery = [];
      json["gallery"].forEach((item) {
        gallery!.add(Gallery.fromJson(item));
      });
    }

    if (json["product_category"] != null) {
      storeCategory = [];
      json["product_category"].forEach((item) {
        storeCategory!.add(StoreCategory.fromJson(item));
      });
    }
    if (json["product"] != null) {
      products = [];
      json["product"].forEach((item) {
        products!.add(Product.fromJson(item));
      });
    }
    if (json["products"] != null) {
      products = [];
      json["products"].forEach((item) {
        products!.add(Product.fromJson(item));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['business_name'] = businessName;
    map['category_id'] = categoryId;
    map['cover_image'] = coverImage;
    map['first_name'] = firstName;
    map['last_name'] = lastName;
    map['position'] = position;
    map['phone'] = phone;
    map['telephone'] = telephone;
    map['whatsapp'] = whatsapp;
    map['address'] = address;
    map['email'] = email;
    map['website'] = website;
    map['location'] = location;
    map['company_est_date'] = companyEstDate;
    map['about_us'] = aboutUs;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['facebook_link'] = facebookLink;
    map['twitter_link'] = twitterLink;
    map['instagram_link'] = instagramLink;
    map['linkedin_link'] = linkedinLink;
    map['youtube_link'] = youtubeLink;
    map['pinterest_link'] = pinterestLink;
    map['youtube_video1'] = youtubeVideo1;
    map['youtube_video2'] = youtubeVideo2;
    map['youtube_video3'] = youtubeVideo3;
    map['youtube_video4'] = youtubeVideo4;
    map['youtube_video5'] = youtubeVideo5;
    map['youtube_video6'] = youtubeVideo6;
    map['paytm'] = paytm;
    map['googlepay'] = googlepay;
    map['phonepe'] = phonepe;
    map['bank_name'] = bankName;
    map['account_name'] = accountName;
    map['account_number'] = accountNumber;
    map['ifsc_code'] = ifscCode;
    map['gst'] = gst;
    map['paytm_qr'] = paytmQr;
    map['googlepay_qr'] = googlepayQr;
    map['phonepe_qr'] = phonepeQr;
    map['status'] = status;
    map['payment_status'] = paymentStatus;
    map['payment_date'] = paymentDate;
    map['trail_ends'] = trailEnds;
    map['package_expire'] = packageExpire;
    map['shop_time'] = shopTime;
    map['post_views'] = postViews;
    map['minCartAmount'] = minCartAmount;
    map['deliveryCharge'] = deliveryCharge;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['shop_status'] = shop_status;
    map['total_rating'] = totalRating;
    map['total_feedabck'] = totalFeedabck;
    if (feedback != null) {
      // map['feedback'] = feedback?.map((v) => v.toJson()).toList();
    }

    if (storeCategory != null) {
      map['product_category'] = storeCategory?.map((v) => v.toJson()).toList();
    }

    if (products != null) {
      map['product'] = products?.map((v) => v.toJson()).toList();
    }

    if (offers != null) {
      map['offers'] = offers?.map((v) => v.toJson()).toList();
    }
    return map;
  }

  List<String> getYoutubeVideo() {
    List<String> videoList = [];
    if (youtubeVideo1 != null) {
      videoList.add(youtubeVideo1);
    }

    if (youtubeVideo2 != null) {
      videoList.add(youtubeVideo2);
    }

    if (youtubeVideo3 != null) {
      videoList.add(youtubeVideo3);
    }

    if (youtubeVideo4 != null) {
      videoList.add(youtubeVideo4);
    }

    if (youtubeVideo5 != null) {
      videoList.add(youtubeVideo5);
    }

    if (youtubeVideo6 != null) {
      videoList.add(youtubeVideo6);
    }

    return videoList;
  }
}

class Gallery {
  int? id;
  int? cardId;
  String? galleryImage;
  String? createdAt;
  String? updateAt;

  Gallery.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    cardId = json["card_id"];

    if (json['gallery_image'] != null && json['gallery_image'] != "") {
      galleryImage = NetworkUtils.base_image_url + json['gallery_image'];
    } else {
      galleryImage = null;
    }

    createdAt = json["created_at"];
    updateAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "card_id": cardId,
      "gallery_image": galleryImage,
      "created_at": createdAt,
      "updated_at": updateAt
    };
  }
}

class StoreCategory {
  int? id;
  int? userId;
  String? name;
  String? slug;
  String? categoryImage;
  String? categoryIcon;
  String? status;
  int? pcatOrder;
  String? createdAt;
  String? updatedAt;

  StoreCategory(
      {this.id,
      this.name,
      this.slug,
      this.categoryImage,
      this.categoryIcon,
      this.status,
      this.pcatOrder,
      this.createdAt,
      this.updatedAt});

  StoreCategory.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    if (json['category_image'] != null && json['category_image'] != "") {
      categoryImage = NetworkUtils.base_image_url + json['category_image'];
    } else {
      categoryImage = null;
    }

    categoryIcon = json['category_icon'];
    status = json['status'];
    pcatOrder = json['pcat_order'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['slug'] = slug;
    map['category_image'] = categoryImage;
    map['category_icon'] = categoryIcon;
    map['status'] = status;
    map['cat_order'] = pcatOrder;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }
}
class Offers {
  int? id;
  int? cardId;
  String? offerName;
  String? offerImage;
  String? createdAt;
  String? updatedAt;

  Offers({
    this.id,
    this.cardId,
    this.offerName,
    this.offerImage,
    this.createdAt,
    this.updatedAt});

  Offers.fromJson(dynamic json) {
    id = json['id'];
    cardId = json['card_id'];
    offerName = json['offer_name'];
    offerImage = json['offer_image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['card_id'] = cardId;
    map['offer_name'] = offerName;
    map['offer_image'] = offerImage;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

}
