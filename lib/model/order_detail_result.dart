import 'package:true_digital_store/model/orderlist_dto.dart';

class OrderDetailDto {
  bool? status;
  String? message;
  OrderData? data;

  OrderDetailDto({this.status, this.message, this.data});

  OrderDetailDto.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? OrderData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}
