import 'package:flutter/material.dart';

Size screenSize = Size(0, 0);
double screenHeightConstant = 0;
double fontConstant = 0;

void appSizeInit(context) {
  screenSize = MediaQuery.of(context).size;
//  screenHeightConstant = screenSize.height * 0.0188;
  screenHeightConstant = screenSize.width * 0.031;
//  screenHeightConstant = (screenSize.height / screenSize.width) * 7.5;
  fontConstant = screenHeightConstant / 12.5;

  print(
      "\n-----------------Device-----------------\n\tHeight : ${screenSize.height.round()}\n\tWidth : ${screenSize.width.round()}\nscreenHeightConstant:${screenHeightConstant.round()}\nfontConstant:${fontConstant.round()}\n--------------------");
}

class AppSize {
  static get s1 => 1.0; //~1.0

  static get s2 => 2.0; //~2.0

  static get extraSmall => screenHeightConstant / 2; //~5.0

  static get small => screenHeightConstant; //~10.0

  static get smallMedium => (screenHeightConstant * 3) / 2; //~15.0

  static get medium => screenHeightConstant * 2; //~20.0

  static get mediumLarge => (screenHeightConstant * 5) / 2; //~25.0

  static get large => screenHeightConstant * 3; //~30.0

  static get extraLarge => screenHeightConstant * 4; //~40.0

  static get xxL => screenHeightConstant * 5; //~50.0

  static get s60 => screenHeightConstant * 6; //~60.0

  static get s70 => screenHeightConstant * 7; //~70.0

  static get s80 => screenHeightConstant * 8; //~80.0
  static get s90 => screenHeightConstant * 9; //~80.0
  static get s100 => screenHeightConstant * 10; //~80.0
}

class AppFontSize {
  static get s5 => fontConstant * 5;

  static get s6 => fontConstant * 6;

  static get s7 => fontConstant * 7;

  static get s8 => fontConstant * 8;

  static get s9 => fontConstant * 9;

  static get s10 => fontConstant * 10;

  static get s11 => fontConstant * 11;

  static get s12 => fontConstant * 12;

  static get s13 => fontConstant * 12;

  static get s14 => fontConstant * 14;

  static get s15 => fontConstant * 15;

  static get s16 => fontConstant * 16;

  static get s17 => fontConstant * 17;

  static get s18 => fontConstant * 18;

  static get s19 => fontConstant * 19;

  static get s20 => fontConstant * 20;

  static get s21 => fontConstant * 21;

  static get s22 => fontConstant * 22;

  static get s23 => fontConstant * 23;

  static get s24 => fontConstant * 24;

  static get s25 => fontConstant * 25;

  static get s26 => fontConstant * 26;

  static get s27 => fontConstant * 27;

  static get s28 => fontConstant * 28;

  static get s29 => fontConstant * 29;

  static get s30 => fontConstant * 30;

  static get s31 => fontConstant * 31;

  static get s32 => fontConstant * 32;

  static get s36 => fontConstant * 36;
}