import 'package:intl/intl.dart';

int versionCode = 1;
String appVersion = "1.0.0";

enum ButtonType { positiveButton, negativeButton }

class Constants {
  /*
  Preference keys
   */
  static const String kCurrentUser = "kCurrentUser";

  static const String CURRENCY_SIGN = '₹';

  static const welcomeTostagePicker = 'Welcome to Stage Picker';
  static const auditioners = 'Auditioners';
  static const or = 'Or';
  static const stagePicker = 'Stage\nPicker';

  static const timeOutInSeconds = 61;

  /*Alert Title*/
  static const String alertTitle = "Alert";

  static const String create = 'CREATE ';

  static const String account = 'ACCOUNT';

  static const String acthound = 'BY ACTHOUND';

  static const String acthoundTitle = 'ACTHOUND LLC';

  static const String stageName = 'STAGE NAME';

  static const String termsOfService = 'TERMS OF SERVICE';

  static const String companyVerification = 'COMPANY VERIFICATION';

  static const String companyVerificationQ1 =
      '1. Tell us about your company and whhy you are interested in creating an account.';

  static const String verificationQ1 =
      "1. Email or phhone number associated with the account for verification";

  static const String verificationQ2 =
      "2. Is the company linked to this account an actual registered business?";

  static const String verificationQ3 =
      "3.Please  provide links to at least two websites that will help us identify you";

  static const String location = 'LOCATION';

  static const String age = 'AGE';

  static const String gender = 'GENDER';

  static const String website = 'WEBSITE';

  static const String height = 'HEIGHT';

  static const stagePickerTest = 'STAGEPICKER TEST \n AUDITION';

  static const String theaterShow = 'THEATER SHOW AUDITION';

  static const String Profile = 'PROFILE';

  static const String welcomeBack = 'WELCOME BACK !';

  static const String alreadyHaveAnAccount = 'ALREADY HAVE AN ACCOUNT?';

  static const String negativeButtonText = "Cancel";

  static const String positiveButtonText = "Ok";

  static const String notConnected = "No Internet ! ";

  static const String tryAgain = "Try Again ! ";

  static const String done = "Done";

  static const String back = "Back";

  static const String audiotionId = 'audiotionId';

  static const String timeOutMessage =
      "Timeout !\n Please check if you have good internet connection!\n Try Again";

  static const String errorMessage =
      "Something Went Wrong !\n Please check if you have good internet connection!\n Try Again";

  static const String somethingWentWrong = "Something Went Wrong!";

  static const String wip = "Work in Progess";

  static const String noData = "No Data";

  static const String exit = 'Exit';

  static const String next = 'NEXT';

  static const String enterEmail = "Email";

  static const String id = "id";

  static const String token = 'token';

  static const String enterValidEmail = "Enter Valid Email";

  static const String enterValidMessage = "Enter Valid Message";

  static const String enterValidAmount = "Enter Valid Amount";

  static const String enterPassword = "Password";

  static const String enterValidPassword = "Enter Valid Password";

  static const String enterConfirmPassword = "Confirm Password";

  static const String enterValidConfirmPassword =
      "Password and Confirm Password should be identical";

  static const String enterPhone = "Enter Mobile Number";

  static const String mobile = "Mobile Number";

  static const String enterOTP = "Enter OTP";

  static const String otp = 'OTP';

  static const String save = 'Save';

  static const String enterValidPhone = "Enter Valid Mobile Number";

  static const String unAuthenticated = 'Unauthenticated';

  static const String emptyMessage = "Can't be Empty";

  static const String enterName = "Name";

  static const String enterValidName = "Enter Valid Name";

  static const String enterValidOTP = "Enter Valid OTP";

  static const String register = 'Register';

  static const String login = 'Log In';

  static const String getStarted = 'GET STARTED';

  static const String activeAudition = '3 ACTIVE AUDITION';

  static const String logOut = 'Log Out';

  static const String profileUpdate = 'Profile Update';

  static const String changePassword = 'Change Password';

  static const String cancel = 'Cancel';

  static const String forgotPassword = 'Forgot Password';

  //Login Options
  static const String producerLogin = 'Producer Login';

  static const String auditionerLogin = 'Auditioner Login';

  static const String notAMember = 'Not a member? ';

  static const String signUp = 'Sign Up';

  static const String letStart = 'LETS GET STARTED!';

  static const String iAm = 'I AM...';

  static const String version = 'Version 0.0.1';

  static const String actHound = 'Acthound LLC';

  //Autioner SignUp
  static const String auditioner = 'Auditioner';

  static const String anAuditioner = 'AN AUDITIONER';

  static const String profilePhoto = 'Profile Photo';

  static const String fName = "First Name";

  static const String lName = "Last Name";

  static const String phone = "Phone";

  static const String producerForgotPassword = 'Producer';

  static const String camera = 'Camera';

  static const String gallery = 'Gallery';

  //change Password

  static const String currentPassword = 'currentPassword';

  static const String newPassword = 'newPassword';

  static const String confirmPassword = 'confirmPassword';

  //Producer Signup
  static const String producer = 'Producer';

  static const String AProducer = 'A PRODUCER';

  static const String company = 'Company';

  static const String userType = 'type';

  static const String selectImage = 'Please select image.';

  static const String registeredSuccessfully = "Register Successfully";

  static const String auditionSuccessfully = "Audition added successfully";

  //Producer Login
  static const String phoneOrEmail = 'Phone or Email';

  //Auditioner Dashboard
  static const String postAudition = 'POST AUDITION';

  static const String activeAuditions = 'Active Auditions';

  //Post Audtion
  static const String title = 'Title';

  static const String tagLine = 'Tag Line';

  static const String startDate = 'Start Date';

  static const String endDate = 'End Date';

  static const String category = 'Category';

  static const String roles = 'Roles';

  static const String participationNumber = 'Participation Number';

//  static const String location  = 'Location';

  static const String description = 'Description';

  static const String tags = 'Tags';

  static const String post = 'Post';

  //Audition Feeds
  static const String newestPost = 'NEWEST POST';

  static const String readMore = 'Read More..';

  static const String auditionList = 'AUDITION LIST';

  // searching

  static const String search = 'Search';

  static const String ethiniCity = 'Ethinicity:';

  static const String selectEthiniCity = 'Select Ethinicity';

  static const String skills = 'Skills:';

  static const String selectSkills = 'Select Skills';

  static const String typeKeyword = 'Type Keyword:';

  static const String selectTypeKeyword = 'TypeKeyword(s)';

  static const String saveSearchOptions = 'Save search options';

  static const String type = 'type';

  static const String edit = 'Edit';

  static const String preference = 'Preference';

  //setting screen

  static const String setting = 'Settings';

  static const String email = 'Email:';

  static const String emails = 'email';

  static const String emailsPhone = 'EMAIL OR PHONE ';

  static const String emailEnter = 'Enter Email ';

  static const String firstName = 'firstName';

//  static const String fName = 'First Name';

  static const String phones = 'Phone:';

  static const String lastName = 'lastName';

  static const String viewList = 'VIEW LIST';

  static const String logoutConfirm = 'Do you really want to logout?';

  //FILTER SCREEN

  static const String filter = 'Filter Result';

//  static const String gender = 'Gender:';

  static const String allGender = 'All Genders';

  static const String tarnsGender = 'Tarnsgender:';

  static const String locationRadius = 'location & Radius:';

  static const String selectLocation = 'selectLocation';

  static const String radius = 'Radius(miles):50';

  static const String number = '0';

  static const String number1 = '100';

  //production Type screen

  static const String productionType = 'Production Type:';

  static const String selectProductionType = 'Select Production Type(s)';

  static const String selectRoleType = 'Select Role Type';

  static const String RoleType = 'Role Type:';

  static const String compensation = 'Compensation:';

  static const String selectCompensation = 'Select Compensation';

  static const String UnionStatus = 'Union Status:';

  static const String submit = 'SUBMIT';

  static const String selectUnionStatus = 'Select Union Status';

  //switch Account Screen

  static const String companyProfile = 'COMPANY PROFILE';

  static const String switchTo = "Switch To";

  static const String switchAccount = "Switch Account";

  static const String auditionerAccount = " Auditioners Account";

  static const String stagePickerTestAudition = " Stagepicker Test Audition";

  static const String entertainmentJobs = "Entertainment Jobs";

  static const String productionDes = " production Description:";

  static const String expires = "Expires";

  static const String theaterShowDes =
      "StagePicker is testing it's first theater show audition app. Ages 18-30 must be in the NYC ares and.... ";

  //Auditioner Dashboard
  static const String myAuditions = 'My Auditions';

  // Participation Details

  static const String active = 'active in 1 hour';

  static const String smsNotification = 'SMS Notification';

  static const String notification = 'NOTIFICATIONS :';

  static const String upcomingAudition = 'VIEW YOUR UPCOMING';

  static const String audition = 'AUDITION ';

  static const String auditionFeed = 'AUDITION FEED';

  static const String activeParticipant = '(40)Participants:';

  //user Login

  static const String userName = 'username';

  static const String password = 'password';

  static const String invalidCredentials = 'Please enter valid credentials';

  static const String grantType = "grant_type";

  static const String invalidToken = 'Invalid Token';

  static const String loginMessage = 'Login Successfully';

  // Api Massage
//  VERIFICATION

  static const String verificationTitle = "VERIFICATION";

  static const String switchAccountMassage =
      "This Account Type Successfully Change";

  static const String verification =
      'We hold security and safety in high regard to'
      ' our customer platforms, and classes. As such, it is our top priority to ensure protection of the highest caliber we can provide. \n \nBy applying for verification, you agree to our terms of service. ';

  static const String verificationAgree =
      "I agree to the verification terms of serivce";

  static const String verificationApproved = 'Verification has been approved';

  static const String verificationDenied = 'Verification has been denied';

  static const String verificationDeniedDes =
      'Acthound could not verify information presented, you are ineligible for verification on this on this account and are unable to reapply for 2 months.';
}

class UserProgress {
  static const String newUser = "new_user";

  static const String registered = "registered";

  static const String profileCreated = "profile_created";

  static const String invalid = "invalid";
}

class ApiConstant {
  static const String email = "email";

  static const String firstName = "firstName";

  static const String lastName = "lastName";

  static const String phoneNo = "phoneNo";

  static const String password = "password";

  static const String isTermAccepted = "isTermAccepted";

  static const String company = "company";

  static const String type = "type";

  static const String image = "image";

  // audition list
  static const String ethnicityId = 'ethnicityId';

  static const String skillId = 'skillId';

  static const String keyword = 'keyword';
}

List<Map> categoriesList = [
  {
    "id": 'film',
    "title": "Film - Feature Film",
  },
  {
    "id": 'theater',
    "title": "Theater - Plays",
  },
  {
    "id": 'tv',
    "title": "TV & Video",
  },
  {
    "id": 'commercials',
    "title": "Commercials",
  },
  {
    "id": 'modeling',
    "title": "Modeling",
  },
  {
    "id": 'performingArt',
    "title": "Performing Arts",
  },
  {
    "id": 'event',
    "title": "Event & Representation",
  },
  {
    "id": 'voiceOver',
    "title": "Voiceover",
  },
  {
    "id": 'entertainment',
    "title": "Entertainment Jobs & Crew",
  },
];

List<Map<String, List<Map>>> subCategoriesList = [
  {
    "film": [
      {"id": 1, "title": "Short Film"},
      {"id": 2, "title": "Student Film"},
    ]
  },
  {
    "theater": [
      {"id": 3, "title": "Musicals"},
      {"id": 4, "title": "Chorus Calls"},
    ]
  },
  {
    "tv": [
      {"id": 5, "title": "Scripted TV & Video"},
      {"id": 6, "title": "Music Videos"},
      {"id": 7, "title": "Reality TV & Documentary"},
      {"id": 8, "title": "Multimedia"},
    ]
  },
  {
    "commercials": [
      {"id": 9, "title": "National Commercials"},
      {"id": 10, "title": "Local Commercials"},
      {"id": 11, "title": "Spec Commercials"},
      {"id": 12, "title": "Online Commercials & Promos"},
    ]
  },
  {
    "modeling": [
      {"id": 13, "title": "Print Modeling"},
      {"id": 14, "title": "Fashion & Runaway Modeling"},
      {"id": 15, "title": "Commercials & Fit Modeling"},
      {"id": 16, "title": "Promotional & Event Modeling"},
    ]
  },
  {
    "performingArt": [
      {"id": 17, "title": "Dancers & Choreographers"},
      {"id": 18, "title": "Cabaret & Variety"},
      {"id": 19, "title": "Singers"},
      {"id": 20, "title": "Musicians & Composers"},
      {"id": 21, "title": "Comedians & Improv"},
      {"id": 22, "title": "Theme Parks & Cruise Lines"},
    ]
  },
  {
    "event": [
      {"id": 23, "title": "Agents & Managers"},
      {"id": 24, "title": "Workshops"},
      {"id": 25, "title": "Competitions"},
      {"id": 26, "title": "Festivals & Events"},
      {"id": 27, "title": "Groups & Membership Companies"},
    ]
  },
  {
    "voiceOver": [
      {"id": 28, "title": "Commercial & Film Voiceover"},
      {"id": 29, "title": "Animation & Videogame Voiceover"},
      {"id": 30, "title": "Audio Books"},
      {"id": 31, "title": "Radio Podcast"},
    ]
  },
  {
    "entertainment": [
      {"id": 32, "title": "Stage Staff & Tech"},
      {"id": 33, "title": "Film & TV Crew"},
      {"id": 34, "title": "Writers"},
      {"id": 35, "title": "Gigs"},
    ]
  },
];

class PostApiConstant {
  static const String title = "title";

  static const String tagLine = "tagline";

  static const String startDate = "startDate";

  static const String endDate = "endDate";

  static const String roleType = "roleType";

  static const String participationNumber = "participationNumber";

  static const String location = "location";

  static const String description = "description";

  static const String tags = "tags";

  static const String category = "category";

  static const String latitude = "latitude";

  static const String longitude = "longitude";

  static const String image = "image";

  static const String productionType = "productionType";

  static const String compensation = "compensation";

  static const String union = "union";

  static const String ethnicity = "ethnicity";

  static const String skills = "skills";
}

bool isEmail(String em) {
  String p =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

  RegExp regExp = new RegExp(p);

  return regExp.hasMatch(em);
}

String dateConvert(String d) {
  DateTime date = DateTime.parse(d);
  final DateFormat formatter = DateFormat('dd/MM/yyyy\nE\nhh:mm');
  return formatter.format(date);
}

String dateConvertDDMMYYYY(String d) {
  DateTime date = DateTime.parse(d);
  final DateFormat formatter = DateFormat('dd/MM/yyyy');
  return formatter.format(date);
}
