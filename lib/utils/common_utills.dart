import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app_assets.dart';

class CommonUtills {
  static Future<bool> checkNetworkStatus([BuildContext? context]) async {
    bool connected = false;

    var connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult == ConnectivityResult.mobile) {
      connected = true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      connected = true;
    }
    return connected;
  }

  static errortoast(BuildContext context, String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 0,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static messagetoast(BuildContext context, String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black26,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static successtoast(BuildContext context, String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static bool validateemail(String email) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!(regex.hasMatch(email)))
      return false;
    else
      return true;
  }

  static bool isempty(BuildContext context, TextEditingController controller,
      String msg, int length) {
    String text = controller.text.toString();
    if (text.length < length || controller.text.isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  static BoxDecoration whiteboxroundeddecoration() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
          bottomLeft: Radius.circular(16.0),
          bottomRight: Radius.circular(16.0)),
    );
  }

  static void showprogressdialogcomplete(BuildContext context, bool show) {
    if (show == true) {
      try {
        showGeneralDialog(
            context: context,
            barrierDismissible: false,
            barrierLabel:
                MaterialLocalizations.of(context).modalBarrierDismissLabel,
            barrierColor: Colors.black45,
            transitionDuration: const Duration(milliseconds: 200),
            pageBuilder: (BuildContext buildContext, Animation animation,
                Animation secondaryAnimation) {
              return Center(
                  child: Container(
                      height: 65,
                      width: 65,
                      child: CircularProgressIndicator(
                        color: Colors.red,
                        // size: 50.0,
                      )));
            });
      } catch (e) {
        print(e);
      }
    } else {
      Navigator.of(context).pop();
    }
  }

  static progressdialogbox() {
    return Center(
        child: Container(
            height: 65,
            width: 65,
            child: SpinKitChasingDots(
              color: Colors.green,
              size: 50.0,
            )));
  }

  static dateTimeParse(String date) {
    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
    String dateis = DateFormat("MMM dd, yyyy hh:mm a").format(tempDate);

    return dateis;
  }

  static noInternetToast(BuildContext context) {
    CommonUtills.errortoast(
        context, "Internet connection appear to be offline!");
  }

  static Future<void> makePhoneCall(String url) async {
    print('Phone number : ' + 'tel://$url');
    if (await canLaunch('tel://${url}')) {
      await launch('tel://${url}');
    } else {
      throw 'Could not launch $url';
    }
  }

  static Future<void> openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  static Future<void> openMapWithAddrees(String address) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=${address}';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  static Future<void> openLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not open the link.';
    }
  }

  static Widget imageViewWidget(String imageUrl, double width, double height,
      {BoxFit fit = BoxFit.none}) {
    if (imageUrl.isEmpty) {
      return Image.asset(
        Assets.noimage,
        width: width,
        height: height,
        fit: fit,
      );
    } else {
      return Image.network(
        imageUrl,
        width: width,
        height: height,
        fit: fit,
      );
    }
  }

  // static Future<LocationData?> getCurrentLocation() async {
  //   Location location = Location();
  //   bool _serviceEnabled;
  //   PermissionStatus _permissionGranted;
  //
  //   _serviceEnabled = await location.serviceEnabled();
  //   if (!_serviceEnabled) {
  //     _serviceEnabled = await location.requestService();
  //     if (!_serviceEnabled) {
  //       return null;
  //     }
  //   }
  //
  //   _permissionGranted = await location.hasPermission();
  //   if (_permissionGranted == PermissionStatus.denied) {
  //     _permissionGranted = await location.requestPermission();
  //     if (_permissionGranted != PermissionStatus.granted) {
  //       return null;
  //     }
  //   }
  //   LocationData locData = await location.getLocation();
  //   print("getting location data");
  //   return LocationData.fromMap(
  //       {"latitude": 27.3981082, "longitude": 74.56967329999999});
  //   ;
  // }

  static void confirmationAlert(BuildContext context, String title,
      String message, Function(bool action) action) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: message.isNotEmpty ? Text(message) : null,
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  action(false);
                },
                child: Text('No'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  action(true);
                },
                child: Text(
                  'Yes',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ],
          );
        });
  }
}
