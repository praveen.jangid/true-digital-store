import 'package:flutter/material.dart';
import 'dart:math';

import 'app_assets.dart';



class FullScreenLoader extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: new Container(
        color: Colors.transparent,
        width: double.maxFinite,
        height: double.maxFinite,
        child: new Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        ),
      ),
      onWillPop: () {
        return Future<bool>.value(false);
      },
    );
  }
}

class InternetLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: new Container(
        color: Colors.transparent,
        width: double.maxFinite,
        height: double.maxFinite,
        child: new Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: new SimpleLoadingWidget(
              image: Assets.internet,
              size: 100.0,
            ),
          ),
        ),
      ),
      onWillPop: () {
        return Future<bool>.value(false);
      },
    );
  }
}

class LoadingWidget extends StatefulWidget {
  final double radius;
  final double iconSize;
  final double size;

  LoadingWidget({this.radius = 60.0, this.iconSize = 30.0, this.size = 100.0});

  @override
  _LoadingWidgetState createState() => _LoadingWidgetState();
}

class _LoadingWidgetState extends State<LoadingWidget> with SingleTickerProviderStateMixin {
  late Animation<double> animationRotation;
  late Animation<double> animationRadiusIn;
  late Animation<double> animationRadiusOut;
  late AnimationController controller;

  late double radius;
  late double dotRadius;
  late double size;

  @override
  void initState() {
    super.initState();

    radius = widget.radius;
    dotRadius = widget.iconSize;
    size = widget.size;

    print(dotRadius);

    controller = AnimationController(
        lowerBound: 0.0, upperBound: 1.0, duration: const Duration(milliseconds: 3000), vsync: this);

    animationRotation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 1.0, curve: Curves.linear),
      ),
    );


    animationRadiusIn = Tween(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.75, 1.0, curve: Curves.elasticIn),
      ),
    );

    animationRadiusOut = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 0.25, curve: Curves.elasticOut),
      ),
    );

    controller.addListener(() {
      setState(() {
        if (controller.value >= 0.75 && controller.value <= 1.0)
          radius = widget.radius * animationRadiusIn.value;
        else if (controller.value >= 0.0 && controller.value <= 0.25) radius = widget.radius * animationRadiusOut.value;
      });
    });

    controller.repeat();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      //color: Colors.black12,
      child: new Center(
        child: new Center(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
//              Image(
//                image: AssetImage(
//                  Assets.logo_round,
//                ),
//                width: size / 2,
//                height: size / 2,
//                fit: BoxFit.fitWidth,
//              ),
              new RotationTransition(
                turns: animationRotation,
                child: Stack(
                  children: <Widget>[
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.lowWastage,
//                        assetName: Assets.lowWastage,
//                      ),
                      offset: Offset(
                        radius * cos(0.0),
                        radius * sin(0.0),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.chemicalFree,
//                        assetName: Assets.lessChemical,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 1 * pi / 4),
                        radius * sin(0.0 + 1 * pi / 4),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.waterBlue,
//                        assetName: Assets.conservesWater,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 2 * pi / 4),
                        radius * sin(0.0 + 2 * pi / 4),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.healthierOpinion,
//                        assetName: Assets.healthierOpinion,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 3 * pi / 4),
                        radius * sin(0.0 + 3 * pi / 4),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.healthierOpinion,
//                        assetName: Assets.healthierOpinion,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 4 * pi / 4),
                        radius * sin(0.0 + 4 * pi / 4),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.healthierOpinion,
//                        assetName: Assets.healthierOpinion,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 5 * pi / 4),
                        radius * sin(0.0 + 5 * pi / 4),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.healthierOpinion,
//                        assetName: Assets.healthierOpinion,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 6 * pi / 4),
                        radius * sin(0.0 + 6 * pi / 4),
                      ),
                    ),
                    new Transform.translate(
//                      child: BadgeWidget(
//                        backGroundColor: AppTheme.colors.healthierOpinion,
//                        assetName: Assets.healthierOpinion,
//                      ),
                      offset: Offset(
                        radius * cos(0.0 + 7 * pi / 4),
                        radius * sin(0.0 + 7 * pi / 4),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class IconWidget extends StatelessWidget {
  final double? size;
  final Color? color;
  final IconData? icon;

  IconWidget({this.size, this.color, this.icon});

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Icon(
        icon,
        size: size,
        color: color,
      ),
    );
  }
}

class SimpleLoadingWidget extends StatelessWidget {
  final double? size;
  final String? image;
  final Color? color;

  const SimpleLoadingWidget({
    Key? key,
    this.size = 150.0,
    this.image,
    this.color,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      child: new Image(
        color: color,
        image: new AssetImage(image ?? Assets.internet),
      ),
    );
  }
}