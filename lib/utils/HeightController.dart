import 'package:flutter/material.dart';

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';



class HeightController extends StatefulWidget {
  @override
  _HeightControllerState createState() => _HeightControllerState();
}

class _HeightControllerState extends State<HeightController> {
  Map<String, String> selectedVal = {};

  RulerPickerController? _rulerPickerController;
  double heightis = 110;

  @override
  void initState() {
    super.initState();
    _rulerPickerController = RulerPickerController(value: 0);
    setState(() {
      // if (holdValues['Height'] != null) {
      //   selectedVal = holdValues['Height'];
      // }
    });

  }
String _getFeetInch() {
    double INCH_IN_CM = 2.54;
    int numInches = (heightis / INCH_IN_CM).roundToDouble().toInt();
    int feet = (numInches / 12).toInt();
    int inches = numInches % 12;
    return "${feet}" + "." + "${inches}";
}

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Height',
            style: TextStyle(
              color: Colors.white,
              fontFamily: "Nunito",
              fontWeight: FontWeight.w500,
              fontSize: 18.0,
            ),
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: null,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.clear,
                color: Colors.white,
              ),
              onPressed: () {
                // do something
                // Navigator.of(context).popUntil((route){
                //   return route.settings.name == BASIC_INFORMATION_CONTROLLER;
                // });
                Navigator.pop(context);
              },
            )
          ],
        ),
        body: SingleChildScrollView(
            child: Container(
                padding:
                EdgeInsets.only(left: 0, right: 0, top: 10, bottom: 10),
                child: Column(children: [
                  Padding(
                      padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 0),
                      child: Stack(children: [
                        Stack(
                          children: [
                            Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                elevation: 2,
                                shadowColor: Colors.white,
                                child: Container(
                                  height: MediaQuery. of(context). size. height - 120,

                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Container(

                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 30,
                                              bottom: 30,
                                              left: 0,
                                              right: 0),
                                          child: Center(
                                            child: ClipRRect(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(80.0)),
                                                child: Container(
                                                  width: 120,
                                                  height: 120,
                                                  color: Colors.white,
                                                  child: Center(
                                                    child: Image.asset(
                                                      'assets/images/heightWithoutBg.png',
                                                      width: 50,
                                                      //height: 80,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                )),
                                          ),
                                        ),
                                        width: double.infinity,
                                        decoration: new BoxDecoration(
                                            color: Colors.black12,
                                            borderRadius: new BorderRadius.only(
                                              topLeft:
                                              const Radius.circular(15.0),
                                              topRight:
                                              const Radius.circular(15.0),
                                            )),
                                      ),
                                      Stack(
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [

                                              RulerPicker(
                                                  controller: _rulerPickerController,
                                                  backgroundColor: Colors.transparent,
                                                  onValueChange: (value) {
                                                    setState(() {
                                                      heightis = value + 110;
                                                      selectedVal['val1'] =
                                                      '${heightis.toInt()}';
                                                      selectedVal['val2'] =
                                                          _getFeetInch();
                                                      selectedVal['type'] = 'Height';
                                                    });
                                                  },
                                                  width: 80,
                                                  height: 330,
                                                  fractionDigits: 5,
                                                selectedValue: double.parse( selectedVal['val'] ?? '110'),

                                                ),
                                              SizedBox(width: 10,),
                                            ],
                                          ),
                                          Positioned(
                                            left: 15,
                                            top: 130,
                                            child: Text(
                                              '${heightis.toInt()} cm',
                                              style: TextStyle(
                                                color: Colors.orange,
                                                fontFamily: "Nunito",
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            left: 15,
                                            top: 180,
                                            child: Text(
                                              _getFeetInch(),
                                              style: TextStyle(
                                                color: Colors.orange,
                                                fontFamily: "Nunito",
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            top: 165,
                                            right: 15,
                                            left: 15,
                                            child: LayoutBuilder(
                                              builder: (BuildContext context, BoxConstraints constraints) {
                                                final boxWidth = constraints.constrainWidth();
                                                final dashWidth = 4.0;
                                                final dashHeight = 1;
                                                final dashCount = (boxWidth / (2 * dashWidth)).floor();
                                                return Flex(
                                                  children: List.generate(dashCount, (_) {
                                                    return SizedBox(
                                                      width: dashWidth,
                                                      height: 1,
                                                      child: DecoratedBox(
                                                        decoration: BoxDecoration(color: Colors.orange),
                                                      ),
                                                    );
                                                  }),
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  direction: Axis.horizontal,
                                                );
                                              },
                                            ),
                                          )
                                        ],
                                      ),


                                     // Spacer(),
                                      SizedBox(
                                        height: 25,
                                      ),
                                      _clickButton(),
                                      SizedBox(
                                        height: 25,
                                      )
                                    ],
                                  ),
                                )),
                            Positioned(
                              bottom: 5,
                              right: 5,
                              child: Image.asset(
                                'assests/curve.png',
                                height: 75,
                                  color: Colors.orange

                              ),
                            ),
                          ],
                        )
                      ]))
                ]))));
  }



  Widget _clickButton() {

    return FloatingActionButton(
        backgroundColor: Colors.red,
        onPressed: () async {
          setState(() {
        //    holdValues['Height'] = selectedVal;
          });
          print(selectedVal);
          Navigator.pop(this.context, selectedVal);
          // Navigator.pushNamed(context, DASHBOARD_SCREEN)
          // Navigator.pushNamed(
          //     context,
          //     EXERCISE_CONTROLLER,
          //     arguments:
          //     BasicInfoType
          //         .exercise)
          //     .then((value) {
          //
          // });
        },
        tooltip: 'Click',
        child: Icon(
          Icons.arrow_forward_ios,
          color: Colors.white,
        ));
  }
}





/// a triangle painter
class _TrianglePainter extends CustomPainter {
  final double lineSize;

  _TrianglePainter({this.lineSize = 16});
  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(lineSize, 0);
    path.lineTo(lineSize / 2, tan(pi / 3) * lineSize / 2);
    path.close();
    Paint paint = Paint();
    paint.color = Color.fromARGB(255, 118, 165, 248);
    paint.style = PaintingStyle.fill;
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

/// The controller for the ruler picker
/// init the ruler value from the controller
/// 用于 RulerPicker 的控制器，可以在构造函数里初始化默认值
class RulerPickerController extends ValueNotifier<num> {

  RulerPickerController({num value = 0.0}) : super(value);
  num get value => super.value;
  set value(num newValue) {
    super.value = newValue;
  }
}

typedef void ValueChangedCallback(num value);

/// RulerPicker 标尺选择器
/// [width] 必须是具体的值，包括父级container的width，不能是 double.infinity，
/// 可以传入MediaQuery.of(context).size.width
class RulerPicker extends StatefulWidget {
  final ValueChangedCallback? onValueChange;
  final double? width;
  final double? height;
  final Color backgroundColor;
  /// the marker on the ruler, default is a arrow
  final Widget? marker;
  double? _value, selectedValue;

  /// the fraction digits of the picker value
  int fractionDigits;
  RulerPickerController? controller;
  RulerPicker({
    @required this.onValueChange,
    @required this.width,
    @required this.height,
    this.backgroundColor = Colors.white,
    this.fractionDigits = 0,
    this.controller,
    this.marker,
    this.selectedValue
  });
  @override
  State<StatefulWidget> createState() {
    return RulerPickerState();
  }
}

// todo 实现 animateTo
class RulerPickerState extends State<RulerPicker> {
  double lastOffset = 0;
  bool isPosFixed = false;
  String? value;
  ScrollController? scrollController;

  /// default mark
  Widget mark() {
    /// default mark arrow
    Widget triangle() {
      return SizedBox(
        width: 16,
        height: 16,
        child: CustomPaint(
          painter: _TrianglePainter(),
        ),
      );
    }

    return Container(
      child: SizedBox(
        width: 16,
        height: 34,
        child: Stack(
          children: <Widget>[
            triangle(),
            Container(
              width: 3,
              height: 34,
              margin: EdgeInsets.only(left: 6),
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      width: widget.width,
      height: widget.height,
      color: widget.backgroundColor,
      child: Stack(
        children: <Widget>[
          Listener(
            onPointerDown: (event) {
              FocusScope.of(context).requestFocus(new FocusNode());
              isPosFixed = false;
            },
            onPointerUp: (event) {},
            child: NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                } else if (scrollNotification is ScrollUpdateNotification) {
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPosFixed) {
                    isPosFixed = true;
                    fixPosition((scrollNotification.metrics.pixels / 5)
                        .roundToDouble() *
                        5);
                    scrollController?.notifyListeners();
                  }
                }
                return true;
              },
              child: ListView.builder(
                controller: scrollController,
                scrollDirection: Axis.vertical,
                itemCount: 200,
                itemBuilder: (BuildContext context, int index) {
                  return Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      // constraints: BoxConstraints(maxWidth: 10),
                      padding: index == 0
                          ? EdgeInsets.only(
                        top: (widget.height ?? 0) / 2,
                      ) :
                          index == 199 ?  EdgeInsets.only(
                            bottom: (widget.height ?? 0) / 2,
                          ) : EdgeInsets.zero,
                      child: Container(
                        //width: 10,
                        height: 10,
                        child: Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            Container(
                              width: index % 5 == 0 ? 32 : 20,
                              height: index % 5 == 0 ? 2 : 1,
                              color: Colors.grey,
                            ),
                            Positioned(
                              bottom: 0,
                              width: 50,
                              left: -45,
                              child: index % 5 == 0
                                  ? Container(
                                alignment: Alignment.center,
                                child: Text(
                                  (index + 110).toString(),
                                  style: TextStyle(
                                    color:
                                    Colors.grey,
                                    fontSize: 14,
                                  ),
                                ),
                              )
                                  : Container(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          // Positioned(
          //   top: widget.height / 2 - 6,
          //   child: widget.marker ?? mark(),
          // ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController();
    scrollController?.addListener(() {
      setState(() {
        widget._value = double.parse((scrollController!.offset / 10)
            .toStringAsFixed(widget.fractionDigits));
        if (widget._value! < 0) widget._value = 0;
        if (widget.onValueChange != null) {
          widget.onValueChange!(widget._value!);
        }
      });
    });
    Future.delayed(const Duration(milliseconds: 500), () {

// Here you can write your code

      setState(() {
        scrollController?.jumpTo(((widget.selectedValue! - 110) * 10) ?? 0);
        // Here you can write your code for open new view
      });

    });

    widget.controller?.addListener(() {
      setPositionByValue((widget.controller?.value ?? 0));
    });
  }

  @override
  void dispose() {
    super.dispose();
    scrollController?.dispose();
  }

  @override
  void didUpdateWidget(RulerPicker oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  /// 滑动后修正标记，使之对齐
  void fixPosition(double curPos) {
    print('curPos: ${curPos}');
    double targetPos =
    double.parse(curPos.toStringAsFixed(widget.fractionDigits));
    print('targetPos: ${targetPos}');
    if (targetPos < 0) targetPos = 0;
    // todo animateTo 异步操作
    scrollController?.jumpTo(
      targetPos,
      // duration: Duration(milliseconds: 500),
      // curve: Curves.easeOut,
    );
  }

  /// 根据数值设置标记位置
  void setPositionByValue(num value) {
    num targetPos = value * 10;
    if (targetPos < 0) targetPos = 0;
    scrollController?.jumpTo(
      targetPos.toDouble(),
      // duration: Duration(milliseconds: 500),
      // curve: Curves.easeOut,
    );
  }
}
