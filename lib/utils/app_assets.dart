class Assets {
  static const String noimage = 'assets/images/noimage.png';

  static const String internet = 'assets/images/antenna.gif';

  static const String alarm = 'assets/images/alarm.png';

  static const String sad = 'assets/images/sad.png';

  static const String happy = 'assets/images/smile.png';

  static const String success = 'assets/images/confetti.png';

  static const String iconLocked = 'assets/images/icon-locked.png';

  static const String backGround = 'assets/images/backgroundimage1.png';

  static const String backGroundImage = "assets/images/background.png";

  static const String backGround1 = 'assets/images/Background1.png';

  static const String auditionerBG = 'assets/images/auditioner_bg.png';

  static const String producerBG = 'assets/images/ProducerImage.png';

  static const String signupProducerBG = 'assets/images/signupimage.png';

  static const String signupProducer = 'assets/images/producer_bg.png';
//producer_bg.png
  static const String appIcon = 'assets/images/stagepickerappicon.png';

  static const String auditionFeed = 'assets/images/auditionfeednavbarlogo.png';

  static const String setting = 'assets/images/settingsnavbariconlogo.png';

  static const String auditionFeedLogo = 'assets/images/auditionfeedlogo.png';

  static const String userProfileImage = 'assets/images/profileUser.png';

  static const String userProfile = 'assets/images/user.png';

  static const String photoPlaceHolder = 'assets/images/photography.png';

  static const String auditionPlaceHolder = 'assets/images/director.png';

  static const String verification = 'assets/images/verification.png';
}
