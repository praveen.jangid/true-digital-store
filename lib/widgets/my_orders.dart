import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/orderlist_dto.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_routes.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/order_detail.dart';
import 'package:intl/intl.dart';
import 'package:true_digital_store/widgets/rateStoreScreen.dart';
class MyOrders extends StatefulWidget {
  final bool isShowAppBar;

  const MyOrders({Key? key, required this.isShowAppBar}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _MyOrderState();
  }
}

class _MyOrderState extends State<MyOrders> {
  var orderList = <OrderData>[];

  @override
  void initState() {
    super.initState();
    _fetchOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.isShowAppBar ? null : AppBar(
        title: Text('My Orders'),
      ),
      body: SafeArea(
        child: ListView.builder(
          itemBuilder: (context, index) {
            var theOrder = orderList[index];
            return InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OrderDetailWidget(theOrder),
                    ));
              },
              child: Card(
                elevation: 3.0,
                margin: EdgeInsets.all(10),
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column(
                    children: [
                      Card(
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                  color: Colors.red.withAlpha(10),
                                  child: Image.network(
                                    theOrder.theStore
                                        ?.coverImage ??
                                        '',
                                    width: 70,
                                    height: 70,
                                  )),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  theOrder.theStore
                                      ?.businessName ??
                                      '',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                    width:
                                    MediaQuery.of(context).size.width / 1.6,
                                    child: Text(
                                        theOrder.theStore?.address ??
                                            '',
                                        style: TextStyle(fontSize: 12))),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8, left: 8),
                        child: Text(
                          formateDate(theOrder.createdAt ?? ''),
                          style: TextStyle(
                              fontFamily: AppTextStyle.Bold, fontSize: 16),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Order ID : ${theOrder.id ?? ""}",
                                  style: TextStyle(
                                      fontFamily: AppTextStyle.Bold, fontSize: 16),
                                ),
SizedBox(width: 10,),
                                Container(
                                  color: Colors.green.shade50,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Text(
                                      orderStatus(int.parse(theOrder.status ?? '0')),
                                      style: TextStyle(
                                          fontFamily: AppTextStyle.Bold, fontSize: 13),
                                    ),
                                  ),
                                ),

                              ],
                            ),

                          ],
                        ),
                      ),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Text(
                              "Total items : ${theOrder.orderItems?.length ?? "0"}",
                              style: TextStyle(
                                  fontFamily: AppTextStyle.Bold, fontSize: 13),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Total price : ${theOrder.totalAmount ?? "0"}",
                              style: TextStyle(
                                  fontFamily: AppTextStyle.Bold, fontSize: 13),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: theOrder.status == "5",
                        child: InkWell(
                          onTap: () {
                            AppRoutes.goto(context, RateStoreScreen(storeId: theOrder.storeId.toString(),));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Text(
                                  "Rate:",
                                  style: TextStyle(
                                      fontFamily: AppTextStyle.Bold, fontSize: 14, color: Colors.red),
                                ),
                                SizedBox(width: 8,),
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black12,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2))
                                  ),
                                  child:
                                      Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Row(
                                          children: [
                                            Text(
                                              "1",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontFamily: AppTextStyle.Bold, fontSize: 14, color: Colors.grey),
                                            ),
                                            Icon(Icons.star, size: 8,)
                                          ],
                                        ),
                                      ),

                                ),
                                SizedBox(width: 4,),
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black12,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2))
                                  ),
                                  child:
                                  Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Row(
                                      children: [
                                        Text(
                                          "2",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: AppTextStyle.Bold, fontSize: 14, color: Colors.grey),
                                        ),
                                        Icon(Icons.star, size: 8,)
                                      ],
                                    ),
                                  ),

                                ),
                                SizedBox(width: 4,),
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black12,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2))
                                  ),
                                  child:
                                  Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Row(
                                      children: [
                                        Text(
                                          "3",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: AppTextStyle.Bold, fontSize: 14, color: Colors.grey),
                                        ),
                                        Icon(Icons.star, size: 8,)
                                      ],
                                    ),
                                  ),

                                ),
                                SizedBox(width: 4,),
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black12,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2))
                                  ),
                                  child:
                                  Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Row(
                                      children: [
                                        Text(
                                          "4",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: AppTextStyle.Bold, fontSize: 14, color: Colors.grey),
                                        ),
                                        Icon(Icons.star, size: 8,)
                                      ],
                                    ),
                                  ),

                                ),
                                SizedBox(width: 4,),
                                Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black12,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2))
                                  ),
                                  child:
                                  Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: Row(
                                      children: [
                                        Text(
                                          "5",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: AppTextStyle.Bold, fontSize: 14, color: Colors.grey),
                                        ),
                                        Icon(Icons.star, size: 8,)
                                      ],
                                    ),
                                  ),

                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                  ),
                ),
              ),
            );
          },
          itemCount: orderList.length,
        ),
      ),
    );
  }

  void _fetchOrders() async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.get(NetworkUtils.kOrderList, context).then((value) {
        CommonUtills.showprogressdialogcomplete(context, false);
        OrderlistDto response = OrderlistDto.fromJson(value);
        if (response.status == true) {
          setState(() {
            orderList = response.data ?? [];
          });
        } else {
          setState(() {
            orderList = [];
          });
        }
      }).onError((error, stackTrace) {
        CommonUtills.errortoast(
            context, 'Something went wrong, please try again');
      });
    } else {
      CommonUtills.errortoast(context,
          'No internet connection available, please check and try again');
    }
  }
}

String orderStatus(int i) {

  if(i == 1) {
    return "Pending";
  }
  if(i == 2) {
    return "Accpected";
  }
  if(i == 3) {
    return "Processing";
  }
  if(i == 4) {
    return "Shipping";
  }
  if(i == 5) {
    return "Delivered";
  }
  if(i == 6) {
    return "Decline";
  }
  return "";
}


formateDate(String d) {
  if(d == "") {
    return "";
  }
  DateFormat formatter = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
  DateTime date = formatter.parse(d).toLocal();
  formatter = DateFormat("dd-MM-yyyy HH:mm:ss");
  return formatter.format(date);
}