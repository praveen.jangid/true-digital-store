import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:true_digital_store/model/cart_list_response.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_constants.dart';
import 'package:true_digital_store/utils/common_utills.dart';

import '../cartIconWidget.dart';

class itemListWidget extends StatefulWidget {
  Product? theProduct;
  final String storeId;

  itemListWidget(this.theProduct, @required this.storeId);

  @override
  _itemListWidgetState createState() => _itemListWidgetState(theProduct);
}

class _itemListWidgetState extends State<itemListWidget> {
  Product? theProduct;

  _itemListWidgetState(this.theProduct);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomRight,
      children: [

        Container(
          // height: MediaQuery.of(context).size.width/2,
          // width: MediaQuery.of(context).size.width/2,
          child: new Card(
            elevation: 2.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [

                Row(

                  children: [
                    // Expanded(
                    //   child: new Container(
                    //     margin: new EdgeInsets.all(8),
                    //     decoration: new BoxDecoration(
                    //       image: new DecorationImage(
                    //         fit: BoxFit.cover,
                    //         image: NetworkImage(
                    //           theProduct?.getProductImage() ?? '',
                    //         ),
                    //       ),
                    //     ),
                    //     //  height: 60,
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          Image.network(theProduct?.getProductImage() ?? '', width: 80,),
                          if (theProduct?.getPercentDiscount() != null && theProduct?.getPercentDiscount() != "")
                          Container(
                            color: Colors.red,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8, right: 8, top: 3, bottom: 3),
                              child: Text((theProduct?.getPercentDiscount() ?? ''),
                                  style: TextStyle(
                                      fontSize: 11,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontFamily: AppTextStyle.regular)),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                            const EdgeInsets.only(left: 0, right: 8, top: 8, bottom: 0),
                            child: Text(
                              theProduct?.productName ?? '',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: AppTextStyle.bold),
                            ),
                          ),
                          SizedBox(height: 5,),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 0,
                              right: 0,
                            ),
                            child: Row(
                              children: [
                                Text(
                                  theProduct?.weight ?? '',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.grey,
                                      fontFamily: AppTextStyle.regular),
                                ),
                                SizedBox(
                                  width: 5,
                                ),



                              ],
                            ),
                          ),
                          Row(
                            children: [
                              Text(
                                  theProduct?.getPercentDiscount() != ""
                                      ? (Constants.CURRENCY_SIGN +
                                      (theProduct?.mrpPrice ?? ''))
                                      : "",
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.grey,
                                      fontFamily: AppTextStyle.regular)),
                              if(theProduct?.getPercentDiscount() != "")
                              SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: Text(
                                    Constants.CURRENCY_SIGN + (theProduct?.sellPrice ?? ''),
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontFamily: AppTextStyle.regular)),
                              ),
                              Padding(
                                  padding:
                                  const EdgeInsets.only(left: 8, right: 8, bottom: 8, top: 8),
                                  child: Row(
                                    children: [

                                      findInCart() == null
                                          ? Container(
                                          height: 30,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.black12,
                                              ),
                                              borderRadius:
                                              BorderRadius.all(Radius.circular(200))),
                                          child: InkWell(
                                            onTap: () async {
                                              if (widget.theProduct?.theStore?.shop_status !=
                                                  "OPEN") {
                                                CommonUtills.errortoast(
                                                    context, "Store is closed");
                                                return;
                                              }
                                              var result = await theProduct?.addToCart(
                                                  context,
                                                  findInCart() != null
                                                      ? (findInCart()?.quantity ?? 0) + 1
                                                      : 1,
                                                  widget.storeId);
                                              cartRelodDone = () {
                                                setState(() {
                                                  cartData;
                                                });
                                              };

                                              log("ADDEDDDD ::::");
                                            },
                                            child: Row(
                                              children: [
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Icon(
                                                  Icons.add,
                                                  color: Colors.green,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  'Add',
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      color: Colors.green,
                                                      fontFamily: AppTextStyle.regular),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                              ],
                                            ),
                                          ))
                                          : Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          IconButton(
                                            onPressed: () async {
                                              // _con.decrementQuantity();
                                              Cart? theCartItem = findInCart();
                                              if (theCartItem?.quantity == 1) {
                                                theProduct!.cartItemId = theCartItem!.id!;
                                                await theProduct?.removeFromCart(context);
                                                cartRelodDone = () {
                                                  setState(() {
                                                    cartData;
                                                  });
                                                };
                                                setState(() {
                                                  cartData?.data?.cart
                                                      ?.remove(findInCart());
                                                });
                                              } else {
                                                setState(() {
                                                  findInCart()?.quantity =
                                                      (findInCart()?.quantity ?? 0) - 1;
                                                });

                                                var result =
                                                await theProduct?.updateCartQuantity(
                                                    context,
                                                    (findInCart()?.quantity ?? 0));
                                                cartRelodDone = () {
                                                  setState(() {
                                                    cartData;
                                                  });
                                                };
                                              }
                                            },
                                            iconSize: 30,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 10),
                                            icon: Icon(Icons.remove_circle_outline),
                                            color: Theme.of(context).hintColor,
                                          ),
                                          Text(findInCart()?.quantity?.toString() ?? '0',
                                              style: Theme.of(context).textTheme.subtitle1),
                                          IconButton(
                                            onPressed: () async {
                                              setState(() {
                                                findInCart()?.quantity =
                                                    (findInCart()?.quantity ?? 0) + 1;
                                              });
                                              var result =
                                              await theProduct?.updateCartQuantity(
                                                  context,
                                                  (findInCart()?.quantity ?? 0));
                                              cartRelodDone = () {
                                                setState(() {
                                                  cartData;
                                                });
                                              };
                                            },
                                            iconSize: 30,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 5, vertical: 10),
                                            icon: Icon(Icons.add_circle_outline),
                                            color: Theme.of(context).hintColor,
                                          )
                                        ],
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),


                  ],
                  mainAxisAlignment: MainAxisAlignment.start,
                ),

              ],
            ),
          ),
        ),
        Visibility(
          visible: theProduct?.status == "0",
          child: Container(
            margin: EdgeInsets.only(bottom: 10, right: 5),
            color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text("Out of stock", style: TextStyle(color: Colors.red, fontSize: 14),),
              )),
        )
      ],
    );
  }

  Cart? findInCart() {
    var item = cartData?.data?.cart
        ?.where((element) => element.productId == widget.theProduct?.id);
    return (item?.length ?? 0) > 0 ? item?.first : null;
  }
}
