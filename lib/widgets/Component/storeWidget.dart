import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_assets.dart';
import 'package:true_digital_store/utils/common_utills.dart';

Widget storeWidget(Store theStore) {
  return Card(
    elevation: 2,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5.0),
    ),
    child: ClipPath(
      clipper: ShapeBorderClipper(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          theStore.coverImage != ""
              ? Image.network(
                  theStore.coverImage ?? "",
                  fit: BoxFit.cover,
                  height: 100,
                )
              : Image.asset(
                  Assets.noimage,
                  height: 100,
                ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 62,
              child: Text(
                theStore.businessName ?? '',
                maxLines: 3,
                style:
                    TextStyle(fontSize: 17, fontFamily: AppTextStyle.regular),
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, bottom: 10),
            child: Row(
              children: [
                RatingBarIndicator(
                  rating: (theStore.totalRating ?? 0).toDouble(),
                  itemCount: 5,
                  itemSize: 10.0,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  '(${theStore.totalRating ?? 0})',
                  style: TextStyle(
                      fontSize: 10,
                      color: Colors.green,
                      fontFamily: AppTextStyle.bold),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                InkWell(
                  child: Icon(
                    Icons.call,
                    color: Colors.green,
                  ),
                  onTap: () {
                    CommonUtills.makePhoneCall(theStore.phone ?? '');
                  },
                ),
                //  SizedBox(width: 10,),
                InkWell(
                  child: Icon(
                    Icons.location_on_outlined,
                    color: Colors.green,
                  ),
                  onTap: () {
                    // CommonUtills.openMapWithAddrees(theStore.location ?? '');
                    if (theStore.latitude == null ||
                        theStore.latitude == '' ||
                        theStore.longitude == null ||
                        theStore.longitude == '') {
                      return;
                    }
                    print('opening map');
                    CommonUtills.openMap(double.parse(theStore.latitude!),
                        double.parse(theStore.longitude!));
                  },
                ),
                // Row(
                //   children: [
                //
                //   ],
                // ),
                Text(
                  theStore.shop_status ?? '',
                  style: TextStyle(
                      fontSize: 12,
                      color: (theStore.shop_status ?? '').toLowerCase() == "close" ? Colors.red : Colors.green,
                      fontFamily: AppTextStyle.bold),
                ),
                // Icon(CupertinoIcons.circle_fill, size: 10, color: Colors.green,),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
