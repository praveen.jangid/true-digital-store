import 'dart:io';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/material.dart';
//import 'package:webview_flutter/webview_flutter.dart';

class SupportScreen extends StatefulWidget {
  @override
  _SupportScreenState createState() => _SupportScreenState();
}

class _SupportScreenState extends State<SupportScreen> {
 // WebViewController? _controller;
  InAppWebViewController? _webViewController;
  String url = "";

  @override void initState() {
    // TODO: implement initState
    super.initState();
    //if (Platform.isAndroid) WebView.platform = AndroidWebView();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Support'),
      ),
      // body: WebView(
      //   initialUrl: 'https://truedigitalstore.com/contact',
      //   onWebViewCreated: (WebViewController webViewController) {
      //     _controller = webViewController;
      //     // _loadHtmlFromAssets();
      //   },
      // ),
      body: InAppWebView(
        initialUrlRequest: URLRequest(url: Uri.parse("https://truedigitalstore.com/contact")),
        //initialUrl: "https://flutter.dev/",
        initialOptions: InAppWebViewGroupOptions(
            crossPlatform: InAppWebViewOptions(

            //  debuggingEnabled: true,
            )
        ),
        onWebViewCreated: (InAppWebViewController controller) {
          _webViewController = controller;
        },
        // onLoadStart: (InAppWebViewController? controller, String? url) {
        //   setState(() {
        //     this.url = url;
        //   });
        // },
        // onLoadStop: (InAppWebViewController controller, String url) async {
        //   setState(() {
        //     this.url = url;
        //   });
        // },
        // onProgressChanged: (InAppWebViewController controller, int progress) {
        //   setState(() {
        //     this.progress = progress / 100;
        //   });
        // },
      ),
    );

  }
}
