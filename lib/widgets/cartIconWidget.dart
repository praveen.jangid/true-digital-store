import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/cart_list_response.dart';

import 'cart.dart';
import 'login.dart';

CartListResponse? cartData;
Function? reloadCart;
Function? cartRelodDone;
class cartIconWidget extends StatefulWidget {
  @override
  _cartIconWidgetState createState() => _cartIconWidgetState();
}

class _cartIconWidgetState extends State<cartIconWidget> {

  @override void initState() {
    // TODO: implement initState
    super.initState();
    _fetchCartItems();
  }

  _fetchCartItems() {
    CartListResponse.cartList(context, (response) {
      if (response is CartListResponse) {
        cartData = response as CartListResponse;
        if (cartData?.status == true) {
          if (cartRelodDone != null) {
            cartRelodDone!();
          }
          setState(() {
            // apiCalling = false;
            // cartList = this.cartResponse?.data?.cart ?? [];
          });
        } else {}
      }
    });
  }

  @override
  Widget build(BuildContext context) {

 reloadCart = () {
   _fetchCartItems();
 };
    return  FlatButton(
      onPressed: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => NetworkUtils.singupModel == null ? LoginWidget() : CartWidget()));
        // if (currentUser.value.apiToken != null) {
        //   Navigator.of(context).pushNamed('/Cart', arguments: RouteArgument(param: '/Pages', id: '2'));
        // } else {
        //   Navigator.of(context).pushNamed('/Login');
        // }
      },
      child: Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: <Widget>[
          Icon(
            Icons.shopping_cart_outlined,
            color: Colors.white,
            size: 28,
          ),
          /*Container(
          child: SvgPicture.asset("assets/img/shooping-cart.svg"),
          ),*/
          Container(
            child: Text(
              cartData?.data?.cart?.length?.toString() ?? '0',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black),
            ),
            padding: EdgeInsets.all(0),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(10))),
            constraints: BoxConstraints(minWidth: 15, minHeight: 15, maxHeight: 15),
          ),
        ],
      ),
      color: Colors.transparent,
    );
  }
}
