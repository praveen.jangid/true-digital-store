import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:true_digital_store/model/store.dart';

class PhotoWidget extends StatefulWidget {
  late List<Gallery> photos;

  PhotoWidget(this.photos);

  @override
  State<StatefulWidget> createState() {
    return _PhotoState(this.photos);
  }
}

class _PhotoState extends State<PhotoWidget> {
  List<Gallery> itemList = [];

  _PhotoState(this.itemList);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Photos'),
      ),
      body: SafeArea(
        child: itemList.length == 0
            ? Center(
                child: Text("No photos"),
              )
            : StaggeredGridView.countBuilder(
                shrinkWrap: true,
                padding: EdgeInsets.all(10),
                itemCount: itemList.length,
                staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                crossAxisCount: 2,
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {},
                      child: itemList[0].galleryImage == null
                          ? Image.asset("assets/images/gallery.png")
                          : Image.network(itemList[0].galleryImage!));
                  ;
                }),
      ),
    );
  }
}
