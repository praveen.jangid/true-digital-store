import 'package:flutter/material.dart';

class NotificationSetting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NotificationState();
  }
}

class _NotificationState extends State<NotificationSetting> {
  bool notificationEnable = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notification Setting'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Row(
            children: [
              Text('Notifications'),
              Spacer(),
              Switch(
                value: notificationEnable,
                onChanged: (value) {
                  setState(() {
                    notificationEnable = value;
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
