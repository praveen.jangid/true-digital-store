import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:true_digital_store/theme/color.dart';

class ContactUsWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ContactUsState();
  }
}

class _ContactUsState extends State<ContactUsWidget> {
  final _formKey = new GlobalKey<FormState>();
  Color _borderColor = HexColor("#96D6D0");
  double textFieldPadding = 25;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contact Us'),
      ),
      body: SingleChildScrollView(
        child: Form(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: textFieldPadding,
                ),
                TextFormField(
                  inputFormatters: [
                    FilteringTextInputFormatter.singleLineFormatter
                  ],
                  style: TextStyle(color: Colors.black),
                  initialValue: '',
                  decoration: textFieldDecoration('Name'),
                ),
                SizedBox(
                  height: textFieldPadding,
                ),
                TextFormField(
                  inputFormatters: [
                    FilteringTextInputFormatter.singleLineFormatter
                  ],
                  style: TextStyle(color: Colors.black),
                  initialValue: '',
                  keyboardType: TextInputType.emailAddress,
                  decoration: textFieldDecoration('Email'),
                ),
                SizedBox(
                  height: textFieldPadding,
                ),
                TextFormField(
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  style: TextStyle(color: Colors.black),
                  initialValue: '',
                  maxLength: 10,
                  keyboardType: TextInputType.phone,
                  decoration: textFieldDecoration('Phone'),
                ),
                SizedBox(
                  height: textFieldPadding,
                ),
                TextFormField(
                  maxLines: 6,
                  inputFormatters: [
                    FilteringTextInputFormatter.singleLineFormatter
                  ],
                  style: TextStyle(color: Colors.black),
                  keyboardType: TextInputType.multiline,
                  initialValue:
                      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                  decoration: textFieldDecoration('Message'),
                ),
                SizedBox(
                  height: textFieldPadding,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Center(
                          child: Text(
                        "Submit",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                    ),
                    color: AppColor.themePrimary,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  InputDecoration textFieldDecoration(String hintText) {
    return InputDecoration(
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 1.0)),
      disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 0.0)),
      labelText: hintText,
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: _borderColor, width: 2.0)),
      border: OutlineInputBorder(),
    );
  }
}
