import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class StoreYoutubeVideo extends StatefulWidget {
  List<String> videoslist = [];

  StoreYoutubeVideo(this.videoslist);

  @override
  State<StatefulWidget> createState() {
    return _StoreYoutubeState(videoslist);
  }
}

class _StoreYoutubeState extends State<StoreYoutubeVideo> {
  List<String> list = [];
  List<String> vidoeThumnails = [];

  _StoreYoutubeState(this.list);

  @override
  void initState() {
    super.initState();

    try {
      List<String> temp = [];
      list.forEach((element) {
        temp.add(
            "https://img.youtube.com/vi/${YoutubePlayer.convertUrlToId(element)}/0.jpg");
      });
      setState(() {
        this.vidoeThumnails = temp;
      });
    } on Exception catch (exception) {
      // only executed if error is of type Exception
      print('exception');
    } catch (error) {
      // executed for errors of all types other than Exception
      print(error.toString());
      print('catch error');
      //  videoIdd="error";

    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Videos"),
      ),
      body: SafeArea(
        child: list.length == 0
            ? Center(
                child: Text("No videos"),
              )
            : StaggeredGridView.countBuilder(
                shrinkWrap: true,
                padding: EdgeInsets.all(10),
                itemCount: this.vidoeThumnails.length,
                staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                crossAxisCount: 2,
                itemBuilder: (context, index) {
                  return InkWell(
                      onTap: () {
                        CommonUtills.openLink(list[index]);
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          (this.vidoeThumnails[index] == null
                              ? Image.asset("assets/images/gallery.png")
                              : Image.network(this.vidoeThumnails[index]!)),
                          Image.asset(
                            "assets/images/youtube.png",
                            width: 50,
                            height: 50,
                          )
                        ],
                      ));
                  ;
                }),
      ),
    );
  }
}
