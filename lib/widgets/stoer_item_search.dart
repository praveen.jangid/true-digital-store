import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/model/store_product_search.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/Component/itemWidget.dart';

class StoreItemSearchWidget extends StatefulWidget {
  final Store? theStore;

  const StoreItemSearchWidget({Key? key, required this.theStore}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _StoreItemSearchState();
  }
}

class _StoreItemSearchState extends State<StoreItemSearchWidget> {
  List<Product> items = [];
  StoreProductSearch? storeDetail;

  TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search'),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: searchBar(),
            ),
            Expanded(
              child: GridView.builder(
                  padding: EdgeInsets.all(10),
                  itemCount: items.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 0.8,
                      crossAxisCount: 2,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 15),
                  itemBuilder: (context, index) {
                    return InkWell(
                        onTap: () {}, child: itemWidget(items[index], "", () {

                    }));
                    ;
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget searchBar() {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: Container(
        height: 40,
        decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.all(Radius.circular(200))),
        child: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: TextFormField(
            controller: _searchController,
            textInputAction: TextInputAction.search,
            onFieldSubmitted: (b) {
              //_fetchProducts();
            },
            onChanged: (val) {
              _fetchProducts();
            },
            decoration: InputDecoration(
                suffixIcon: InkWell(
                  child: Icon(Icons.search),
                  onTap: () {
                    _fetchProducts();
                  },
                ),
                hintText: 'Search in store',
                border: InputBorder.none),
          ),
        ),
      ),
    );
  }

  void _fetchProducts() async {
    String keyword = _searchController.text.trim();
    if (keyword.isEmpty) {
      setState(() {
        this.items.clear();
      });
      return;
    }
    if (true == await CommonUtills.checkNetworkStatus()) {
    //  CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _netUtil = new NetworkUtils();
      setState(() {
        this.items.clear();
      });
      String path = "${NetworkUtils.kStoreProductSearch}?keyword=${keyword}&store_id=${widget.theStore?.id.toString()}";

      // String path =
      //     "${NetworkUtils.kStoreSubcategoryProduct}?product_category_id=79";
      _netUtil.get(path, context).then((dynamic res) async {
      //  CommonUtills.showprogressdialogcomplete(context, false);

        StoreProductSearch results = new StoreProductSearch.fromJson(res);
        storeDetail = results;
        if (results.status == true) {
          setState(() {
            this.items = results.data ?? [];
          });
        } else {
          CommonUtills.errortoast(context, results?.message ?? '');
        }
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }
}
