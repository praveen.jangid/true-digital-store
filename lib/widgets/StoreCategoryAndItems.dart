import 'dart:developer';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/model/store_detail_response.dart';
import 'package:true_digital_store/model/store_product_response.dart';
import 'package:true_digital_store/model/store_subcategory_product_list.dart';
import 'package:true_digital_store/theme/color.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_assets.dart';
import 'package:true_digital_store/utils/app_routes.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/photo.dart';
import 'package:true_digital_store/widgets/product.dart';
import 'package:true_digital_store/widgets/product_detail.dart';
import 'package:true_digital_store/widgets/stoer_item_search.dart';
import 'package:true_digital_store/widgets/store_youtube_video.dart';

import 'Component/itemWidget.dart';
import 'cartIconWidget.dart';

class StioreCategoryAndItems extends StatefulWidget {
  late Store theStore;

  StioreCategoryAndItems(this.theStore);

  @override
  _StioreCategoryAndItemsState createState() =>
      _StioreCategoryAndItemsState(theStore);
}

class _StioreCategoryAndItemsState extends State<StioreCategoryAndItems> {
  late Store theStore;
  bool isApiCalling = false;
  StoreDetailResponse? storeDetail;
  late StoreProductResponse _productResponse;
  List<Gallery> galleryItems = [];
  List<Product> storeProducts = [];
  List<StoreCategory> categories = [];
  List<String> images = [];
  _StioreCategoryAndItemsState(this.theStore);
  int _current = 0;
  StoreSubcategoryProductList? offerAndPramosionCategory;

  @override
  void initState() {
    super.initState();
    _getStoreDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Store'),
        automaticallyImplyLeading: true,
        actions: [cartIconWidget()],
      ),
      body: SafeArea(
        child: isApiCalling
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 10),
                      child: Text(
                        theStore.businessName ?? '',
                        style: TextStyle(
                            fontSize: 18, fontFamily: AppTextStyle.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 7),
                      child: Text(
                        theStore.address ?? '',
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.black,
                            fontFamily: AppTextStyle.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 7),
                      child: Row(
                        children: [
                          RatingBarIndicator(
                            rating: double.parse(
                                (theStore.totalRating ?? 0).toString()),
                            itemCount: 5,
                            itemSize: 15.0,
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "(${(theStore.totalRating ?? 0).toString()})",
                            style: TextStyle(
                                fontSize: 10,
                                color: Colors.green,
                                fontFamily: AppTextStyle.bold),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 7),
                      child: Row(
                        children: [
                          Icon(
                            Icons.remove_red_eye_rounded,
                            color: Colors.green,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            storeDetail?.data?.postViews?.toString() ?? '0',
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: AppTextStyle.regular,
                                color: Colors.green),
                          ),
                          SizedBox(
                            width: 25,
                          ),
                          Icon(
                            Icons.card_travel,
                            color: Colors.green,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '(${storeDetail?.data?.order_delivered ?? 0}) Order Delivered',
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: AppTextStyle.regular,
                                color: Colors.green),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 10, top: 10, right: 10),
                      child: searchBar(),
                    ),
                    Visibility(
                      visible: (storeDetail?.data?.offers?.length ?? 0) > 0,
                      child: Stack(
                        children: [
                          CarouselSlider(
                            items: imagesSlide(),
                            options: CarouselOptions(
                                autoPlay: true,
                                height: 150,
                                viewportFraction: 1,
                                enlargeCenterPage: true,
                                aspectRatio: 1.0,
                                // disableCenter: true,
                                onPageChanged: (index, reason) {
                                  setState(() {
                                    _current = index;
                                  });
                                }),
                          ),
                          Positioned(
                            bottom: 5,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: images.map((url) {
                                  int index = images.indexOf(url);
                                  return Container(
                                    width: 8.0,
                                    height: 8.0,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 2.0),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: _current == index
                                          ? Colors.white
                                          : Colors.grey,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  offerAndPramosionCategory
                                          ?.data?.first.name ??
                                      'Offer & Promotion',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: AppTextStyle.bold,
                                      color: Colors.black),
                                ),
                                TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    ' ',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: AppTextStyle.bold,
                                        color: Colors.green),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Container(
                              child: GridView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: offerAndPramosionCategory
                                        ?.data?.first?.products?.length ??
                                    0,
                                gridDelegate:
                                    new SliverGridDelegateWithFixedCrossAxisCount(
                                        mainAxisSpacing: 2,
                                        crossAxisSpacing: 2,
                                        crossAxisCount: 1,
                                        childAspectRatio: 1.4),
                                itemBuilder: (BuildContext context, int index) {
                                  Product? theProduct =
                                      offerAndPramosionCategory
                                          ?.data?.first?.products?[index];
                                  theProduct?.theStore = this.theStore;
                                  return new GestureDetector(
                                      child: itemWidget(theProduct,
                                          theProduct!.theStore!.id!, (){

                                          }),
                                      onTap: () {
                                        Get.to(() => ProductDetailWidget(
                                            theProduct,
                                            theProduct!.theStore!.id!));
                                      });
                                },
                              ),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.width / 1.6,
                            ),
                          ),
                        ],
                      ),
                      visible:
                          (offerAndPramosionCategory?.data?.length ?? 0) > 0,
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Popular categories',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: AppTextStyle.bold,
                                    color: Colors.black),
                              ),
                              // TextButton(onPressed: () {
                              //
                              // }, child:  Text(
                              //   'VIEW ALL',
                              //   style: TextStyle(
                              //       fontSize: 14,
                              //       fontFamily: AppTextStyle.bold, color: Colors.green),
                              // ),)
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: GridView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: categories.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      childAspectRatio: 0.8,
                                      crossAxisCount: 3,
                                      crossAxisSpacing: 5,
                                      mainAxisSpacing: 15),
                              itemBuilder: (context, index) {
                                StoreCategory theCategory = categories[index];
                                return InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ProductWidget(
                                                this.storeDetail?.data,
                                                theCategory)));
                                  },
                                  child: Container(
                                    color: Colors.transparent,
                                    // height: 300,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          Card(
                                            elevation: 2,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5.0),
                                            ),
                                            child: ClipPath(
                                              clipper: ShapeBorderClipper(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5))),
                                              child: Container(
                                                  // decoration: BoxDecoration(
                                                  //     image: DecorationImage(
                                                  //         fit: BoxFit.cover,
                                                  //         image: NetworkImage(
                                                  //             'https://images.unsplash.com/photo-1579202673506-ca3ce28943ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'))),
                                                  child: Center(
                                                      child: theCategory
                                                                  .categoryImage ==
                                                              null
                                                          ? Image.asset(
                                                              Assets.noimage,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width /
                                                                  3.9,
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width /
                                                                  4.1,
                                                            )
                                                          : Image.network(
                                                              theCategory
                                                                      .categoryImage ??
                                                                  "",
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width /
                                                                  3.9,
                                                              height: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width /
                                                                  4.1,
                                                              fit: BoxFit.cover,
                                                            ))),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Text(
                                              theCategory.name ?? "",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black,
                                                  fontFamily:
                                                      AppTextStyle.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.white,
                            border: Border.all(width: 1)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              child: TextButton(
                                  onPressed: () {
                                    Get.to(
                                        () => PhotoWidget(this.galleryItems));
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/gallery.png',
                                        width: 30,
                                        height: 25,
                                        color: AppColor.green,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text('View Photos')
                                    ],
                                  )),
                            ),
                            SizedBox(
                              width: 1,
                              height: 50,
                              child: Container(
                                color: Colors.black,
                              ),
                            ),
                            Expanded(
                              child: TextButton(
                                onPressed: () {
                                  Get.to(() => StoreYoutubeVideo(this
                                          .storeDetail
                                          ?.data
                                          ?.getYoutubeVideo() ??
                                      []));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/images/youtube.png',
                                      width: 30,
                                      height: 25,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text('Youtube')
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      color: Color.fromRGBO(240, 240, 240, 1),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              this.storeDetail?.data?.businessName ?? '',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontFamily: AppTextStyle.bold),
                            ),
                            Visibility(
                              visible: this.storeDetail?.data?.email != null && this.storeDetail?.data?.email != "",
                              child: SizedBox(
                                height: 10,
                              ),
                            ),
                            Visibility(
                              visible: this.storeDetail?.data?.email != null && this.storeDetail?.data?.email != "",
                              child: Text(
                                'Email :- ${this.storeDetail?.data?.email ?? ''}',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontFamily: AppTextStyle.regular),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            // Text(
                            //   'Phone Number :- ${this.storeDetail?.data?.phone ?? ''}',
                            //   textAlign: TextAlign.left,
                            //   style: TextStyle(
                            //       fontSize: 14,
                            //       color: Colors.black,
                            //       fontFamily: AppTextStyle.regular),
                            // ),
                            // SizedBox(
                            //   height: 10,
                            // ),
                            Text(
                              this.storeDetail?.data?.location ?? '',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontFamily: AppTextStyle.regular),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Visibility(
                              visible: this.storeDetail?.data?.companyEstDate != null && this.storeDetail?.data?.companyEstDate != "",
                              child: Row(
                                children: [
                                  Image.asset(
                                    "assets/images/fssi.png",
                                    scale: 1,
                                    height: 15,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'License No. ${this.storeDetail?.data?.companyEstDate ?? ""}',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: Colors.black,
                                        fontFamily: AppTextStyle.bold),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                              onPressed: () {
                                CommonUtills.openLink(
                                    storeDetail?.data?.facebookLink ?? '');
                              },
                              icon: Image.asset('assets/images/facebook.png')),
                          SizedBox(
                            width: 10,
                          ),
                          IconButton(
                              onPressed: () {
                                CommonUtills.openLink(
                                    storeDetail?.data?.instagramLink ?? '');
                              },
                              icon: Image.asset('assets/images/instagram.png')),
                          SizedBox(
                            width: 10,
                          ),
                          IconButton(
                              onPressed: () {
                                CommonUtills.openLink(
                                    storeDetail?.data?.linkedinLink ?? '');
                              },
                              icon: Image.asset('assets/images/linkedin.png')),
                          SizedBox(
                            width: 10,
                          ),
                          IconButton(
                              onPressed: () {
                                CommonUtills.openLink(
                                    storeDetail?.data?.twitterLink ?? '');
                              },
                              icon: Image.asset('assets/images/twitter.png')),
                          SizedBox(
                            width: 10,
                          ),
                          IconButton(
                              onPressed: () {
                                CommonUtills.openLink(
                                    "https://wa.me/${storeDetail?.data?.whatsapp ?? ''}");
                              },
                              icon: Image.asset('assets/images/whatsapp.png')),
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  Widget searchBar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        InkWell(
          onTap: () {
            AppRoutes.goto(this.context, StoreItemSearchWidget(theStore: widget.theStore,));
          },
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: Container(
                height: 40,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(200))),
                child: Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.search),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Search items...',
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.grey,
                          fontFamily: AppTextStyle.regular),
                    )
                  ],
                )),
          ),
        )
      ],
    );
  }

  _getStoreDetail() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      setState(() {
        isApiCalling = true;
      });
      NetworkUtils _netUtil = new NetworkUtils();
      String apiPath =
          NetworkUtils.kStoreDetail + '?store_id=' + theStore.id.toString();
      _netUtil.get(apiPath, context).then((dynamic res) async {
        log("STORE DETAIL RESPONSE ::: ");
        this.storeDetail = StoreDetailResponse.fromJson(res);
        setState(() {
          isApiCalling = false;
          this.galleryItems = this.storeDetail?.data?.gallery ?? [];
          this.categories = this.storeDetail?.data?.storeCategory ?? [];
          this.storeProducts = this.storeDetail?.data?.products ?? [];
        });
        var cat = this.categories.firstWhere(
            (element) =>
                element.name?.toLowerCase().contains("offers and promotions") ??
                false,
            orElse: null);
        if (cat != null) {
          setState(() {
            categories.remove(cat);
          });
          fetchSubCategoryAndProducts(cat.id);
        }
        _getImages();
      });
    } else {
      setState(() {
        isApiCalling = false;
      });
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }

  void fetchSubCategoryAndProducts(catId) async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      //  CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _netUtil = new NetworkUtils();
      String path =
          "${NetworkUtils.kStoreSubcategoryProduct}?product_category_id=${catId}";

      // String path =
      //     "${NetworkUtils.kStoreSubcategoryProduct}?product_category_id=79";
      _netUtil.get(path, context).then((dynamic res) async {
        //  CommonUtills.showprogressdialogcomplete(context, false);

        StoreSubcategoryProductList results =
            new StoreSubcategoryProductList.fromJson(res);
        if (results.status == true) {
          setState(() {
            this.offerAndPramosionCategory = results;
          });
        } else {
          CommonUtills.errortoast(context, results?.message ?? '');
        }
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }

  _getImages() {
    setState(() {
      storeDetail?.data?.offers?.forEach((element) {
        images.add(element.offerImage ?? '');
      });
    });
  }

  List<Widget> imagesSlide() {
    return images
        .map((item) => GFImageOverlay(
              height: 150,
              width: MediaQuery.of(context).size.width,
              shape: BoxShape.rectangle,
              image: NetworkImage(NetworkUtils.base_image_url + item),
              boxFit: BoxFit.cover,
            ))
        .toList();
  }

  _getStoreProduct() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      NetworkUtils _netUtil = new NetworkUtils();
      String apiPath =
          NetworkUtils.kProductList + '?store_id=' + theStore.id.toString();
      _netUtil.get(apiPath, context).then((dynamic res) async {
        this._productResponse = StoreProductResponse.fromJson(res);
        setState(() {
          this.storeProducts = this._productResponse.data ?? [];
        });
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }
}
