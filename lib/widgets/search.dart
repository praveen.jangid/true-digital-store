import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/model/search_store_product_response.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/utils/app_constants.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/StoreCategoryAndItems.dart';
import 'package:true_digital_store/widgets/home.dart';
import 'package:true_digital_store/widgets/product_detail.dart';

import 'Component/itemWidget.dart';
import 'Component/storeWidget.dart';
import 'cart.dart';
import 'cartIconWidget.dart';

class SearchWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SearchWidgetState();
  }
}

class _SearchWidgetState extends State<SearchWidget> {
  List<Store> storelist = [];
// Will this line compile?
  TextEditingController _searchController = TextEditingController();

  @override
  void initState() {}

  Widget _bottomCartView() {
    return Container(
      height: 60,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.8),
          spreadRadius: 0,
          blurRadius: 8,
          offset: Offset(0, 4),
        )
      ], color: Colors.white),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Icon(Icons.add_shopping_cart_outlined),
            SizedBox(
              width: 10,
            ),
            Text('${cartData?.data?.cart?.length ?? 0} Item'),
            SizedBox(
              width: 10,
            ),
            Text(Constants.CURRENCY_SIGN + '${cartData?.data?.totalPrice ?? ""}'),
            Spacer(),
            TextButton(
                onPressed: () => Get.to(CartWidget()), child: Text('View Cart'))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.white,
        // wrap in gesture to dismiss keyboard
        bottomNavigationBar: (cartData?.data?.cart?.length ?? 0) == 0 ? null : _bottomCartView(),
        body: SafeArea(
          child: new GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.arrow_back)),
                    Expanded(child: searchBar()
                        // Container(
                        //   height: 35,
                        //   margin: EdgeInsets.all(10),
                        //   decoration:
                        //       BoxDecoration(border: Border.all(color: Colors.grey)),
                        //   child: TextFormField(
                        //     initialValue: '',
                        //     decoration: InputDecoration(
                        //         hintText: 'Search', border: InputBorder.none),
                        //   ),
                        // ),
                        )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 40,
                  child: Row(
                    children: [
                      SizedBox(
                        width: 8,
                      ),
                      TabBar(
                        isScrollable: true,
                        tabs: [
                          Container(
                            child: Text(
                                'Items ${_getItemsCount().toString() ?? '0'}',
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                            height: 30,
                          ),
                          Container(
                            child: Text('Store (${storelist.length})',
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                            height: 30,
                          )
                          //  Tab(icon: Icon(Icons.directions_bike)),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: TabBarView(
                    children: [getCatWithItem(), getStores()],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget searchBar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Container(
            height: 40,
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                ),
                borderRadius: BorderRadius.all(Radius.circular(200))),
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: TextFormField(
                controller: _searchController,
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (b) {
                  print("search");
                  _searchStoreAndProducts();
                },
                decoration: InputDecoration(
                    icon: InkWell(
                      child: Icon(Icons.search),
                      onTap: () {
                        _searchStoreAndProducts();
                      },
                    ),
                    suffixIcon: InkWell(
                      child: Icon(Icons.close),
                      onTap: () {
                        setState(() {
                          _searchController.text = "";
                        });
                      },
                    ),
                    hintText: 'Search for an item and store',
                    border: InputBorder.none),
              ),
            ),
          ),
        ),
      ],
    );
  }

  int _getItemsCount() {
    int c = 0;
    storelist.forEach((element) {
      element.products?.forEach((element) {
        c += 1;
      });
    });
    return c;
  }

  getCatWithItem() {
    return ListView.builder(
      itemCount: storelist?.length ?? 0,
      // shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        Store theStore = storelist[index];
        log("PRODUCTS:::: ${theStore.products}");
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            StioreCategoryAndItems(theStore)));
              },
              child: Container(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: 10,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      child: new Image.network(
                        theStore.coverImage ?? "",
                        width: 40,
                        height: 40,
                        fit: BoxFit.cover,
                      ),
                    ),
                    new Padding(padding: const EdgeInsets.only(right: 5.0)),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Text(theStore.businessName ?? '',
                              style: new TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold)),
                          SizedBox(
                            height: 2,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.location_on_outlined,
                                size: 15,
                              ),
                              SizedBox(
                                width: 2,
                              ),
                              new Text(theStore.address ?? '',
                                  style: new TextStyle(
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.normal)),
                              // SizedBox(
                              //   width: 10,
                              // ),
                              // new Text('Free Delivery',
                              //     style: new TextStyle(
                              //         fontSize: 12.0,
                              //         fontWeight: FontWeight.normal)),
                            ],
                          )
                        ],
                      ),
                    ),
                    Icon(Icons.arrow_forward),
                    SizedBox(
                      width: 10,
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 15.0),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                child: GridView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: (theStore?.products ?? [])?.length ?? 0,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      mainAxisSpacing: 2,
                      crossAxisSpacing: 2,
                      crossAxisCount:
                          (theStore.products ?? []).length > 1 ? 2 : 1,
                      childAspectRatio: 1.4),
                  itemBuilder: (BuildContext context, int index) {
                    Product theProduct = theStore.products![index];
                    theProduct.theStore = theStore;
                    return new GestureDetector(
                        child: itemWidget(theProduct, theStore?.id ?? '', () {
                          setState(() {
                            cartData;
                          });
                        }),
                        onTap: () {
                          Get.to(() =>
                              ProductDetailWidget(theStore.products![index], theStore.id ?? ''));
                        });
                  },
                ),
                width: MediaQuery.of(context).size.width,
                height: (theStore.products ?? []).length > 1
                    ? MediaQuery.of(context).size.height / 1.6
                    : MediaQuery.of(context).size.height / 3.2,
              ),
            ),
            SizedBox(height: 20.0),
          ],
        );
      },
    );
  }

  getStores() {
    return StaggeredGridView.countBuilder(
        padding: EdgeInsets.all(10),
        itemCount: storelist.length,
        staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        crossAxisCount: 2,
        itemBuilder: (context, index) {
          return InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            StioreCategoryAndItems(storelist[index])));
              },
              child: storeWidget(storelist[index]));
          ;
        });
  }

  _searchStoreAndProducts() async {
    String searchKeyword = _searchController.text.trim();
    if (searchKeyword.isEmpty) {
      CommonUtills.errortoast(
          context, 'Please enter product/store name to search');
      return;
    }

    setState(() {
      storelist.clear();
    });
    FocusManager.instance.primaryFocus?.unfocus();
    if (true == await CommonUtills.checkNetworkStatus()) {
      CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _netUtil = new NetworkUtils();
      String apiPath =
          NetworkUtils.kSearchForStoreAndProduct + '?keyword=' + searchKeyword;

      if (latitude != null) {
        apiPath = "${apiPath}&lat=${latitude}&long=${longitude}";
      }

      _netUtil.get(apiPath, context).then((dynamic res) async {
        log("SEARCH RESPONSE:::::: ${res}");
        CommonUtills.showprogressdialogcomplete(context, false);
        SearchStoreProductResponse result =
            SearchStoreProductResponse.fromJson(res);
        if (result.data != null && result.data!.length > 0) {
          setState(() {
            storelist.addAll(result.data!);
            //print(storelist);
          });
        } else {}
      });
    } else {
      CommonUtills.noInternetToast(context);
    }
  }

  _totalProductCount() {
    return 0;
  }
}
