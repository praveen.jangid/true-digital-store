import 'dart:io';

import 'package:flutter/material.dart';
import 'package:true_digital_store/theme/text_style.dart';
//import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class AboutWidget extends StatefulWidget {
  @override
  State<AboutWidget> createState() => _AboutWidgetState();
}

class _AboutWidgetState extends State<AboutWidget> {
 // WebViewController? _controller;
  InAppWebViewController? _webViewController;

  @override void initState() {
    // TODO: implement initState
    super.initState();
   // if (Platform.isAndroid) WebView.platform = AndroidWebView();

  }

  @override
  Widget build(BuildContext context) {
    String aboutContent =
        "True digital store is a platform where consumer get all the essential and necessary Products From their local(nearby) Store with contactless home delivery by online ordering as well as consumer can get direct appointment to the salon, doctor etc. It is the perfect true wisdom ease of digital business. It is based on one click. You can get whole essential and necessary information of the product of a particular store by just one click in your devices. It is just like a e-commerce web site, which provide you to get your store offline to online which enhance your business by rising direct ordering option. It manifests more faith or your business value in the market";

    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
      ),
    //    body: WebView(
    //   initialUrl: 'https://truedigitalstore.com/about-us',
    //   onWebViewCreated: (WebViewController webViewController) {
    //     _controller = webViewController;
    //    // _loadHtmlFromAssets();
    //   },
    // ),
      body: InAppWebView(
      initialUrlRequest: URLRequest(url: Uri.parse('https://truedigitalstore.com/about-us')),
    //initialUrl: "https://flutter.dev/",
    initialOptions: InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(

    //  debuggingEnabled: true,
    )
    ),
    onWebViewCreated: (InAppWebViewController controller) {
    _webViewController = controller;
    }
      )
    );
  }
}
