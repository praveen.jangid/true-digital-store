import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'coupon_list_response.dart';

class CouponList extends StatefulWidget {
  final String storeId;

  const CouponList({Key? key, required this.storeId}) : super(key: key);
  @override
  _CouponListState createState() => _CouponListState();
}

class _CouponListState extends State<CouponList> {
  CouponListResponse? couponRes;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchCartItems();
  }

  _fetchCartItems() async {
    // CommonUtills.showprogressdialogcomplete(context, true);
    CouponListResponse.couponList(context, widget.storeId, (response) {
      // CommonUtills.showprogressdialogcomplete(context, false);
      log("RESULT:::  ${response}");

      if (response is CouponListResponse) {
        this.couponRes = response;
        if (this.couponRes?.status == true) {
          setState(() {
            this.couponRes = response;
          });
        } else {
          log("NO RESULT:::");
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            // if (_con.carts.isNotEmpty)
            //   Navigator.pop(context);
            // else
            //   Navigator.pushReplacementNamed(context, '/Pages', arguments: 2);
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
          color: Theme.of(context).hintColor,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Discount Code",
          style: Theme.of(context).textTheme.headline6,
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            ListView(
              primary: true,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: SearchBarCoupon(
                    onClickFilter: (event) async {
                      if (event.isEmpty) {
                        //   _con.scaffoldKey?.currentState
                        //       ?.showSnackBar(SnackBar(
                        //     content:
                        //     Text("Please enter Discount Code"),
                        //   ));
                        //   return;
                        // } else {
                        //   setState(() {
                        //     appData.isloading = true;
                        //   });
                        //   var value = await _con
                        //       .listenForApplyCoupons(event);
                        //   setState(() {
                        //     appData.isloading = false;
                        //   });
                        // setState(() {
                        //   data?.isLoading = false;
                        // });

                      }
                      Get.back(result: event);
                      //widget.parentScaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
                Card(
                  elevation: 0,
                  child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Available Coupons",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.separated(
                  padding: EdgeInsets.only(top: 0, bottom: 10),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  primary: false,
                  itemCount: couponRes?.data?.length ?? 0,
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 1);
                  },
                  itemBuilder: (context, index) {
                    var data = couponRes?.data?[index];
                    return CouponItemWidget(
                      data: data,
                      onApplyClick: () async {
                        Get.back(result: data, canPop: true);
                        //   if (data.totalUses < 5) {
                        //     _con.scaffoldKey?.currentState
                        //         ?.showSnackBar(SnackBar(
                        //       content: Text(
                        //           "Coupon currently not available"),
                        //     ));
                        //     return;
                        //   } else if (data?.minCartamount >
                        //       _con.total) {
                        //     _con.scaffoldKey?.currentState
                        //         ?.showSnackBar(SnackBar(
                        //       content: Text(
                        //           "Cart amount must be above ${data?.minCartamount}"),
                        //     ));
                        //     return;
                        //   } else if ((data?.discountables?.length ??
                        //       0) >
                        //       0) {
                        //     if (data.discountables.first
                        //         .discountableId
                        //         .toString() !=
                        //         _con.restaurant.id.toString()) {
                        //       _con.scaffoldKey?.currentState
                        //           ?.showSnackBar(SnackBar(
                        //         content: Text(
                        //             "Discount Code is not applicable for this establishment"),
                        //       ));
                        //       return;
                        //     }
                        //   }
                        //   setState(() {
                        //     data?.isLoading = true;
                        //   });
                        //   var value = await _con
                        //       .listenForApplyCoupons(data.code);
                        //
                        //   setState(() {
                        //     data?.isLoading = false;
                        //   });
                        //   // log("APPLYIED COPON DATA::::: ${value.toJson()}");
                      },
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SearchBarCoupon extends StatelessWidget {
  final Function(String) onClickFilter;

  SearchBarCoupon({Key? key, required this.onClickFilter}) : super(key: key);
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // Navigator.of(context).push(SearchModal());
        print("clicl");
      },
      child: Container(
        padding: EdgeInsets.all(9),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Theme.of(context).focusColor.withOpacity(0.2),
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 12, left: 0),
              child: Icon(Icons.search, color: Theme.of(context).accentColor),
            ),
            // Expanded(
            //   child: Text(
            //     "Enter Your Discount Code",
            //     maxLines: 1,
            //     softWrap: false,
            //     overflow: TextOverflow.fade,
            //     style: Theme.of(context).textTheme.caption.merge(TextStyle(fontSize: 12)),
            //   ),
            // ),

            Expanded(
              child: Container(
                height: 40,
                child: TextFormField(
                  controller: controller,
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Enter Your Discount Code'),
                ),
              ),
            ),
            SizedBox(width: 8),
            InkWell(
              onTap: () {
                onClickFilter(controller.text);
              },
              child: Container(
                padding: const EdgeInsets.only(
                    right: 10, left: 10, top: 5, bottom: 5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Theme.of(context).accentColor,
                ),
                child: false
                    ? CircularProgressIndicator()
                    : Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        spacing: 4,
                        children: [
                          Text(
                            "Apply",
                            style: TextStyle(color: Colors.white),
                          ),
                          Icon(
                            Icons.done,
                            color: Colors.white,
                            size: 21,
                          ),
                        ],
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CouponItemWidget extends StatefulWidget {
  final CouponListData? data;
  final Function? onApplyClick;

  const CouponItemWidget({Key? key, this.data, this.onApplyClick})
      : super(key: key);

  @override
  _CouponItemWidgetState createState() => _CouponItemWidgetState();
}

class _CouponItemWidgetState extends State<CouponItemWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).accentColor,
      focusColor: Theme.of(context).accentColor,
      highlightColor: Theme.of(context).primaryColor,
      /*onTap: () {
        Navigator.of(context).pushNamed('/Food', arguments: RouteArgument(id: widget.cart.food.id, heroTag: widget.heroTag));
      },*/
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.1),
                blurRadius: 5,
                offset: Offset(0, 2)),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            //SizedBox(width: 15),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: [
                              Container(
                                  padding: EdgeInsets.all(8),
                                  //margin: EdgeInsets.all(10),
                                  // width: 120,
                                  //height: 100,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(5),
                                        topRight: Radius.circular(5),
                                        bottomRight: Radius.circular(5),
                                        bottomLeft: Radius.circular(5)),
                                    color: Color.fromRGBO(227, 226, 230, 1.0),
                                  ),
                                  child: Row(
                                    children: [
                                      Text(
                                        widget.data?.code ?? '',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1,
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      // Container(
                                      //   height: 20,
                                      //   width: 20,
                                      //   decoration: BoxDecoration(
                                      //       image: DecorationImage(
                                      //           image: AssetImage(
                                      //               "assets/img/icon_cp.png"))),
                                      // )
                                    ],
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          // Text(
                          //   widget.data.description,
                          //   overflow: TextOverflow.ellipsis,
                          //   maxLines: 2,
                          //   style: Theme.of(context).textTheme.subtitle1,
                          // ),
                          //Helper.applyHtml1(context, widget.data.description),
                          Text(
                            "${widget.data?.discountRate} Off On Subtotal Minimum Order Value ${widget.data?.minOrderAmount ?? 0}, Maximum discount amount is ${widget.data?.discountRate}",
                            overflow: TextOverflow.visible,
                            // maxLines: 0,
                            style: Theme.of(context).textTheme.caption,
                          ),
                        ],
                      ),
                    ),
                  ),
                  //SizedBox(width: 8),
                  InkWell(
                    onTap: () {
                      widget.onApplyClick!();
                    },
                    child: Container(
                      width: 70,
                      padding: const EdgeInsets.only(
                          right: 10, left: 10, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        color: Theme.of(context).accentColor,
                      ),
                      alignment: Alignment.center,
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        spacing: 4,
                        children: [
                          Text(
                            "APPLY",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
