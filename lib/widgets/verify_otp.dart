import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/login_response.dart';
import 'package:true_digital_store/theme/color.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/home.dart';

class VerifyOTP extends StatefulWidget {
  final String OTP;
  final String phoneNumber;
  const VerifyOTP({Key? key,required this.OTP, required this.phoneNumber}) : super(key: key);

  @override
  _VerifyOTPState createState() => _VerifyOTPState();
}

class _VerifyOTPState extends State<VerifyOTP> {
  String? fillOtp;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Verify OTP'),
      ),
      //TODO: Keyboard should hide when click outside of textfield
      body: new GestureDetector(
        onTap: () {
          print('hide keyboard');
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     OTPDigitTextFieldBox(first: true, last: false),
            //     OTPDigitTextFieldBox(first: false, last: false),
            //     OTPDigitTextFieldBox(first: false, last: false),
            //     OTPDigitTextFieldBox(first: false, last: true),
            //   ],
            // ),
            OtpTextField(
              numberOfFields: 4,
              borderColor: Colors.green,
              //set to true to show as box or false to show as dash
              showFieldAsBox: false,
              //runs when a code is typed in
              onCodeChanged: (String code) {
                //handle validation or checks here
              },
              //runs when every textfield is filled
              onSubmit: (String verificationCode){
                fillOtp = verificationCode;
              }, // end onSubmit
            ),
            SizedBox(height: 30,),
            TextButton(
              onPressed: () async {
                FocusManager.instance.primaryFocus?.unfocus();
                CommonUtills.showprogressdialogcomplete(context, true);
                if (widget.OTP == fillOtp) {
                  LoginResponse? res = await LoginResponse.loginWith(context, widget.phoneNumber);
                  CommonUtills.showprogressdialogcomplete(context, false);
                  NetworkUtils.setLoginDetailsINTOSharedPrefrence(res ?? LoginResponse());
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
                    return HomeWidget();
                  }));
                  CommonUtills.successtoast(context, "Login Successfully");
                } else {
                  CommonUtills.errortoast(context, "Please enter valid OTP");
                }
               // Navigator.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Verify OTP'),
              ),
              style: ButtonStyle(
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                  backgroundColor:
                      MaterialStateProperty.all(AppColor.themePrimary)),
            )
          ]),
        ),
      ),
    );
  }
}

class OTPDigitTextFieldBox extends StatelessWidget {
  final bool first;
  final bool last;
  const OTPDigitTextFieldBox(
      {Key? key, required this.first, required this.last})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.width / 6,
      //width: MediaQuery.of(context).size.width / 6,
      child: AspectRatio(
        aspectRatio: 1.0,
        child:  TextField(
            autofocus: true,
            onChanged: (value) {
              if (value.length == 1 && last == false) {
                FocusScope.of(context).nextFocus();
              }
              if (value.length == 0 && first == false) {
                FocusScope.of(context).previousFocus();
              }
            },
            showCursor: false,
            readOnly: false,
            textAlign: TextAlign.center,
            style: TextStyle(fontFamily: AppTextStyle.Regular),
            keyboardType: TextInputType.number,
            maxLength: 1,
            decoration: InputDecoration(
               contentPadding: EdgeInsets.all(0),
              counter: Offstage(),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 2, color: AppColor.themePrimary),
                  borderRadius: BorderRadius.circular(10)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 2, color: AppColor.themePrimary),
                  borderRadius: BorderRadius.circular(10)),
              hintText: "-",
              // hintStyle: MyStyles.hintTextStyle,
            ),
          ),
        ),

    );
  }
}
