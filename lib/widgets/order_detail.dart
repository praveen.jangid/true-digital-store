import 'package:flutter/material.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/order_detail_result.dart';
import 'package:true_digital_store/model/orderlist_dto.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_constants.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/my_orders.dart';

class OrderDetailWidget extends StatefulWidget {
  OrderData? theOrder;

  OrderDetailWidget(this.theOrder);

  @override
  State<StatefulWidget> createState() {
    return OrderDetailState();
  }
}

class OrderDetailState extends State<OrderDetailWidget> {
  OrderDetailDto? orderDetail;

  @override
  void initState() {
    super.initState();
    _getOrderDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Order Detail"),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Card(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            color: Colors.red.withAlpha(10),
                            child: Image.network(
                              orderDetail?.data?.theStore
                                  ?.coverImage ??
                                  '',
                              width: 70,
                              height: 70,
                            )),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            orderDetail?.data?.theStore
                                ?.businessName ??
                                '',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                              width:
                              MediaQuery.of(context).size.width / 1.6,
                              child: Text(
                                  orderDetail?.data?.theStore?.address ??
                                      '',
                                  style: TextStyle(fontSize: 12))),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                              width:
                              MediaQuery.of(context).size.width / 1.6,
                              child: Text(
                                  formateDate(orderDetail?.data?.createdAt ?? ''),
                                  style: TextStyle(fontSize: 12))),
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(),
                SizedBox(
                  height: 10,
                ),
                Text(
                  orderStatus(int.parse(orderDetail?.data?.status ?? "0")),
                  style: TextStyle(fontFamily: AppTextStyle.SemiBold),
                ),

                // SizedBox(
                //   height: 10,
                // ),
                // Text(
                //   "Your order is successful and seller will confirm your order shortly",
                //   style: TextStyle(color: Colors.grey, fontSize: 13),
                // ),
                SizedBox(
                  height: 10,
                ),
                Divider(),
                Text(
                  "${this.orderDetail?.data?.orderItems?.length ?? 0} Items",
                  style: TextStyle(color: Colors.black),
                ),
                SizedBox(
                  height: 10,
                ),
                _buildOrderItemList(context),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      "Total Amount ",
                      style: TextStyle(color: Colors.black),
                    ),
                    Spacer(),
                    Text(
                      "${Constants.CURRENCY_SIGN}${widget.theOrder?.totalAmount ?? ""}",
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "YOUR DETAILS",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Visibility(
                  visible: widget.theOrder?.shipName != null && widget.theOrder?.shipName != "",
                  child: Row(
                    children: [
                      Text(
                        "Name :",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      ),
                      Spacer(),
                      Text(
                        widget.theOrder?.shipName ?? "",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: widget.theOrder?.shipPhone != null && widget.theOrder?.shipPhone != "",
                  child: SizedBox(
                    height: 10,
                  ),
                ),
                Visibility(
                  visible: widget.theOrder?.shipPhone != null && widget.theOrder?.shipPhone != "",
                  child: Row(
                    children: [
                      Text(
                        "Phone :",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      ),
                      Spacer(),
                      Text(
                        widget.theOrder?.shipPhone ?? "",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: widget.theOrder?.shipAddress != null && widget.theOrder?.shipAddress != "",
                  child: SizedBox(
                    height: 10,
                  ),
                ),
                Visibility(
                  visible: widget.theOrder?.shipAddress != null && widget.theOrder?.shipAddress != "",
                  child: Row(
                    children: [
                      Text(
                        "Address :",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      ),
                      Spacer(),
                      Text(
                        widget.theOrder?.shipAddress ?? "",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: widget.theOrder?.ship_location != null && widget.theOrder?.ship_location != "",
                  child: SizedBox(
                    height: 10,
                  ),
                ),
                Visibility(
                  visible: widget.theOrder?.ship_location != null && widget.theOrder?.ship_location != "",
                  child: Row(
                    children: [
                      Text(
                        "Location :",
                        style: TextStyle(fontFamily: AppTextStyle.Medium),
                      ),
                     // Spacer(),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Text(
                          widget.theOrder?.ship_location ?? "",
                          style: TextStyle(fontFamily: AppTextStyle.Medium),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      "Payment :",
                      style: TextStyle(fontFamily: AppTextStyle.Medium),
                    ),
                    Spacer(),
                    Text(
                      widget.theOrder?.getPaymentMode() ?? "",
                      style: TextStyle(fontFamily: AppTextStyle.Medium),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOrderItemList(context) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: this.orderDetail?.data?.orderItems?.length ?? 0,
        itemBuilder: (context, index) {
          OrderItem theCart = this.orderDetail!.data!.orderItems![index];
          return Card(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
              shape: RoundedRectangleBorder(side: BorderSide.none),
              elevation: 1,
              child: InkWell(
                onTap: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => LoginWidget()));
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        children: [
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: ClipPath(
                              clipper: ShapeBorderClipper(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5))),
                              child: theCart.productImage != null
                                  ? Image.network(
                                      NetworkUtils.base_image_url +
                                          (theCart.productImage ?? ""),
                                      width: 80,
                                      height: 100,
                                      fit: BoxFit.fill,
                                    )
                                  : Icon(
                                      Icons.image_outlined,
                                      size: 100,
                                    ),
                            ),
                            elevation: 2,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  theCart.productTitle ?? '',
                                  style: TextStyle(
                                      fontFamily: AppTextStyle.regular,
                                      fontSize: 15),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "${theCart.quantity} x ${Constants.CURRENCY_SIGN + theCart.sellPrice.toString()}",
                                      style: TextStyle(
                                          fontFamily: AppTextStyle.regular,
                                          fontSize: 15),
                                    ),
                                    Spacer(),
                                    Text(
                                      "${Constants.CURRENCY_SIGN + theCart.totalItemsPrice().toString()}",
                                      style: TextStyle(
                                          fontFamily: AppTextStyle.regular,
                                          fontSize: 15),
                                    )
                                    // SizedBox(
                                    //   width: 10,
                                    // ),
                                    // Text(
                                    //   Constants.CURRENCY_SIGN + '200',
                                    //   style: TextStyle(
                                    //     decoration: AppTextStyle.lineThrough,
                                    //     fontFamily: AppTextStyle.regular,
                                    //     fontSize: 15,
                                    //   ),
                                    // ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ));
        });
  }

  _getOrderDetail() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      NetworkUtils _netUtil = new NetworkUtils();
      String apiPath = NetworkUtils.kGetOrderDetail +
          '?order_id=' +
          widget.theOrder!.id!.toString();
      CommonUtills.showprogressdialogcomplete(context, true);
      _netUtil.get(apiPath, context).then((dynamic res) async {
        CommonUtills.showprogressdialogcomplete(context, false);
        orderDetail = OrderDetailDto.fromJson(res);
        if (orderDetail?.status == false) {
          CommonUtills.errortoast(context, orderDetail!.message!);
        }
        setState(() {});
      });
    } else {
      setState(() {});
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }
}


