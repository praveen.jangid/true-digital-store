import 'package:flutter/material.dart';
import 'package:true_digital_store/model/category_list_response.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/widgets/store.dart';

class CategoryWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CategoryState();
  }
}

class CategoryState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select Your Category'),
      ),
      body: SafeArea(
        child: ListView.builder(
            itemCount: 15,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => StoreWidget(CategoryData())));
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 5),
                  child: Row(
                    children: [
                      Image.network(
                        'https://images.unsplash.com/photo-1543968332-f99478b1ebdc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=150&q=80',
                        width: 60,
                        height: 40,
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        'Restaurant &  Hotels',
                        style: TextStyle(
                            fontFamily: AppTextStyle.regular, fontSize: 15),
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}
