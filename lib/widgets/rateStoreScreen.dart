import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/theme/color.dart';
import 'package:true_digital_store/utils/common_utills.dart';

class RateStoreScreen extends StatefulWidget {
  final String storeId;

  const RateStoreScreen({Key? key, required this.storeId}) : super(key: key);
  @override
  _RateStoreScreenState createState() => _RateStoreScreenState();
}

class _RateStoreScreenState extends State<RateStoreScreen> {
  final _formKey = new GlobalKey<FormState>();
  Color _borderColor = HexColor("#96D6D0");
  TextEditingController _nameController = TextEditingController();
  TextEditingController _commentsController = TextEditingController();
  double ratingis = 1.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rate'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
          ),
          Center(
            child: RatingBar.builder(
              initialRating: 1,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemSize: 40,
              itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
                ratingis = rating;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              controller: _nameController,
              //  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              style: TextStyle(color: Colors.black),
              decoration: textFieldDecoration('NAME.'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: TextFormField(
              controller: _commentsController,
              minLines: 3,
              maxLines: 3,
              //  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              style: TextStyle(color: Colors.black),
              decoration: textFieldDecoration('COMMENTS.'),
            ),
          ),
          InkWell(
            onTap: () async {
              var name = _nameController.text.trim();
              var comments = _commentsController.text.trim();

              if (name.isEmpty) {
                CommonUtills.errortoast(context, "Please enter name");
              } else if (comments.isEmpty) {
                CommonUtills.errortoast(context, "Please enter comments");
              } else {
                // name:PRadeep
                // message:Ganesh nagar
                // rating:2
                // store_id:41
_addFeedback();
                }

            },
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Center(
                      child: Text(
                    "Submit",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontFamily: 'Roboto',
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                    ),
                  )),
                ),
                color: AppColor.themePrimary,
              ),
            ),
          )
        ],
      ),
    );
  }

  _addFeedback() async {
    Map<String, dynamic> params = {"name" : _nameController.text,
      "message" : _commentsController.text,
      "rating" : ratingis.toString(),
      "store_id" : widget.storeId
    };
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.post(NetworkUtils.kAddFeedback, context, body: params).then((value) {
        CommonUtills.showprogressdialogcomplete(context, false);
        if (value['status'] == true) {
          CommonUtills.errortoast(
              context, value['message'] ?? 'Something went wrong, please try again');
          Navigator.pop(context);
        } else {
          CommonUtills.errortoast(
              context, value['message'] ?? 'Something went wrong, please try again');
        }
      }).onError((error, stackTrace) {
        CommonUtills.showprogressdialogcomplete(context, false);

        CommonUtills.errortoast(
            context, 'Something went wrong, please try again');
      });
    } else {
      CommonUtills.errortoast(context,
          'No internet connection available, please check and try again');
    }
  }

  InputDecoration textFieldDecoration(String hintText) {
    return InputDecoration(
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 1.0)),
      disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 0.0)),
      hintText: hintText,
      labelText: hintText,
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: _borderColor, width: 2.0)),
      border: OutlineInputBorder(),
    );
  }
}
