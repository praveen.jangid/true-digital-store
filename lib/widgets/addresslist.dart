import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:true_digital_store/model/address_list_response.dart';
import 'package:true_digital_store/model/cart_list_response.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/add_address.dart';
import 'package:true_digital_store/widgets/payment.dart';
import 'package:true_digital_store/widgets/paymentModeScreen.dart';

class AddressWidget extends StatefulWidget {
  bool fromCart;
  CartListResponse? cartResponse;
  AddressWidget(this.cartResponse, [this.fromCart = false]);

  @override
  State<StatefulWidget> createState() {
    return AddressState(fromCart);
  }
}

class AddressState extends State<AddressWidget> {
  List<Address> addresslist = [];

  Color _lightGray = HexColor('#767676');

  bool fromCart, loading = false;

  AddressState([this.fromCart = false]);

  @override
  void initState() {
    super.initState();
    _getAddressList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Addresses'),
          actions: [
            IconButton(
                onPressed: () {
                  Get.to(AddAddressWidget(), fullscreenDialog: true)?.then((value) {
                    _getAddressList();
                  });
                },
                icon: Icon(Icons.add))
          ],
        ),
        body: loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : addresslist.length > 0
                ? _listView()
                : Center(
                    child: Text(
                    'No addresses,\n Click + to add address',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16, fontFamily: AppTextStyle.SemiBold),
                  )));
  }

  Widget _listView() {
    return ListView.builder(
        itemCount: addresslist.length,
        itemBuilder: (context, index) {
          Address theAddress = addresslist[index];
          return InkWell(
            onTap: () {
              if (this.fromCart) {
                widget.cartResponse?.data?.addressId = theAddress.id?.toString();
                Get.to(PaymentModeScreen(cartResponse:  widget.cartResponse));
              }
            },
            child: Card(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 5),
              shape: RoundedRectangleBorder(side: BorderSide.none),
              elevation: 4,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    _textWidget(theAddress.name ?? '', AppTextStyle.SemiBold,
                        17, Colors.black),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        _textWidget(
                            'Mobile : ', AppTextStyle.Regular, 15, _lightGray),
                        _textWidget(theAddress.phone ?? '', AppTextStyle.Bold,
                            16, Colors.black)
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    _textWidget(theAddress.location ?? '', AppTextStyle.Regular,
                        15, _lightGray),
                    SizedBox(
                      height: 5,
                    ),
                    // _textWidget(theAddress.cityState(), AppTextStyle.Regular,
                    //     15, _lightGray),
                    Container(
                      width: 150,
                      child: Row(
                        children: [
                          // TextButton(
                          //     onPressed: () async {
                          //       var result = await Get.to(
                          //           AddAddressWidget(theAddress),
                          //           fullscreenDialog: true);
                          //       if (result != null) {
                          //         print('updateing resutl');
                          //       } else {
                          //         print(result);
                          //       }
                          //     },
                          //     child: Text('EDIT')),
                          TextButton(
                              onPressed: () {
                                showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text('Delete Address?'),
                                        content: Text(
                                            'Are you sure you want to delete this address?'),
                                        actions: [
                                          TextButton(
                                              onPressed: () {
                                                Navigator.pop(context, true);
                                              },
                                              child: Text('No')),
                                          TextButton(
                                              onPressed: () {
                                                Get.back();
                                                _deleteAddress(theAddress);
                                              },
                                              child: Text(
                                                'Yes',
                                                style: TextStyle(
                                                    fontFamily:
                                                        AppTextStyle.bold,
                                                    color: Colors.red),
                                              )),
                                        ],
                                      );
                                    });
                              },
                              child: Text(
                                'DELETE',
                                style: TextStyle(color: Colors.red),
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget _textWidget(
      String text, String fontFamily, double fontSize, Color textColor) {
    return Text(text,
        style: TextStyle(
            fontFamily: fontFamily, color: textColor, fontSize: fontSize));
  }

  _getAddressList() {
    setState(() {
      loading = true;
    });
    AddressListResponse.addressList(context, (response) {
      print(response);
      if (response is List<Address>) {
        setState(() {
          loading = false;
          addresslist = response as List<Address>;
        });
      } else {}
    });
  }

  _deleteAddress(Address theAddress) {
    CommonUtills.showprogressdialogcomplete(context, true);
    theAddress.deleteAddress(context, (response) {
      CommonUtills.showprogressdialogcomplete(context, false);
      if (response is AddressListResponse) {
        var temp = response as AddressListResponse;
        if (temp.status == true) {
          setState(() {
            addresslist.remove(theAddress);
          });
          CommonUtills.successtoast(context, temp.message.toString());
        } else {
          CommonUtills.successtoast(context, temp.message.toString());
        }
      } else {
        CommonUtills.successtoast(context, response.toString());
      }
    });
  }
}
