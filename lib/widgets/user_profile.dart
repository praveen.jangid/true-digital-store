import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_routes.dart';
import 'package:true_digital_store/widgets/about.dart';
import 'package:true_digital_store/widgets/addresslist.dart';
import 'package:true_digital_store/widgets/my_orders.dart';
import 'package:true_digital_store/widgets/notification_setting.dart';
import 'package:true_digital_store/widgets/suppoerScreen.dart';

import 'home.dart';

class UserProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserProfileState();
  }
}

class _UserProfileState extends State<UserProfile> {
  List<String> _menus = [
    'Recent Orders',
    'Saved Addresses',
   // 'Notification Settings',
    'Support',
    'About',
    'Log Out'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
        ),
        body: ListView.separated(
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  switch (index) {
                    case 0:
                      //Orders
                      Get.to(() => MyOrders(isShowAppBar: false,));
                      break;
                    case 1:
                      //address
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AddressWidget(null),
                          ));
                      break;
                    // case 2:
                    //   //notification setting
                    //   Navigator.push(
                    //       context,
                    //       MaterialPageRoute(
                    //         builder: (context) => NotificationSetting(),
                    //       ));
                    //   break;
                    case 2:
                      //support
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SupportScreen(),
                          ));
                      break;
                    case 3:
                      //about
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AboutWidget(),
                          ));
                      break;
                    case 4:
                      _logoutDialog();
                      break;
                  }
                },
                title: Text(_menus[index]),
                trailing: Icon(Icons.arrow_forward_ios),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
            itemCount: _menus.length));
  }

  _logoutDialog() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Log Out?'),
            content: Text('Are you sure you want to Log Out?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                  child: Text('No')),
              TextButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context, true);
                    });
                    NetworkUtils.logOut(context);

                  },
                  child: Text(
                    'Yes',
                    style: TextStyle(
                        fontFamily: AppTextStyle.bold, color: Colors.red),
                  )),
            ],
          );
        });
  }
}
