import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/cart_list_response.dart';
import 'package:true_digital_store/model/order_place_dto.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/theme/color.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_constants.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/addresslist.dart';

import 'cartIconWidget.dart';
import 'couponList.dart';
import 'coupon_list_response.dart';
import 'orderSuccessfully.dart';

class CartWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CartState();
  }
}

class CartState extends State<CartWidget> {
  Color _borderColor = HexColor("#96D6D0");
  bool apiCalling = false;
  CartListResponse? cartResponse;
  List<Cart>? cartList;

  TextEditingController ccController = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 500), () {
// Here you can write your code

      setState(() {
        _fetchCartItems();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('F5F5F5'),
      appBar: AppBar(
        title: Text('Cart'),
        actions: [
          Visibility(
            child: InkWell(
              onTap: () {
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text('Clear Cart?'),
                        content: Text(
                            'Are you sure you want to clear all cart items?'),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context, true);
                              },
                              child: Text('No')),
                          TextButton(
                              onPressed: () {
                                Get.back();
                                _clearCartApi();
                              },
                              child: Text(
                                'Yes',
                                style: TextStyle(
                                    fontFamily: AppTextStyle.bold,
                                    color: Colors.red),
                              )),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(child: Text('Clear Bag')),
              ),
            ),
            visible: (cartList?.length ?? 0) > 0 ? true : false,
          )
        ],
      ),
      body: SafeArea(
        child: cartList?.length == 0
            ? Center(
                child: Text('No items in cart'),
              )
            : SingleChildScrollView(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 10),
                      child: Row(
                        children: [
                          Text(
                              '${this.cartResponse?.data?.cart?.length.toString() ?? '0'} Items'),
                          Spacer(),
                          Text('Total : ' +
                              Constants.CURRENCY_SIGN +
                              '${this.cartResponse?.data?.totalPrice.toString() ?? ''}'),
                        ],
                      ),
                    ),
                  ),
                  if ((cartResponse?.data?.cart?.length ?? 0) > 0)
                    Card(
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                color: Colors.red.withAlpha(10),
                                child: Image.network(
                                  cartResponse?.data?.cart?.first?.store_details
                                          ?.coverImage ??
                                      '',
                                  width: 70,
                                  height: 70,
                                )),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                cartResponse?.data?.cart?.first.store_details
                                        ?.businessName ??
                                    '',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                  width:
                                      MediaQuery.of(context).size.width / 1.6,
                                  child: Text(
                                      cartResponse?.data?.cart?.first
                                              .store_details?.address ??
                                          '',
                                      style: TextStyle(fontSize: 12))),
                            ],
                          ),
                        ],
                      ),
                    ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          "Minimum ₹${cartResponse?.data?.cart?.first.store_details?.minCartAmount ?? ''} cart amount to free delivery"),
                    ),
                  ),
                  apiCalling
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : _buildCartList(this.context),
                  SizedBox(
                    height: 10,
                    child: Container(
                      color: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 16,
                        ),
                        Icon(Icons.payment),
                        SizedBox(
                          width: 10,
                        ),
                        Text(this.cartResponse?.data?.couponCode != null
                            ? 'Coupon ${this.cartResponse?.data?.couponCode} is applied'
                            : 'APPLY COUPON'),
                        Spacer(),
                        TextButton(
                            onPressed: () async {
                              if (this.cartResponse?.data?.couponCode != null) {
                                //remove  coupon
                                CommonUtills.confirmationAlert(
                                    context,
                                    "Remove Coupon",
                                    "Are you sure you want to remove coupon code?",
                                    (action) {
                                  if (action) {
                                    _applyCoupon("", "");
                                  }
                                });
                                return;
                              }
                              var result = await Get.to(() => CouponList(
                                    storeId:
                                        cartList?.first?.storeId?.toString() ??
                                            "0",
                                  ));
                              if (result != null && result is CouponListData) {
                                _applyCoupon(
                                    result.code ?? "", result.couponUuid ?? '');
                              } else if (result != null && result is String) {
                                _applyCoupon(result ?? "", '');
                              }
                            },
                            child: Text(
                                this.cartResponse?.data?.couponCode != null
                                    ? "Remove"
                                    : 'Apply'))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                    child: Container(
                      color: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'Sub Total',
                                style:
                                    TextStyle(fontFamily: AppTextStyle.Regular),
                              ),
                              Spacer(),
                              Text(
                                Constants.CURRENCY_SIGN +
                                    "${(cartResponse?.data?.subTotal ?? "")}",
                                style: TextStyle(fontFamily: AppTextStyle.Bold),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'GST',
                                style:
                                    TextStyle(fontFamily: AppTextStyle.Regular),
                              ),
                              Spacer(),
                              Text(
                                "${(cartResponse?.data?.gstCharges ?? "")}" !=
                                        ""
                                    ? "${(cartResponse?.data?.gstCharges ?? "")}%"
                                    : "",
                                style: TextStyle(fontFamily: AppTextStyle.Bold),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'Delivery Charge',
                                style:
                                    TextStyle(fontFamily: AppTextStyle.Regular),
                              ),
                              Spacer(),
                              Text(
                                "${(cartResponse?.data?.deliveryCharge ?? "")}" !=
                                        ""
                                    ? Constants.CURRENCY_SIGN +
                                        "${(cartResponse?.data?.deliveryCharge ?? "")}"
                                    : "",
                                style:
                                    TextStyle(fontFamily: AppTextStyle.regular),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'Discount',
                                style:
                                    TextStyle(fontFamily: AppTextStyle.Regular),
                              ),
                              Spacer(),
                              Text(
                                "${(cartResponse?.data?.discount ?? "")}" != ""
                                    ? Constants.CURRENCY_SIGN +
                                        "${(cartResponse?.data?.discount ?? "")}"
                                    : "",
                                style:
                                    TextStyle(fontFamily: AppTextStyle.regular),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'Total',
                                style:
                                    TextStyle(fontFamily: AppTextStyle.Regular),
                              ),
                              Spacer(),
                              Text(
                                Constants.CURRENCY_SIGN +
                                    "${(cartResponse?.data?.totalPrice ?? "")}",
                                style: TextStyle(fontFamily: AppTextStyle.Bold),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Row(
                            children: [
                              Text(
                                'Inclusive of all taxes',
                                style:
                                    TextStyle(fontFamily: AppTextStyle.Regular),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _chooseDeliveryOption();
                    },
                    child: Container(
                      height: 40,
                      color: AppColor.green,
                      child: Center(
                          child: Text(
                        'Place Order',
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontFamily: AppTextStyle.bold),
                      )),
                    ),
                  )
                ],
              )),
      ),
    );
  }

  void deleteProduct() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Delete Item?'),
            content: Text('Are you sure you want to delete this item?'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                  child: Text('No')),
              TextButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context, true);
                    });
                  },
                  child: Text(
                    'Yes',
                    style: TextStyle(
                        fontFamily: AppTextStyle.bold, color: Colors.red),
                  )),
            ],
          );
        });
  }

  void _chooseDeliveryOption() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: new Icon(Icons.place),
                title: new Text('Take away'),
                onTap: () {
                  Navigator.pop(context);
                  _placeOrder(2);
                },
              ),
              ListTile(
                leading: new Icon(Icons.place),
                title: new Text('Delivery'),
                onTap: () {
                  Navigator.pop(context);
                  _placeOrder(1);
                },
              ),
              ListTile(
                leading: new Icon(Icons.close),
                title: new Text('Cancel'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Widget _buildCartList(context) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: cartList?.length ?? 0,
        itemBuilder: (context, index) {
          Cart theCart = cartList![index];
          return Card(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
              shape: RoundedRectangleBorder(side: BorderSide.none),
              elevation: 1,
              child: InkWell(
                onTap: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => LoginWidget()));
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        children: [
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: ClipPath(
                              clipper: ShapeBorderClipper(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5))),
                              child: (theCart.productImages?.length ?? 0) > 0
                                  ? Image.network(
                                      NetworkUtils.base_image_url +
                                          ((theCart.productImages?.length ??
                                                      0) >
                                                  0
                                              ? theCart.productImages?.first
                                                      .productImage ??
                                                  ''
                                              : ""),
                                      width: 80,
                                      height: 100,
                                      fit: BoxFit.fill,
                                    )
                                  : Icon(
                                      Icons.image_outlined,
                                      size: 80,
                                    ),
                            ),
                            elevation: 2,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 200,
                                child: Text(
                                  theCart.productDetails?.productName ?? '',
                                  style: TextStyle(
                                      fontFamily: AppTextStyle.regular,
                                      fontSize: 15),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Text(
                                    Constants.CURRENCY_SIGN +
                                        theCart.sellPrice.toString(),
                                    style: TextStyle(
                                        fontFamily: AppTextStyle.regular,
                                        fontSize: 15),
                                  ),
                                  // SizedBox(
                                  //   width: 10,
                                  // ),
                                  // Text(
                                  //   Constants.CURRENCY_SIGN + '200',
                                  //   style: TextStyle(
                                  //     decoration: AppTextStyle.lineThrough,
                                  //     fontFamily: AppTextStyle.regular,
                                  //     fontSize: 15,
                                  //   ),
                                  // ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      theCart?.productDetails?.theStore =
                                          Store.fromJson(
                                              {"id": theCart.storeId});
                                      //deleteProduct();
                                      theCart?.productDetails?.cartItemId =
                                          theCart.id;
                                      await theCart?.productDetails
                                          ?.removeFromCart(context);
                                      cartRelodDone = () {
                                        setState(() {
                                          cartData;
                                        });
                                      };
                                      setState(() {
                                        cartData?.data?.cart?.remove(theCart);
                                        cartList?.remove(theCart);
                                      });
                                    },
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                      height: 30,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black12,
                                          ),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(200))),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 5,
                                          ),
                                          InkWell(
                                            onTap: () async {
                                              theCart.productDetails?.theStore =
                                                  Store.fromJson(
                                                      {"id": theCart.storeId});
                                              if (theCart.quantity == 1) {
                                                theCart.productDetails
                                                    ?.cartItemId = theCart.id;
                                                await theCart.productDetails
                                                    ?.removeFromCart(
                                                        this.context);
                                                _fetchCartItems();
                                                cartRelodDone = () {
                                                  setState(() {
                                                    cartData;
                                                  });
                                                };
                                                setState(() {
                                                  cartData?.data?.cart
                                                      ?.remove(theCart);
                                                });
                                              } else {
                                                setState(() {
                                                  theCart.quantity =
                                                      (theCart.quantity ?? 0) -
                                                          1;
                                                });

                                                var result = await theCart
                                                    .productDetails
                                                    ?.updateCartQuantity(
                                                        this.context,
                                                        (theCart.quantity ??
                                                            0));
                                                _fetchCartItems();
                                                cartRelodDone = () {
                                                  setState(() {
                                                    cartData;
                                                  });
                                                };
                                              }
                                            },
                                            child: Icon(
                                              Icons.remove,
                                              color: Colors.green,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          SizedBox(
                                            width: 30,
                                            child: TextFormField(
                                              textAlign: TextAlign.center,
                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  disabledBorder:
                                                      InputBorder.none,
                                                  focusedBorder:
                                                      InputBorder.none),
                                              initialValue:
                                                  theCart.quantity.toString(),
                                              readOnly: true,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          InkWell(
                                            onTap: () async {
                                              setState(() {
                                                theCart.quantity =
                                                    (theCart.quantity ?? 0) + 1;
                                              });
                                              theCart.productDetails?.theStore =
                                                  Store.fromJson(
                                                      {"id": theCart.storeId});
                                              var result = await theCart
                                                  .productDetails
                                                  ?.updateCartQuantity(
                                                      this.context,
                                                      (theCart.quantity ?? 0));
                                              _fetchCartItems();

                                              cartRelodDone = () {
                                                setState(() {
                                                  cartData;
                                                });
                                              };
                                            },
                                            child: Icon(
                                              Icons.add,
                                              color: Colors.green,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                        ],
                                      )),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ));
        });
  }

  _fetchCartItems() async {
    setState(() {
      cartList = [];
    });
    CommonUtills.showprogressdialogcomplete(context, true);
    CartListResponse.cartList(context, (response) {
      CommonUtills.showprogressdialogcomplete(context, false);
      log("RESULT:::  ${response}");

      if (response is CartListResponse) {
        this.cartResponse = response;
        log("CART RESPONSE RESULT::: ${this.cartResponse?.toJson()}");
        if (this.cartResponse?.status == true) {
          cartList = [];
          setState(() {
            this.cartResponse = response;
            apiCalling = false;
            cartList = this.cartResponse?.data?.cart ?? [];
          });
        } else {
          log("NO RESULT:::");
        }
      }
    });
  }

  _clearCartApi() {
    CommonUtills.showprogressdialogcomplete(context, true);
    CartListResponse.clearCartList(context, (response, status) {
      CommonUtills.showprogressdialogcomplete(context, false);
      if (reloadCart != null) {
        reloadCart!();
      }
      if (status) {
        setState(() {
          cartResponse = null;
          cartList?.clear();
        });
        CommonUtills.successtoast(context, response.toString());
      } else {
        CommonUtills.successtoast(context, response.toString());
      }
    });
  }

  /*
  * Takeway :- 2
  * Delivery :- 1
   */
  _placeOrder(int deliveryType) {
    if (deliveryType == 1) {
      Get.to(AddressWidget(cartResponse, true));
      return;
    }

    if (cartResponse == null) {
      print("Cart response :- $cartResponse");
      return;
    }

    Map<String, dynamic> params = {};
    params["coupon_code"] = cartResponse?.data?.couponCode ?? '';
    params["delivery_charge"] = "${cartResponse?.data?.deliveryCharge ?? 0}";
    params["discount_amount"] = "${(cartResponse?.data?.discount == null || cartResponse?.data?.discount == "")  ? '0' : cartResponse?.data?.discount ?? '0'}";
    params["coupon_uuid"] = cartResponse?.data?.couponUUID ?? '';
    params["address_id"] = "1";
    params["payment_mode"] = "3";
    params["delivery_type"] = "${deliveryType}";
    CommonUtills.showprogressdialogcomplete(context, true);
    CartListResponse.placeOrder(context, params, (response) {
      CommonUtills.showprogressdialogcomplete(context, false);
      print(response);
      if (response is OrderPlaceDto) {
        var res = response;
        if (res.status == true) {
          Get.to(OrderSuccessfully());
          setState(() {
            cartList = [];
          });
          //Get.back();
          // CommonUtills.successtoast(context, res?.message ?? "Order placed");
        } else {
          CommonUtills.successtoast(
              context, "Something went wrong, please try again");
        }
      }
    });
  }

  _applyCoupon(String coupon, String uuid) {
    String code = coupon; //ccController.text.trim();
    // if (code.isEmpty) {
    //   return;
    // }
    setState(() {
      apiCalling = true;
    });
    // CommonUtills.showprogressdialogcomplete(context, true);
    CartListResponse.applyCoupon(context, code, (response) {
      //  CommonUtills.showprogressdialogcomplete(context, false);
      if (response is CartListResponse) {
        setState(() {
          apiCalling = false;
        });
        if (response.data?.couponMessage == "Coupon code already used.") {
          CommonUtills.errortoast(
              context, response.data?.couponMessage?? "");
          return;
        }
        if (response.data?.couponMessage == "Cart amount is not sufficient to use this coupon code.") {
          CommonUtills.errortoast(
              context, response.data?.couponMessage?? "");
          return;
        }

        this.cartResponse = response as CartListResponse;
        if (this.cartResponse?.status == true) {
          setState(() {
            this.cartResponse?.data?.couponUUID = uuid;
            apiCalling = false;
            cartList = this.cartResponse?.data?.cart ?? [];
            // CommonUtills.errortoast(
            //     context, this.cartResponse?.data?.couponMessage ?? "");
          });
        } else {
          CommonUtills.errortoast(
              context, this.cartResponse?.data?.couponMessage ?? "");
        }
      }
    });
  }
}
