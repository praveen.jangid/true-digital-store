import 'package:flutter/material.dart';
import 'package:true_digital_store/theme/text_style.dart';

class PaymentWideget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Confirmation'),
      ),
      body: Center(
        child: Text(
          'Your order has been placed',
          style: TextStyle(fontFamily: AppTextStyle.bold, fontSize: 18),
        ),
      ),
    );
  }
}
