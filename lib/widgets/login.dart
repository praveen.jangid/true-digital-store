import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/verify_otp.dart';
import 'package:twilio_flutter/twilio_flutter.dart';
import 'dart:math';

class LoginWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }
}

class LoginState extends State<LoginWidget> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _mobileController = new TextEditingController();
  TwilioFlutter? twilioFlutter;
  bool isLoading = false;

  @override
  void initState() {
    twilioFlutter = TwilioFlutter(
        accountSid: NetworkUtils.kTwillioSID,
        authToken: NetworkUtils.kTwillioAuthToken,
        twilioNumber: '+18584139964');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 25,),
                Container(
                  //height: 150,
                  color: Colors.transparent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Icon(
                      //   Icons.mail_outline,
                      //   size: 80,
                      // ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              "Mobile Verification",
                              style: TextStyle(
                                  fontFamily: AppTextStyle.SemiBold,
                                  fontSize: 17),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Enter your mobile number",
                              style: TextStyle(
                                  fontFamily: AppTextStyle.Regular,
                                  fontSize: 14),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: '',
                          isDense: true,
                          prefixIconConstraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          prefixIcon: Text(
                            "+91  ",
                            style: TextStyle(
                                fontFamily: AppTextStyle.Regular, fontSize: 16),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        inputFormatters: [
                          FilteringTextInputFormatter.singleLineFormatter
                        ],
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: AppTextStyle.Regular),
                       // initialValue: '',
                        controller: _mobileController,
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return 'Please enter mobile';
                          }
                          return null;
                        },
                        // decoration: textFieldDecoration('Full Name'),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextButton(
                        onPressed: () async {
                          if (isLoading) {
                            return;
                          }
                          FocusManager.instance.primaryFocus?.unfocus();
                          if (_formKey.currentState!.validate()) {
                            int min = 1000; //min and max values act as your 6 digit range
                            int max = 9999;
                            var randomizer = new Random();
                            var rNum = min + randomizer.nextInt(max - min);

                            if (true ==
                                await CommonUtills.checkNetworkStatus(
                                    context)) {
                              setState(() {
                                isLoading = true;
                              });
                              twilioFlutter?.sendSMS(
                                  toNumber: '+91${_mobileController.text}',
                                  messageBody: 'Greetings From True Digital Store, OTP to login in True Digital Store app is ${rNum.toString()}').then((value) {
                                setState(() {
                                  isLoading = false;
                                });
                                    print("VALUE::::: $value");
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => VerifyOTP(OTP: rNum.toString(), phoneNumber: '+91${_mobileController.text}',)));
                              });
                            }
                          }
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => VerifyOTP()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text( isLoading ? "Please Wait..." : 'Send OTP'),
                        ),
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all(Colors.white),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.teal)),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
