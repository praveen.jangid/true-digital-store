import 'package:flutter/material.dart';
import 'package:true_digital_store/utils/app_routes.dart';

import 'home.dart';
import 'package:get/get.dart';
import 'my_orders.dart';


class OrderSuccessfully extends StatefulWidget {
  @override
  _OrderSuccessfullyState createState() => _OrderSuccessfullyState();
}

class _OrderSuccessfullyState extends State<OrderSuccessfully> {
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FloatingActionButton(
                elevation: 0.0,
                child: new Icon(Icons.check),
                backgroundColor: Colors.green,
                onPressed: (){}
            ),
            SizedBox(height: 15,),
            Text("Your order is successful!", style: TextStyle(fontWeight: FontWeight.bold),),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text("You will receive a conformation message shortly. For more details, check order status on your Phone", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.normal),),
            ),
            SizedBox(height: 15,),
            Container(
              width: MediaQuery.of(context).size.width - 50,
              child: OutlinedButton(
            onPressed: () {
              Get.to(() => MyOrders(isShowAppBar: false,));
              },
            style: ButtonStyle(
              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0),)),
            ),
            child: const Text("Track Order", style: TextStyle(color: Colors.black),),
          ),
        ),
            SizedBox(height: 5,),
            Container(
              width: MediaQuery.of(context).size.width - 50,
              child: OutlinedButton(
                onPressed: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
                    return HomeWidget();
                  }));
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith(getColor),
                 // shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0))),
                ),
                child: const Text("Continue shopping", style: TextStyle(color: Colors.white),),
              ),
            ),
          ],
        ),
      ),
    );
  }
  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.green;
    }
    return Colors.green;
  }
}
