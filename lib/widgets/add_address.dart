import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/address_list_response.dart';
import 'package:true_digital_store/theme/color.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/home.dart';

class AddAddressWidget extends StatefulWidget {
  Address? theAddress;

  AddAddressWidget([this.theAddress]);

  @override
  State<StatefulWidget> createState() {
    return AddAddressState(theAddress);
  }
}

class AddAddressState extends State<AddAddressWidget> {
  final _formKey = new GlobalKey<FormState>();
  Color _borderColor = HexColor("#96D6D0");
  double textFieldPadding = 25;
  PickResult? pickRes;

  Address? theAddress;
  GoogleMapController? mapController;
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _houseController = TextEditingController();
  TextEditingController _nameController = TextEditingController();

  AddAddressState(this.theAddress);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (theAddress == null) {
      theAddress = Address();
    }
    Future.delayed(const Duration(milliseconds: 500), () {
// Here you can write your code
      setState(() {
        _openAddressPicker();
      });
    });
  }

  _openAddressPicker() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return PlacePicker(
            apiKey: NetworkUtils.kPlaceApi,
            initialPosition: LatLng(26.345, 75.653),
            useCurrentLocation: true,
            selectInitialPosition: true,
            //usePlaceDetailSearch: true,
            onPlacePicked: (result) {
              // latitude = result.geometry?.location.lat;
              // longitude = result.geometry?.location.lng;
              mapController?.animateCamera(CameraUpdate.newLatLngZoom(
                  LatLng(result?.geometry?.location?.lat ?? 0,
                      result?.geometry?.location?.lng ?? 0),
                  14));

              print("SELECTED ADDRESS:::::: ${result.formattedAddress}");
              print(
                  "SELECTED ADDRESS:::::: ${result.addressComponents?[1].longName}");

              // selectedPlace = result;
              Navigator.of(context).pop();
              setState(() {
                pickRes = result;
                //_address = result.formattedAddress;
              });
            },
          );
        },
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Address'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 30, 15, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  InkWell(
                    onTap: () {
                      _openAddressPicker();
                    },
                    child: Container(
                      height: 200,
                      child: GoogleMap(
                        myLocationEnabled: true,

                        onMapCreated: _onMapCreated,
                        initialCameraPosition: CameraPosition(
                          target:
                              LatLng(latitude ?? -33.852, longitude ?? 151.211),
                          zoom: 15.0,
                        ),

                        // TODO(iskakaushik): Remove this when collection literals makes it to stable.
                        // https://github.com/flutter/flutter/issues/28312
                        // ignore: prefer_collection_literals
                        markers: Set<Marker>.of([
                          Marker(
                            markerId: MarkerId(pickRes?.formattedAddress ?? ''),
                            position: LatLng(
                              pickRes?.geometry?.location?.lat ?? latitude ?? 0,
                              pickRes?.geometry?.location?.lng ??
                                  longitude ??
                                  0,
                            ),
                            infoWindow: InfoWindow(
                                title: pickRes?.formattedAddress ?? '',
                                snippet: ''),
                            onTap: () {
                              //_onMarkerTapped(markerId);
                            },
                          )
                        ]), // YOUR MARKS IN MAP
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Icon(Icons.location_on_outlined),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(
                          pickRes?.formattedAddress ?? '',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16, fontFamily: AppTextStyle.SemiBold),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          _openAddressPicker();
                        },
                        child: Icon(Icons.edit),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),

                  TextFormField(
                    controller: _nameController,
                    //  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    style: TextStyle(color: Colors.black),
                    decoration: textFieldDecoration('Name'),
                  ),
                  SizedBox(
                    height: 15,
                  ),

                  TextFormField(
                    controller: _mobileController,
                    //  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    style: TextStyle(color: Colors.black),
                    decoration: textFieldDecoration('MOBILE NO.'),
                  ),
                  SizedBox(
                    height: textFieldPadding,
                  ),
                  TextFormField(
                    controller: _houseController,
                    inputFormatters: [
                      FilteringTextInputFormatter.singleLineFormatter
                    ],
                    style: TextStyle(color: Colors.black),
                    decoration: textFieldDecoration('HOUSE / FLAT / FLOOR NO.'),
                  ),

                  SizedBox(
                    height: textFieldPadding,
                  ),
                  // TextFormField(
                  //   // inputFormatters: [
                  //   //   FilteringTextInputFormatter.singleLineFormatter
                  //   // ],
                  //   style: TextStyle(color: Colors.black),
                  //   initialValue: '',
                  //   minLines: 4,
                  //   maxLines: 5,
                  //   decoration: textFieldDecoration('DIRECTIONS TO REACH (OPTIONAL)'),
                  // ),
                  //
                  // SizedBox(
                  //   height: textFieldPadding,
                  // ),
                  InkWell(
                    onTap: () {
                      saveAddress();
                    },
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Center(
                            child: Text(
                          "Submit",
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                      ),
                      color: AppColor.themePrimary,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  InputDecoration textFieldDecoration(String hintText) {
    return InputDecoration(
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 1.0)),
      disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 0.0)),
      hintText: hintText,
      labelText: hintText,
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: _borderColor, width: 2.0)),
      border: OutlineInputBorder(),
    );
  }

  void saveAddress() async {
    var name = _nameController.text.trim();
    var mobileNo = _mobileController.text.trim();
    var houseNo = _houseController.text.trim();
    var address = pickRes?.formattedAddress ?? "";

    if (name.isEmpty) {
      CommonUtills.errortoast(context, "Please enter name");
    } else if (mobileNo.isEmpty) {
      CommonUtills.errortoast(context, "Please enter mobile number");
    } else if (houseNo.isEmpty) {
      CommonUtills.errortoast(context, "Please enter House/Flat/Floor No.");
    } else {
      // String? address1 = pickRes?.addressComponents
      //     ?.firstWhere((element) => element.types.contains("premise"))
      //     .longName;
      // String? address2 = pickRes?.addressComponents
      //     ?.firstWhere((element) => element.types.contains("political"))
      //     .longName;

      // String? city = pickRes?.addressComponents
      //     ?.firstWhere((element) => element.types.contains("locality"))
      //     .longName;
      // String? state = pickRes?.addressComponents
      //     ?.firstWhere((element) =>
      //         element.types.contains("administrative_area_level_1"))
      //     .longName;
      // String? zipcode = pickRes?.addressComponents
      //     ?.firstWhere((element) => element.types.contains("postal_code"))
      //     .longName;

      // theAddress?.city = city;
      // theAddress?.state = state;
      // theAddress?.zipcode = zipcode;
      theAddress?.name = name;
      theAddress?.address = houseNo;
      theAddress?.phone = _mobileController.text.trim();
      theAddress?.location = address;
      theAddress?.latitude =
          ("${pickRes?.geometry?.location?.lat}" ?? "").toString();
      theAddress?.longitude =
          ("${pickRes?.geometry?.location?.lng}" ?? "").toString();

      CommonUtills.showprogressdialogcomplete(context, true);
      var result = await theAddress?.addAddress(context);
      CommonUtills.showprogressdialogcomplete(context, false);
      if (result != null) {
        Get.back();
      }
    }
  }
}
