import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/model/store_subcategory_product_list.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_assets.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/product_detail.dart';
import 'package:true_digital_store/widgets/stoer_item_search.dart';

import 'Component/itemListWidget.dart';
import 'Component/itemWidget.dart';
import 'cartIconWidget.dart';

class ProductWidget extends StatefulWidget {
  StoreCategory? theStoreCategory;
  Store? theStore;

  ProductWidget(this.theStore, this.theStoreCategory);

  @override
  State<StatefulWidget> createState() {
    return ProductState();
  }
}

class ProductState extends State<ProductWidget> {
  List<StoreSubcategoryData> subCategoryList = [];
  List<Product> categoryProductList = [];
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    fetchSubCategoryAndProducts();
    filterCategoryProducts();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: subCategoryList.length,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Products'),
            actions: [
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (ctx) => StoreItemSearchWidget(theStore: widget.theStore,)));
                  },
                  icon: Icon(Icons.search)),
              cartIconWidget()
            ],
          ),
          body: SafeArea(
            child: isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: [
                        Container(
                          height: 90,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              widget.theStoreCategory?.categoryImage == null
                                  ? Image.asset(
                                      Assets.noimage,
                                      height: 80,
                                      width: 80,
                                    )
                                  : Image.network(
                                      widget.theStoreCategory?.categoryImage ??
                                          "",
                                      width: 80,
                                      height: 80,
                                      fit: BoxFit.cover,
                                    ),
                              SizedBox(
                                width: 15,
                              ),
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      builder: (context) {
                                        return Container(
                                          child: ListView.separated(
                                              itemBuilder: (context, index) {
                                                StoreCategory theCategory =
                                                    widget.theStore!
                                                        .storeCategory![index];
                                                return InkWell(
                                                  onTap: () {
                                                    widget.theStoreCategory =
                                                        theCategory;
                                                    setState(() {
                                                      subCategoryList.clear();
                                                    });
                                                    fetchSubCategoryAndProducts();
                                                    filterCategoryProducts();
                                                    Navigator.pop(context);
                                                  },
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Row(
                                                      children: [
                                                        theCategory.categoryImage ==
                                                                null
                                                            ? Image.asset(
                                                                Assets.noimage,
                                                                height: 30,
                                                                width: 30,
                                                              )
                                                            : Image.network(
                                                                theCategory
                                                                        .categoryImage ??
                                                                    "",
                                                                width: 30,
                                                                height: 30,
                                                                fit: BoxFit
                                                                    .cover,
                                                              ),
                                                        SizedBox(
                                                          width: 15,
                                                        ),
                                                        Text(
                                                          theCategory.name ??
                                                              "All",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  AppTextStyle
                                                                      .Bold,
                                                              fontSize: 18),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                              separatorBuilder:
                                                  (context, index) {
                                                return Divider();
                                              },
                                              itemCount: widget.theStore
                                                      ?.storeCategory?.length ??
                                                  0),
                                        );
                                      });
                                },
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          widget.theStoreCategory?.name ?? "",
                                          style: TextStyle(
                                              fontFamily: AppTextStyle.Bold,
                                              fontSize: 18),
                                        ),
                                        Icon(
                                          Icons.keyboard_arrow_down_rounded,
                                          size: 30,
                                        )
                                      ],
                                    ),
                                    Text(
                                      widget?.theStore?.businessName ?? '',
                                      style: TextStyle(
                                          fontFamily: AppTextStyle.Bold,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        subCategoryList.length > 0
                            ? Expanded(
                                child: Column(
                                  children: [
                                    TabBar(
                                        isScrollable: true,
                                        tabs: subCategoryList
                                            .map((e) => Container(
                                                  height: 35,
                                                  child: Center(
                                                    child: Text(
                                                      e.name ?? "All",
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                ))
                                            .toList()),
                                    Expanded(
                                        child: subCategoryList.length == 0
                                            ? Center(
                                                child: Text("No records"),
                                              )
                                            : TabBarView(
                                                children: subCategoryList
                                                    .map((e) =>
                                                        productGridView(e))
                                                    .toList(),
                                              ))
                                  ],
                                ),
                              )
                            : categoryProductList.length > 0
                                ? Expanded(child: categoryProductGridView())
                                : Expanded(
                                    child: Center(
                                      child: Text("No products"),
                                    ),
                                  ),
                      ],
                    ),
                  ),
          ),
        ));
  }

  Widget productGridView(StoreSubcategoryData modelObject) {
    return ListView.builder(
        itemCount: modelObject.products?.length ?? 0,
        // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //     childAspectRatio: 0.8,
        //     crossAxisCount: 2,
        //     crossAxisSpacing: 5,
        //     mainAxisSpacing: 15),
        itemBuilder: (context, index) {
          Product? theProduct = modelObject.products![index];
          theProduct?.theStore = this.widget.theStore;
          return InkWell(
            onTap: () => Get.to(() => ProductDetailWidget(
                theProduct, widget.theStore?.id?.toString() ?? '')),
            child:
            itemListWidget(theProduct, widget.theStore?.id?.toString() ?? ''),
          );
        });
  }

  Widget categoryProductGridView() {
    return ListView.builder(
        itemCount: categoryProductList.length ?? 0,
        shrinkWrap: true,
        // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //     childAspectRatio: 0.8,
        //     crossAxisCount: 2,
        //     crossAxisSpacing: 5,
        //     mainAxisSpacing: 15),
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () => Get.to(() => ProductDetailWidget(
                categoryProductList[index],
                widget?.theStore?.id?.toString() ?? '')),
            child: itemListWidget(categoryProductList[index],
                widget.theStore?.id?.toString() ?? ''),
          );
        });
  }

  void fetchSubCategoryAndProducts() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      // CommonUtills.showprogressdialogcomplete(context, true);
      setState(() {
        isLoading = true;
      });
      NetworkUtils _netUtil = new NetworkUtils();
      String path =
          "${NetworkUtils.kStoreSubcategoryProduct}?product_category_id=${widget.theStoreCategory!.id!}";

      // String path =
      //     "${NetworkUtils.kStoreSubcategoryProduct}?product_category_id=79";
      _netUtil.get(path, context).then((dynamic res) async {
        //  CommonUtills.showprogressdialogcomplete(context, false);

        StoreSubcategoryProductList results =
            new StoreSubcategoryProductList.fromJson(res);
        if (results.status == true) {
          setState(() {
            this.subCategoryList = results.data ?? [];
            isLoading = false;
          });
        } else {
          CommonUtills.errortoast(context, results.message ?? '');
        }
        setState(() {
          isLoading = false;
        });
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
      setState(() {
        isLoading = true;
      });
    }
  }

  void filterCategoryProducts() {
    var temp = widget.theStore?.products
        ?.where((element) => element.categoryId == widget.theStoreCategory?.id)
        .toList();
    categoryProductList = temp ?? [];
  }
}
