import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:true_digital_store/model/product.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_constants.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/cart.dart';

import 'cartIconWidget.dart';

class ProductDetailWidget extends StatefulWidget {
  Product? theProduct;
  final String storeid;

  ProductDetailWidget(this.theProduct, @required this.storeid);

  @override
  State<StatefulWidget> createState() {
    return _ProductDetailState(theProduct);
  }
}

class _ProductDetailState extends State<ProductDetailWidget> {
  Product? theProduct;

  List<String> images = [];

  int _current = 0;

  _ProductDetailState(this.theProduct);

  @override
  void initState() {
    super.initState();
    this.theProduct?.productImages?.forEach((element) {
      this.images.add(element.getProductImage());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Detail'),
        actions: [
          cartIconWidget()
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _buildProductImageSlider(),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [_productInfoText()],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildProductImageSlider() {
    return Stack(
      children: [
        CarouselSlider(
          items: imagesSlide(),
          options: CarouselOptions(
              autoPlay: true,
              height: 200,
              viewportFraction: 1,
              enlargeCenterPage: true,
              aspectRatio: 1.0,
              // disableCenter: true,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
        ),
        Positioned(
          bottom: 5,
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: images.map((url) {
                int index = images.indexOf(url);
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index ? Colors.white : Colors.grey,
                  ),
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }

  List<Widget> imagesSlide() {
    return images
        .map((item) => Image.network(item)
        // GFImageOverlay(
        //       height: 150,
        //       width: MediaQuery.of(context).size.width,
        //       shape: BoxShape.rectangle,
        //       image: NetworkImage(item),
        //       boxFit: BoxFit.fitHeight,
          //   )
    )

        .toList();
  }

  Widget _productInfoText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                theProduct?.productName ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: AppTextStyle.Bold,
                    fontSize: 18),
              ),
            ),
            Stack(
              children: [
                InkWell(
                  onTap: () {
                    if (widget.theProduct?.theStore?.shop_status !=
                        "OPEN") {
                      CommonUtills.errortoast(
                          context, "Store is closed");
                      return;
                    }
                    theProduct!.addToCart(context, 1, widget.storeid ?? '');
                  },
                  child: Container(
                      height: 30,
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black12,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(200))),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.add,
                            color: Colors.green,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Add',
                            style: TextStyle(
                                fontSize: 13,
                                color: Colors.green,
                                fontFamily: AppTextStyle.regular),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                        ],
                      )),
                ),
                Visibility(
                  visible: theProduct?.status == "0",
                  child: Container(
                      margin: EdgeInsets.only(bottom: 10, right: 5),
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text("Out of stock", style: TextStyle(color: Colors.red, fontSize: 14),),
                      )),
                )
              ],
            ),
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          theProduct?.weight ?? "",
          style: TextStyle(fontFamily: AppTextStyle.Regular, fontSize: 14),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          Constants.CURRENCY_SIGN + (theProduct?.sellPrice ?? ""),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: AppTextStyle.Bold,
              fontSize: 16),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          theProduct?.description ?? "",
          style: TextStyle(fontFamily: AppTextStyle.Regular, fontSize: 14),
        )
      ],
    );
  }

  Widget _bottomCartView() {
    return Container(
      height: 60,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.8),
          spreadRadius: 0,
          blurRadius: 8,
          offset: Offset(0, 4),
        )
      ], color: Colors.white),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Icon(Icons.add_shopping_cart_outlined),
            SizedBox(
              width: 10,
            ),
            Text('1 Item'),
            SizedBox(
              width: 10,
            ),
            Text(Constants.CURRENCY_SIGN + '200'),
            Spacer(),
            TextButton(
                onPressed: () => Get.to(CartWidget()), child: Text('View Cart'))
          ],
        ),
      ),
    );
  }
}
