import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:true_digital_store/theme/text_style.dart';

class LocationWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LocationState();
  }
}

class LocationState extends State<LocationWidget> {
  List<String> locatoins = [
    'Jaipur',
    'Delhi',
    'Mumbai',
    'Bangalore',
    'Hyderabad'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add your location'),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            //TODO : Textfield style
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: TextFormField(
                onChanged: (String value) {
                  print(value);
                },
                decoration: InputDecoration(
                  hintText: 'Enter location',
                  isDense: true,
                  prefixIconConstraints:
                      BoxConstraints(minWidth: 0, minHeight: 0),
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.singleLineFormatter
                ],
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: AppTextStyle.Regular),
                initialValue: '',
                // decoration: textFieldDecoration('Full Name'),
              ),
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: locatoins.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pop(context, locatoins[index]);
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 10, 15, 5),
                          child: Row(
                            children: [
                              Icon(Icons.place_outlined),
                              SizedBox(
                                width: 15,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    locatoins[index],
                                    style: TextStyle(
                                        fontFamily: AppTextStyle.regular,
                                        fontSize: 15),
                                  ),
                                  Text(
                                    'Rajasthan, India',
                                    style: TextStyle(
                                        fontFamily: AppTextStyle.regular,
                                        fontSize: 15),
                                  ),
                                  //TODO : Divider not showing
                                  Divider(
                                    height: 1,
                                    color: Colors.grey,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }))
          ],
        ),
      ),
    );
  }
}
