import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:getwidget/getwidget.dart';
import 'package:location/location.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/category_list_response.dart';
import 'package:true_digital_store/model/store.dart';
import 'package:true_digital_store/model/store_list_response.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_routes.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/home.dart';
import 'package:true_digital_store/widgets/search.dart';

import 'Component/storeWidget.dart';
import 'StoreCategoryAndItems.dart';

class StoreWidget extends StatefulWidget {
  late CategoryData theCategory;

  StoreWidget(this.theCategory);

  @override
  State<StatefulWidget> createState() {
    return StoreState(theCategory);
  }
}

class StoreState extends State<StoreWidget> {

  late CategoryData theCategory;
  bool isApiCalling = false;
  List<Store> storeList = [];

  List<String> images = [
    "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
    "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
    "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg"
  ];

  int _current = 0;

  StoreState(this.theCategory);

  @override
  void initState() {
    super.initState();
    _getStoreList();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(theCategory.name ?? ""),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: searchBar(),
            ),
            // Stack(
            //   children: [
            //     CarouselSlider(
            //       items: imagesSlide(),
            //       options: CarouselOptions(
            //           autoPlay: true,
            //           height: 150,
            //           viewportFraction: 1,
            //           enlargeCenterPage: true,
            //           aspectRatio: 1.0,
            //           // disableCenter: true,
            //           onPageChanged: (index, reason) {
            //             setState(() {
            //               _current = index;
            //             });
            //           }),
            //     ),
            //     Positioned(
            //       bottom: 5,
            //       child: Container(
            //         width: MediaQuery.of(context).size.width,
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.center,
            //           children: images.map((url) {
            //             int index = images.indexOf(url);
            //             return Container(
            //               width: 8.0,
            //               height: 8.0,
            //               margin: EdgeInsets.symmetric(
            //                   vertical: 10.0, horizontal: 2.0),
            //               decoration: BoxDecoration(
            //                 shape: BoxShape.circle,
            //                 color:
            //                     _current == index ? Colors.white : Colors.grey,
            //               ),
            //             );
            //           }).toList(),
            //         ),
            //       ),
            //     ),
            //   ],
            // ),
            Expanded(
              child: isApiCalling
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : _renderGridView(),
            ),
          ],
        ),
      ),
    );
  }

  _renderGridView() {
    return storeList.length == 0
        ? Center(
            child: Text('No store available'),
          )
        : StaggeredGridView.countBuilder(
            shrinkWrap: true,
            padding: EdgeInsets.all(10),
            itemCount: storeList.length,
            staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0,
            crossAxisCount: 2,
            itemBuilder: (context, index) {
              return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                StioreCategoryAndItems(storeList[index])));
                  },
                  child: storeWidget(storeList[index]));
              ;
            });
  }

  List<Widget> imagesSlide() {
    return images
        .map((item) => GFImageOverlay(
              height: 150,
              width: MediaQuery.of(context).size.width,
              shape: BoxShape.rectangle,
              image: NetworkImage(item),
              boxFit: BoxFit.cover,
            ))
        .toList();
  }

  Widget searchBar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        InkWell(
          onTap: () {
            AppRoutes.goto(this.context, SearchWidget());
          },
          child: Container(
              height: 40,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(200))),
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Icon(Icons.search),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Search for store/items',
                    style: TextStyle(
                        fontSize: 13,
                        color: Colors.grey,
                        fontFamily: AppTextStyle.regular),
                  )
                ],
              )),
        )
      ],
    );
  }

  _getStoreList() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      setState(() {
        isApiCalling = true;
      });
      var lati = latitude ?? null;
      var lng = longitude ?? null;

      NetworkUtils _netUtil = new NetworkUtils();
      String apiPath = NetworkUtils.kStoreListByCategory +
          '?category_id=' +
          theCategory.id.toString();
      if (lati != null && lng != null) {
        apiPath = apiPath + "&lat=${lati}&long=${lng}";
      }
      _netUtil.get(apiPath, context).then((dynamic res) async {
        setState(() {
          isApiCalling = false;
        });
        StoreListResponse results = new StoreListResponse.fromJson(res);
        if (results.status == true) {
          if (results.data != null && results.data!.length > 0) {
            setState(() {
              storeList.addAll(results.data!);
            });
          }
        } else {
          CommonUtills.errortoast(context, results?.message ?? '');
        }
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }
}
