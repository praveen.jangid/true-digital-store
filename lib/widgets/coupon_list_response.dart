import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/utils/common_utills.dart';

/// status : true
/// message : "Coupon List by store"
/// data : [{"id":1,"coupon_uuid":"b2293072a69b4cff8fb3b4c12f91e2e4","store_id":47,"couponTitle":"truedigitalstore","code":"afjkejeoje","discountType":"1","discountRate":100,"discountLimit":"1","expiryDate":"14/08/2021","minOrderAmount":"100","termsCondition":"lskfefpkfkfe'gk'pfkeepkf","status":"1","created_at":"2021-08-23T07:01:03.000000Z","updated_at":"2021-08-23T07:01:03.000000Z"}]

class CouponListResponse {
  bool? status;
  String? message;
  List<CouponListData>? data;

  CouponListResponse({
      this.status, 
      this.message, 
      this.data});

  CouponListResponse.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(CouponListData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }


  static Future<dynamic> couponList(
      BuildContext context, String storeId, Function(dynamic) completion) async {
    bool networkStatus = await CommonUtills.checkNetworkStatus();
    if (networkStatus == true) {
      NetworkUtils _networkUtils = NetworkUtils();
      _networkUtils.get(NetworkUtils.kGetCouponList + "?store_id=${storeId}", context).then((value) {
        CouponListResponse response = CouponListResponse.fromJson(value);
        log("COUPON RESSSS::::::::: ${response.toJson()}");
        if (response.status == true) {
          completion(response);
        } else {
          completion(response.message);
        }
      }).onError((error, stackTrace) {
        log("CART ERROR::::::::: ${error}");

        completion(error);
      });
    } else {
      completion(
          'No internet connection available, please check and try again');
    }
  }


}

/// id : 1
/// coupon_uuid : "b2293072a69b4cff8fb3b4c12f91e2e4"
/// store_id : 47
/// couponTitle : "truedigitalstore"
/// code : "afjkejeoje"
/// discountType : "1"
/// discountRate : 100
/// discountLimit : "1"
/// expiryDate : "14/08/2021"
/// minOrderAmount : "100"
/// termsCondition : "lskfefpkfkfe'gk'pfkeepkf"
/// status : "1"
/// created_at : "2021-08-23T07:01:03.000000Z"
/// updated_at : "2021-08-23T07:01:03.000000Z"

class CouponListData {
  int? id;
  String? couponUuid;
  int? storeId;
  String? couponTitle;
  String? code;
  String? discountType;
  int? discountRate;
  String? discountLimit;
  String? expiryDate;
  String? minOrderAmount;
  String? termsCondition;
  String? status;
  String? createdAt;
  String? updatedAt;

  CouponListData({
      this.id, 
      this.couponUuid, 
      this.storeId, 
      this.couponTitle, 
      this.code, 
      this.discountType, 
      this.discountRate, 
      this.discountLimit, 
      this.expiryDate, 
      this.minOrderAmount, 
      this.termsCondition, 
      this.status, 
      this.createdAt, 
      this.updatedAt});

  CouponListData.fromJson(dynamic json) {
    id = json['id'];
    couponUuid = json['coupon_uuid'];
    storeId = json['store_id'];
    couponTitle = json['couponTitle'];
    code = json['code'];
    discountType = json['discountType'];
    discountRate = json['discountRate'];
    discountLimit = json['discountLimit'];
    expiryDate = json['expiryDate'];
    minOrderAmount = json['minOrderAmount'];
    termsCondition = json['termsCondition'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['coupon_uuid'] = couponUuid;
    map['store_id'] = storeId;
    map['couponTitle'] = couponTitle;
    map['code'] = code;
    map['discountType'] = discountType;
    map['discountRate'] = discountRate;
    map['discountLimit'] = discountLimit;
    map['expiryDate'] = expiryDate;
    map['minOrderAmount'] = minOrderAmount;
    map['termsCondition'] = termsCondition;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

}