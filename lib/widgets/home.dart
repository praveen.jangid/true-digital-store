
import 'dart:developer';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart' as geo;
import 'package:getwidget/getwidget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:location/location.dart';
import 'package:true_digital_store/api/network_utils.dart';
import 'package:true_digital_store/model/banner_list_response.dart';
import 'package:true_digital_store/model/category_list_response.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_assets.dart';
import 'package:true_digital_store/utils/app_routes.dart';
import 'package:true_digital_store/utils/common_utills.dart';
import 'package:true_digital_store/widgets/cart.dart';
import 'package:true_digital_store/widgets/my_orders.dart';
import 'package:true_digital_store/widgets/search.dart';
import 'package:true_digital_store/widgets/store.dart';
import 'package:true_digital_store/widgets/user_profile.dart';
import 'package:share/share.dart';
import 'cartIconWidget.dart';
import 'login.dart';

double? latitude, longitude;

class HomeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<HomeWidget> {
  int _currentIndex = 0;
  final List<Widget> _children = [];

  bool isShowHeader = true;
  List<String> images = [];
  List<CategoryData> _categoriesList = [];
  LocationData? _currentPosition;
  Location location = Location();
  int _current = 0;
  String? _address;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getBanners();
    _getCategories();

    //LoginResponse.getUserDetail(context);
    getLoc();
  }

  getLoc() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _currentPosition = await location.getLocation();
    location.getLocation().then((LocationData currentLocation) {
      setState(() {
        _currentPosition = currentLocation;
        latitude = currentLocation.latitude;
        longitude = currentLocation.longitude;
        _getAddress(_currentPosition?.latitude, _currentPosition?.longitude)
            .then((value) {
          setState(() {
            _address = "${value.first.toJson().values.toList().join(", ")}";
          });
        });
      });
    });
    // location.onLocationChanged.listen((LocationData currentLocation) {
    //   print("${currentLocation.longitude} : ${currentLocation.longitude}");
    //   setState(() {
    //     _currentPosition = currentLocation;
    //
    //
    //     _getAddress(_currentPosition?.latitude, _currentPosition?.longitude)
    //         .then((value) {
    //       setState(() {
    //         _address = "${value.first.toJson().values.toList().join(", ")}";
    //       });
    //     });
    //   });
    // });
  }

  Future<List<geo.Placemark>> _getAddress(double? lat, double? lang) async {
    List<geo.Placemark> add =
        await geo.placemarkFromCoordinates(lat ?? 0.0, lang ?? 0.0);
    print("Address:::: ${add.first.toJson()}");
    // final coordinates = new Coordinates(lat, lang);
    //  List<Address> add =
    // await geo.
    return add;
  }

  _getBanners() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _netUtil = new NetworkUtils();
      _netUtil
          .get(NetworkUtils.kGetBannerList, context)
          .then((dynamic res) async {
        CommonUtills.showprogressdialogcomplete(context, false);

        BannerListResponse results = new BannerListResponse.fromJson(res);

        if (results.status == true) {
          results?.data?.forEach((element) {
            images.add(element.bannerImage ?? '');
          });
        } else {
          CommonUtills.errortoast(context, results?.message ?? '');
        }
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }

  _getCategories() async {
    if (true == await CommonUtills.checkNetworkStatus()) {
      CommonUtills.showprogressdialogcomplete(context, true);
      NetworkUtils _netUtil = new NetworkUtils();
      _netUtil
          .get(NetworkUtils.kGetCategoryList, context)
          .then((dynamic res) async {
        CommonUtills.showprogressdialogcomplete(context, false);

        CategoryListResponse results = new CategoryListResponse.fromJson(res);

        if (results.status == true) {
          setState(() {
            _categoriesList = results?.data ?? [];
          });
        } else {
          CommonUtills.errortoast(context, results?.message ?? '');
        }
      });
    } else {
      CommonUtills.errortoast(
          context, "Internet connection appear to be offline!");
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: null,
        automaticallyImplyLeading: false,
        title: InkWell(
          onTap: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         fullscreenDialog: true,
            //         builder: (context) => LocationWidget()));
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return PlacePicker(
                    apiKey: NetworkUtils.kPlaceApi,
                    initialPosition: LatLng(26.345, 75.653),
                    useCurrentLocation: true,
                    selectInitialPosition: true,
                    //usePlaceDetailSearch: true,
                    onPlacePicked: (result) {
                      latitude = result.geometry?.location.lat;
                      longitude = result.geometry?.location.lng;
                      print(
                          "SELECTED ADDRESS:::::: ${result.formattedAddress}");
                      print(
                          "SELECTED ADDRESS:::::: ${result.addressComponents?[1].longName}");

                      // selectedPlace = result;
                      Navigator.of(context).pop();
                      setState(() {
                        _address = result.formattedAddress;
                      });
                    },
                    //forceSearchOnZoomChanged: true,
                    //automaticallyImplyAppBarLeading: false,
                    //autocompleteLanguage: "ko",
                    //region: 'au',
                    //selectInitialPosition: true,
                    // selectedPlaceWidgetBuilder: (_, selectedPlace, state, isSearchBarFocused) {
                    //   print("state: $state, isSearchBarFocused: $isSearchBarFocused");
                    //   return isSearchBarFocused
                    //       ? Container()
                    //       : FloatingCard(
                    //           bottomPosition: 0.0, // MediaQuery.of(context) will cause rebuild. See MediaQuery document for the information.
                    //           leftPosition: 0.0,
                    //           rightPosition: 0.0,
                    //           width: 500,
                    //           borderRadius: BorderRadius.circular(12.0),
                    //           child: state == SearchingState.Searching
                    //               ? Center(child: CircularProgressIndicator())
                    //               : RaisedButton(
                    //                   child: Text("Pick Here"),
                    //                   onPressed: () {
                    //                     // IMPORTANT: You MUST manage selectedPlace data yourself as using this build will not invoke onPlacePicker as
                    //                     //            this will override default 'Select here' Button.
                    //                     print("do something with [selectedPlace] data");
                    //                     Navigator.of(context).pop();
                    //                   },
                    //                 ),
                    //         );
                    // },
                    // pinBuilder: (context, state) {
                    //   if (state == PinState.Idle) {
                    //     return Icon(Icons.favorite_border);
                    //   } else {
                    //     return Icon(Icons.favorite);
                    //   }
                    // },
                  );
                },
              ),
            );
          },
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.place),
              Container(
                width: MediaQuery.of(context).size.width / 2.7,
                child: Text(
                  _address ?? '',
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                  maxLines: 1,
                ),
              ),
              Icon(Icons.keyboard_arrow_down_outlined)
            ],
          ),
        ),
        actions: [
          // IconButton(
          //     onPressed: () {
          //       Navigator.push(context,
          //           MaterialPageRoute(builder: (context) => LoginWidget()));
          //     },
          //     icon: Icon(Icons.supervised_user_circle)),
          IconButton(
              onPressed: () {
                log("${NetworkUtils.singupModel}");
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) =>  NetworkUtils.singupModel == null ? LoginWidget() : UserProfile()));
              },
              icon: Icon(Icons.person_outline)),

          InkWell(
            onTap: () {
              log("${NetworkUtils.singupModel}");
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) =>  NetworkUtils.singupModel == null ? LoginWidget() : CartWidget()));
            },
              child: cartIconWidget()),
          // IconButton(
          //     onPressed: () {
          //       Navigator.push(context,
          //           MaterialPageRoute(builder: (context) => CartWidget()));
          //     },
          //     icon: Icon(Icons.shopping_cart_outlined))
        ],
      ),
      body: _currentIndex == 1 ? MyOrders(isShowAppBar: true,) : SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Visibility(
                    visible: isShowHeader,
                    child: Container(
                      color: HexColor('#F4F8FB'),
                      child: locationView(),
                    )),
                SizedBox(
                  height: 15,
                ),
                Stack(
                  children: [
                    CarouselSlider(
                      items: imagesSlide(),
                      options: CarouselOptions(
                          autoPlay: true,
                          height: 150,
                          viewportFraction: 1,
                          enlargeCenterPage: true,
                          aspectRatio: 1.0,
                          // disableCenter: true,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    ),
                    Positioned(
                      bottom: 5,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: images.map((url) {
                            int index = images.indexOf(url);
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _current == index
                                    ? Colors.white
                                    : Colors.grey,
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Explore best selling categories",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontFamily: AppTextStyle.bold, fontSize: 16),
                ),
                Visibility(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _categoriesList.length > 4
                            ? 4
                            : _categoriesList.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 1,
                            crossAxisCount: 2,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 15),
                        itemBuilder: (context, index) {
                          CategoryData theCategory = _categoriesList[index];
                          return InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          StoreWidget(theCategory)));
                            },
                            child: Container(
                              color: Colors.transparent,
                              height: 300,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child: Column(
                                  children: [
                                    Card(
                                      elevation: 2,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                      ),
                                      child: ClipPath(
                                        clipper: ShapeBorderClipper(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5))),
                                        child: Container(
                                            child: Center(
                                                child: _categoryImageView(
                                                    theCategory.categoryImage ??
                                                        '',
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                        2,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                        3))),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Align(
                                        child: Text(
                                          theCategory.name ?? '',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontFamily: AppTextStyle.bold),
                                        ),
                                        alignment: Alignment.bottomLeft,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                  visible: _categoriesList.length > 0 ? true : false,
                ),
                Visibility(
                  visible: _categoriesList.length > 4 ? true : false,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: ((_categoriesList?.length ?? 0) > 4)
                            ? _categoriesList.sublist(4).length
                            : 0,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 0.8,
                            crossAxisCount: 4,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 15),
                        itemBuilder: (context, index) {
                          var theCategory = _categoriesList.sublist(4)[index];
                          return InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          StoreWidget(theCategory)));
                            },
                            child: Container(
                              color: Colors.transparent,
                              height: 300,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child: Column(
                                  children: [
                                    Card(
                                      elevation: 2,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                      ),
                                      child: ClipPath(
                                        clipper: ShapeBorderClipper(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5))),
                                        child: Container(
                                            // decoration: BoxDecoration(
                                            //     image: DecorationImage(
                                            //         fit: BoxFit.cover,
                                            //         image: NetworkImage(
                                            //             'https://images.unsplash.com/photo-1579202673506-ca3ce28943ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'))),
                                            child: Center(
                                                child: _categoryImageView(
                                                    theCategory.categoryImage ??
                                                        "",
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                        5,
                                                    MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                        6))),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Align(
                                        child: Text(
                                          theCategory.name ?? "",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.black,
                                              fontFamily: AppTextStyle.bold),
                                        ),
                                        alignment: Alignment.bottomCenter,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                ),
                Image.asset("assets/images/screen mail.png")
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          if (index == 2){
            Share.share('https://play.google.com/store/apps/details?id=com.tds.true_digital_store');
          } else {
            if(NetworkUtils.singupModel == null && index == 1) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) =>  LoginWidget()));
              return;
            }
            setState(() {
              _currentIndex = index;
            });
          }
        },// this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.card_travel),
            title: new Text('Order'),
          ),
          BottomNavigationBarItem(icon: Icon(Icons.share), title: Text('Share'))
        ],
      ),
    );
  }

  Widget _categoryImageView(String imageUrl, double width, double height) {
    if (imageUrl.isEmpty) {
      return Image.asset(
        Assets.noimage,
        width: width,
        height: height,
        fit: BoxFit.cover,
      );
    } else {
      return Image.network(
        imageUrl,
        width: width,
        height: height,
        fit: BoxFit.cover,
      );
    }
  }

  List<Widget> imagesSlide() {
    return images
        .map((item) => GFImageOverlay(
              height: 150,
              width: MediaQuery.of(context).size.width,
              shape: BoxShape.rectangle,
              image: NetworkImage(NetworkUtils.base_image_url + item),
              boxFit: BoxFit.cover,
            ))
        .toList();
  }

  Widget locationView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        InkWell(
          onTap: () {
            AppRoutes.goto(this.context, SearchWidget());
          },
          child: Container(
              height: 40,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(200))),
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Icon(Icons.search),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Search for store/items',
                    style: TextStyle(
                        fontSize: 13,
                        color: Colors.grey,
                        fontFamily: AppTextStyle.regular),
                  )
                ],
              )),
        )
      ],
    );
  }
}
