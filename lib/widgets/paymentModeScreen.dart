import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:true_digital_store/model/cart_list_response.dart';
import 'package:true_digital_store/model/order_place_dto.dart';
import 'package:true_digital_store/theme/color.dart';
import 'package:true_digital_store/theme/text_style.dart';
import 'package:true_digital_store/utils/app_constants.dart';
import 'package:true_digital_store/utils/common_utills.dart';

import 'orderSuccessfully.dart';

class PaymentModeScreen extends StatefulWidget {
  final CartListResponse? cartResponse;

  const PaymentModeScreen({Key? key, this.cartResponse}) : super(key: key);

  @override
  _PaymentModeScreenState createState() => _PaymentModeScreenState();
}

class _PaymentModeScreenState extends State<PaymentModeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('F5F5F5'),
      appBar: AppBar(
        title: Text('Payment'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 10,
            ),
            Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Icon(Icons.local_shipping),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Cash on Delivery",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 100,
                          child: Text(
                            "Pay in cash or pay in person at the time of delivery with GPay/PayTM/PhonePe",
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    Icon(
                      Icons.radio_button_checked_sharp,
                      color: Colors.green,
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
              // child: Container(
              //   color: Colors.black12,
              // ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          'Sub Total',
                          style: TextStyle(fontFamily: AppTextStyle.Regular),
                        ),
                        Spacer(),
                        Text(
                          Constants.CURRENCY_SIGN +
                              "${(widget.cartResponse?.data?.subTotal ?? "")}",
                          style: TextStyle(fontFamily: AppTextStyle.Bold),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          'GST',
                          style: TextStyle(fontFamily: AppTextStyle.Regular),
                        ),
                        Spacer(),
                        Text(
                          "${(widget.cartResponse?.data?.gstCharges ?? "")}" !=
                                  ""
                              ? "${(widget.cartResponse?.data?.gstCharges ?? "")}%"
                              : "",
                          style: TextStyle(fontFamily: AppTextStyle.Bold),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          'Delivery Charge',
                          style: TextStyle(fontFamily: AppTextStyle.Regular),
                        ),
                        Spacer(),
                        Text(
                          "${(widget.cartResponse?.data?.deliveryCharge ?? "")}" !=
                                  ""
                              ? Constants.CURRENCY_SIGN +
                                  "${(widget.cartResponse?.data?.deliveryCharge ?? "")}"
                              : "",
                          style: TextStyle(fontFamily: AppTextStyle.regular),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          'Discount',
                          style: TextStyle(fontFamily: AppTextStyle.Regular),
                        ),
                        Spacer(),
                        Text(
                          "${(widget.cartResponse?.data?.discount ?? "")}" != ""
                              ? Constants.CURRENCY_SIGN +
                                  "${(widget.cartResponse?.data?.discount ?? "")}"
                              : "",
                          style: TextStyle(fontFamily: AppTextStyle.regular),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          'Total',
                          style: TextStyle(fontFamily: AppTextStyle.Regular),
                        ),
                        Spacer(),
                        Text(
                          Constants.CURRENCY_SIGN +
                              "${(widget.cartResponse?.data?.totalPrice ?? "")}",
                          style: TextStyle(fontFamily: AppTextStyle.Bold),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      children: [
                        Text(
                          'Inclusive of all taxes',
                          style: TextStyle(fontFamily: AppTextStyle.Regular),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                ],
              ),
            ),
            InkWell(
              onTap: () {
                _placeOrder();
              },
              child: Container(
                height: 40,
                color: AppColor.green,
                child: Center(
                    child: Text(
                  'Place Order',
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontFamily: AppTextStyle.bold),
                )),
              ),
            )
          ],
        )),
      ),
    );
  }

  _placeOrder() {
    if (widget.cartResponse == null) {
      return;
    }

    Map<String, dynamic> params = {};
    params["coupon_code"] = widget.cartResponse?.data?.couponCode ?? '';
    params["delivery_charge"] =
        "${widget.cartResponse?.data?.deliveryCharge ?? 0}";
    params["discount_amount"] = "${(widget.cartResponse?.data?.discount == null || widget.cartResponse?.data?.discount == "")  ? '0' : widget.cartResponse?.data?.discount ?? '0'}";
    params["coupon_uuid"] = widget.cartResponse?.data?.couponUUID ?? '';
    params["address_id"] = widget.cartResponse?.data?.addressId ?? '';
    params["delivery_type"] = "1";
    params["payment_mode"] = "3";

    CommonUtills.showprogressdialogcomplete(context, true);
    CartListResponse.placeOrder(context, params, (response) {
      CommonUtills.showprogressdialogcomplete(context, false);
      print(response);
      if (response is OrderPlaceDto) {
        var res = response;
        if (res.status == true) {
          Get.to(OrderSuccessfully());
          //Get.back();
          // CommonUtills.successtoast(context, res?.message ?? "Order placed");
        } else {
          CommonUtills.successtoast(
              context, "Something went wrong, please try again");
        }
      }
    });
  }
}
