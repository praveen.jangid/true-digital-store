import 'dart:convert';
import 'dart:developer';
import 'dart:io' show Platform, SocketException;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:true_digital_store/model/login_response.dart';
import 'package:true_digital_store/widgets/home.dart';

//import 'package:stagepicker/model/loginSignupResponse.dart';

class NetworkUtils {
//  http://stagepicker.abstractsoftweb.com/api/signup

  static String kTwillioSID = "AC3ba837b35c7fee4e6c6395a9c8eb71c4";
  static String kTwillioAuthToken = "b28f4157ed25f2c0dd78f2f2b1553406";
  static String kPlaceApi = "AIzaSyCg4I7zMb2ksbCQj3EVkdA0UP66wjjdo88";

  static String base_url = 'https://truedigitalstore.com/api/';
  static String base_image_url = 'https://truedigitalstore.com/uploads/';
  static String kGetBannerList = base_url + "getBannerList";
  static String kGetCategoryList = base_url + "getCategoryList";
  static String kStoreSearch = base_url + "getStoreSerch";
  static String kLogin = base_url + "login";
  static String kGetUser = base_url + "getUser";

  static String kStoreListByCategory = base_url + "getStoreList";
  static String kStoreDetail = base_url + "getStoreDetails";
  static String kStoreSubcategoryProduct = base_url + "getProductByCategory";

  static String kGetAddressList = base_url + "getAddress";
  static String kAddAddress = base_url + "addAddress";
  static String kUpdateAddress = base_url + "updateAddress";
  static String kDeleteAddress = base_url + "deleteAddress";

  static String kGetCartData = base_url + "getCartData";
  static String kClearCart = base_url + "clearCart";
  static String kAddToCart = base_url + "addToCart";
  static String kUpdateCartQuantity = base_url + "updateCartQuantity";
  static String kRemoveFromCart = base_url + "removeFromCart";

  static String kSearchForStoreAndProduct =
      base_url + "searchForStoreAndProduct";

  static String kProductList = base_url + "getProductList";

  static String kOrderCreate = base_url + "orderCreate";
  static String kOrderList = base_url + "orderList";

  static String kStoreProductSearch = base_url + "getProductSearch";

  static String kGetCouponList = base_url + "getCouponList";
  static String kGetOrderDetail = base_url + "orderDetails";
  static String kAddFeedback = base_url + "addFeedback";
  static LoginResponse? singupModel;

  static setLoginDetailsINTOSharedPrefrence(
      LoginResponse newLoginDetails) async {
    singupModel = newLoginDetails;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("loginsession", jsonEncode(newLoginDetails));
  }

  static getlogindetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.getString("loginsession");
    print(value);
    if (value != null) {
      Map<String, dynamic> map = jsonDecode(value);
      singupModel = LoginResponse.fromJson(map);
    }
  }

  static logOut(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("loginsession");
    singupModel = null;

    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
      return HomeWidget();
    }));
  }

  static NetworkUtils _instance = new NetworkUtils.internal();
  SharedPreferences? sha;

  // String token=PrefrencesManager.getString(StringMessage.token);

  NetworkUtils.internal();

  factory NetworkUtils() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url, BuildContext context, {encoding}) async {
    Uri u = Uri.parse(url);
    var client = http.Client();
    log("URL::: $url PARAMS:::: " + url.toString());
    print("URL::: $url PARAMS:::: " + url.toString());

    Map<String, String> headers = {};

    if (singupModel != null) {
      headers = {"Authorization": "Bearer ${singupModel?.data?.token ?? ''}"};
    }
    log("API HEADERS: ${headers}");
    try {
      return client.get(u, headers: headers).then((http.Response response) {
        var res = response.body;
        int statusCode = response.statusCode;
        log("API Response: " + res);
        print("API Response: " + res);

        if (statusCode < 200 || statusCode > 401) {
          throw new Exception(statusCode);
        }
        return _decoder.convert(res);
      }).timeout(const Duration(seconds: 300));
    } finally {
      client.close();
    }
  }

  Future<dynamic> post(String url, BuildContext context,
      {body, encoding}) async {
    log("URL::: $url");
    Uri u = Uri.parse(url);
    var client = http.Client();
    Map<String, String> headers = {"Accept": "application/json"};

    if (singupModel != null) {
      headers["Authorization"] = "Bearer ${singupModel?.data?.token ?? ''}";
    }
    log("PARAMS:::: ${body.toString()}   HEADERS:::: ${headers}");

    try {
      return client
          .post(u, body: body, encoding: encoding, headers: headers)
          .then((http.Response response) {
        var res = response.body;
        log("API Response: " + res);
        int statusCode = response.statusCode;
        // debugPrint(res, wrapWidth: 1000000);

        if (statusCode < 200 || statusCode > 401) {
          throw new Exception(statusCode);
        }
        return _decoder.convert(res);
      }).timeout(const Duration(seconds: 30));
    } finally {
      client.close();
    }
  }

  Future<dynamic> multiPart(
      String url,
      BuildContext context,
      Map<String, String> params,
      List<String> filePaths,
      List<String> mediaName) async {
    final multipartRequest = new http.MultipartRequest('POST', Uri.parse(url));
    print(
        ' after baseurl.................PRAMS : - ${params.toString() ?? ''}');
    multipartRequest.fields.addAll(params);
    print(' after params.................');
    filePaths.asMap().forEach((index, element) async {
      var pic = await http.MultipartFile.fromPath(mediaName[index], element);
      print(' after pic add .................');
      multipartRequest.files.add(pic);
      print(pic);
    });

    log("FILE PRINT  ${multipartRequest.files.toString()}");

    print(' after pic add.................');
    try {
      final response = await multipartRequest.send();
      print('error after response.................');
      try {
        var responseByteArray = await response.stream.toBytes();

        log("RESPONSE: STATUSCODE :- ${response.statusCode.toString()} Url :- ${url}  RESPONSE :- ${utf8.decode(responseByteArray)?.toString() ?? ""}");

        return json.decode(utf8.decode(responseByteArray));
      } on FormatException catch (e) {
        log('error ${e.toString()}');
      }
    } on SocketException {
      return json.decode(
          json.encode({'status': 400, 'message': 'No Internet connection'}));
    }
  }

  Future<dynamic> delete(String url, BuildContext context, {encoding}) async {
    Uri u = Uri.parse(url);
    var client = http.Client();
    Map<String, String> headers = {};
    if (singupModel != null) {
      headers = {"Authorization": "Bearer ${singupModel?.data?.token ?? ''}"};
    }
    log("PARAMS:::: ${u.toString()}   HEADERS:::: ${headers}");

    try {
      return client.delete(u, headers: headers).then((http.Response response) {
        String res = response.body;
        int statusCode = response.statusCode;
        log("API Response: " + res);

        if (statusCode < 200 || statusCode > 401) {
          throw new Exception(statusCode);
        }
        return _decoder.convert(res);
      }).timeout(const Duration(seconds: 300));
    } finally {
      client.close();
    }
  }
}
