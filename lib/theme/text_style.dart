import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
class AppTextStyle {
  static const TextDecoration underline = TextDecoration.underline;
  static const TextDecoration lineThrough = TextDecoration.lineThrough;

  static const String regular = "Montserrat-Regular";
  static const String extrabold = "Montserrat-ExtraBold";
  static const String bold = "Montserrat-Bold";
  static const String light = "Montserrat-Light";

  static const String Black = "Montserrat-Black.otf";
  static const String BlackItalic = "Montserrat-BlackItalic.otf";
  static const String Bold = "Montserrat-Bold.otf";
  static const String BoldItalic = "Montserrat-BoldItalic.otf";
  static const String ExtraBold = "Montserrat-ExtraBold.otf";
  static const String ExtraBoldItalic = "Montserrat-ExtraBoldItalic.otf";
  static const String ExtraLight = "Montserrat-ExtraLight.otf";
  static const String ExtraLightItalic = "Montserrat-ExtraLightItalic.otf";
  static const String Italic = "Montserrat-Italic.otf";
  static const String Light = "Montserrat-Light.otf";
  static const String LightItalic = "Montserrat-LightItalic.otf";
  static const String Medium = "Montserrat-Medium.otf";
  static const String MediumItalic = "Montserrat-MediumItalic.otf";
  static const String Regular = "Montserrat-Regular.otf";
  static const String SemiBold = "Montserrat-SemiBold.otf";
  static const String SemiBoldItalic = "Montserrat-SemiBoldItalic.otf";
  static const String Thin = "Montserrat-Thin.otf";
  static const String ThinItalic = "Montserrat-ThinItalic.otf";
}
