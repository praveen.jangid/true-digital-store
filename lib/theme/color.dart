import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class AppColor {
  static Color themeColor = Color(0xff0094DC);
  static Color green = HexColor('#387E23');
  static Color themePrimary = HexColor('#284256');
}
